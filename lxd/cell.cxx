#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "thermo.h"
#include "cell.h"
using namespace std;
cell_oneD::cell_oneD(double L_in, double areaL_in, double areaR_in)
	:L{L_in}, areaL{areaL_in}, areaR{areaR_in}
{
}
cell_oneD::cell_oneD()
	:L{0}, areaL{0}, areaR{0}
{
}
ostream& operator<<(ostream& info, const cell_oneD& c){
	info << "Cell with L: " << c.L << " area_L: " << c.areaL << " area_R: " << c.areaR << "\n";
	return info;
}
double cell_oneD::get_volume(){
	return 0.5*L*(areaL + areaR);
}
void cell_oneD::interpolateL(interpFunc f, cell_oneD* c_1, cell_oneD* c_2){
	interfL.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interfL.m_e = f(c_2->flow.m_e, flow.m_e, c_1->flow.m_e);
	interfL.m_u = f(c_2->flow.m_u, flow.m_u, c_1->flow.m_u);
	flow.gm->update_from_rho_e(interfL);
}
void cell_oneD::interpolateR(interpFunc f, cell_oneD* c_1, cell_oneD* c_2){
	interfR.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interfR.m_e = f(c_2->flow.m_e, flow.m_e, c_1->flow.m_e);
	interfR.m_u = f(c_2->flow.m_u, flow.m_u, c_1->flow.m_u);
	flow.gm->update_from_rho_e(interfR);
}
void cell_oneD::update_in_time(double dt){
	double rhodash = 1.0/get_volume()*(fluxL->mass*areaL - fluxR->mass*areaR);
	double momentumdash = 1.0/get_volume()*(fluxL->momentum*areaL - fluxR->momentum*areaR);
	double totalEdash = 1.0/get_volume()*(fluxL->total_energy*areaL - fluxR->total_energy*areaR);
	double oldenergy = flow.get_total_energy();
	flow.m_rho += rhodash*dt;
	flow.m_u = (flow.get_momentum() + momentumdash*dt)/flow.m_rho;
	flow.m_e = (oldenergy + totalEdash*dt)/flow.m_rho - 0.5*flow.m_u*flow.m_u;
	flow.gm->update_from_rho_e(flow);
}
