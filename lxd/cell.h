#ifndef CELL
#define CELL
#include <iostream>
using namespace std;
using interpFunc = double (*)(double, double, double);
class cell_oneD{
	public:
		flow_props flow;
		flow_props interfR;
		flow_props interfL;
		flux* fluxL;
		flux* fluxR;
		double areaL;
		double areaR;
		double L;
		double get_volume();
		cell_oneD();		
		cell_oneD(double, double, double);
		void interpolateL(interpFunc, cell_oneD*, cell_oneD*);	
		void interpolateR(interpFunc, cell_oneD*, cell_oneD*);
		void update_in_time(double);
};
ostream& operator<<(ostream& info,const cell_oneD&);
#endif