#include <iostream>
#include <cmath>
#include <fstream>
#include "thermo.h"
#include <iomanip>
using namespace std;
flow_props::flow_props(double p, double T, double u, gas_model* gm_m)
	:m_p{p}, m_T{T}, m_u{u}, gm{gm_m}
{
	gm->update_from_p_T(*this);
}
flow_props::flow_props()
	:m_rho{0}, m_u{0},m_T{0}
{
}
flow_props flow_props_from_rho_T(double rho, double T, double u, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_u = u; f.m_T=T; f.gm=gm_m;
		f.gm->update_from_rho_T(f);
		return f;
}
flow_props flow_props_from_rho_p(double rho, double p, double u, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_p = p; f.gm=gm_m;
		f.gm->update_from_rho_p(f);
		return f;
}

double flow_props::get_tke(){
	return 0.5*m_rho*m_u*m_u;
}
double flow_props::get_total_energy(){
	return m_rho*m_e + 0.5*m_rho*m_u*m_u;
}
double flow_props::get_momentum(){
	return m_u*m_rho;
}
void flow_props::write_to_file(string filename){
	ofstream m_file;
	m_file.open(filename, ios::out | ios::app );
	m_file<<setw(12)<<m_rho<<setw(12)<<m_p
		  <<setw(12)<<m_u<<setw(12)<<m_T
		  <<setw(12)<<m_e<<"\n";
	m_file.close();
}
flux::flux()
	:mass{0}, momentum{0}, total_energy{0}, tke{0}, omega{0}
{
}
ostream& operator<<(ostream& info, const flow_props& c){
	info << "p: " << c.m_p << " u: " << c.m_u << " T: " << c.m_T << " rho: "<< c.m_rho
			<< " e: " << c.m_e <<"\n";
	return info;
}
ostream& operator<<(ostream& info, const flux& c){
	info << "mass: " << c.mass << " mom: " << c.momentum << " TE: " << c.total_energy << "\n";
	return info;
}
