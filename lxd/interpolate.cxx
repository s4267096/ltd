#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
using namespace std;

double interpolateSecondOrderUpwind(double fi, double fi_1, double fi_2){
	/*
	|------------|------------|-------------|
	|            |            |             |
	|            |            |             |
	|    fi_2    |   fi_1    *|     f_i     |
	|            |            |             |
	|            |            |             |
	|------------|------------|-------------|
    Interpolated value at the face denoted by *
	*/
	//Second Order Upwind for computing interface values (left or right of interface) depending on a given list of cell centres us
	double DLminus;
	double DLplus;
	double alpha_L0 = 1/8.0; //evenly spaced cells
	double sL;
	double maxu;
	double minu;
	double r = (fi - fi_1)/(fi_1 - fi_2 +1e-12);//used for superbee
	//The following is stuff for the van albada limiter
	DLminus = (fi_1 - fi_2);
	DLplus  = (fi - fi_1);
	sL = (DLminus*DLplus + fabs(DLminus*DLplus))/(DLminus*DLminus + DLplus*DLplus + 1e-12);
	maxu = max(fi_1, fi_2);
	minu = max(fi_1, fi_2);
	// return fi_1 + 0.5*(r*r + r)/(1+r*r)*(fi_1 - fi_2);//SIMPLE VAN ALBADA
	return fi_1 + alpha_L0*(3*DLplus + DLminus)*sL;
}