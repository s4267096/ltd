//some classes to represent 1D flow
#ifndef FLOW
#define FLOW
#include <iostream>
using namespace std;
class gas_model;
class flow_props{
	public:
		flow_props();
		flow_props(double, double, double, gas_model*);
		double m_rho, m_u, m_e, m_p, m_T, m_h;
		gas_model* gm;
		double get_tke();
		double get_total_energy();
		double get_momentum();
		void write_to_file(string);
		// double get_omega();
};	
class flux{
		public:
		double mass;
		double momentum;
		double total_energy;
		double tke;
		double omega;
		flux();
};
flow_props flow_props_from_rho_T(double, double, double, gas_model*);
flow_props flow_props_from_rho_p(double, double, double, gas_model*);
ostream& operator<<(ostream&, const flow_props&);
ostream& operator<<(ostream&, const flux&);
#endif