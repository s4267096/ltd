#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include "analytical.h"
#include "thermo.h"
#include "cell.h"
using namespace std;
void computeShockTubeAnalytical(int n, double L, double t, flow_props f_L, flow_props f_R, cell_oneD* cells, gas_model* gmodel){
	double p4 = f_L.m_p; double rho4 = f_L.m_rho; double u4 = f_L.m_u;
	double p1 = f_R.m_p; double rho1 = f_R.m_rho; double u1 = f_R.m_u;
	double c4 = gmodel->sound_speed(f_L); double c1 = gmodel->sound_speed(f_R);
	double g = gmodel->m_gamma;
	double p2_p1 = getP2_P1(p4/p1, c1, c4, u1, u4, g);
	double c2 = sqrt(p2_p1*((g+1)/(g-1) + p2_p1)/(1 + (g+1)/(g-1)*p2_p1))*c1;
	double u2 = u1 + c1/g*(p2_p1 - 1)/sqrt((g+1)/(2*g)*(p2_p1-1)+1);
	flow_props f2;
	double p2 = p2_p1*p1;
	f2.m_u = u2;
	f2.m_p = p2;
	gmodel->update_from_p_c(f2,c2);
	double v_shock = u1 + c1*sqrt((g+1)/(2*g)*(p2_p1-1)+1);
	// cout << v_shock << "\n";
	double u3 = u2; 
	double p3 = p2;
	double c3 = (u4 - u3)*(g-1)/2 + c4; 
	flow_props f3;
	f3.m_u = u3;
	f3.m_p = p3;
	gmodel->update_from_p_c(f3,c3);
	double dx = 2*L/n; double x = -L + 0.5*dx;
	double x_t;
	for(int i = 0; i != n; i++){
		x_t = x/t;
		if (x_t > v_shock){
			cells[i].flow = f_R;
		}
		else if (x_t > u2) {
			cells[i].flow = f2;
		}
		else if (x_t > u3 - c3){
			cells[i].flow = f3;
		}
		else if (x_t > u4 - c4){
			cells[i].flow = getRarefactionProperties(x_t, u4, c4, p4, g, gmodel);
		}
		else {
			cells[i].flow = f_L;
		}
		x += dx;
	}
}
double getP4_P1(double p2_p1, double c1, double c4, double u1, double u4, double g){
	return p2_p1*pow(1 
			      + (g-1)/2/c4*(u4 - u1
			      				- c1/g*(p2_p1 - 1)/sqrt((g+1)/(2*g)*(p2_p1-1)+1)
			      				)
			      ,-2*g/(g-1));
}
double getP2_P1(double p4_p1, double c1, double c4, double u1, double u4, double g){
	double p2_p1L = 1; double p2_p1R = p4_p1;
	double fL = getP4_P1(p2_p1L, c1, c4, u1, u4, g) - p4_p1;
	double fR = getP4_P1(p2_p1R, c1, c4, u1, u4, g) - p4_p1;
	double p2_p1M; double fM; double tol = 1e-6;
	while (p2_p1R - p2_p1L > 1e-6){
		p2_p1M = 0.5*(p2_p1L + p2_p1R);
		fM = getP4_P1(p2_p1M, c1, c4, u1, u4, g) - p4_p1;
		if (fM * fL < 0) {
			fR = fM;
			p2_p1R = p2_p1M;
		}
		else {
			fL = fM;
			p2_p1L = p2_p1M;
		}
	}
	return p2_p1M;
}
flow_props getRarefactionProperties(double x_t, double u4, double c4, double p4, double g, gas_model* gmodel){
	flow_props f;
	f.m_u = 2/(g+1)*(x_t + (g - 1)/2*u4 + c4);
	double c = 2/(g+1)*(x_t + (g - 1)/2*u4 + c4) - x_t;
	f.m_p = p4*pow(c/c4,2*g/(g-1));
	gmodel->update_from_p_c(f, c);
	return f;
}