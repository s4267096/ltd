//a slightly not so silly program
#include <iostream>
#include "flow_oneD.h"
#include "cell.h"
#include "interpolate.h"
#include "ausmdv.h"
#include "post.h"
using namespace std;
int main()
{
	constexpr int n_cells = 104;
	constexpr int n_tsteps = 200;
	double dt = 0.0000001;
	double L = 0.01;
	double area = 0.01;
	cell_oneD cells[n_cells];
	flux fluxes[n_cells-3];
	std::fill(cells, cells + n_cells, cell_oneD(L,area,area));
	//set up flux address linking
	for(int i=0; i!=n_cells-3; i++){
		cells[i+2].fluxL=&fluxes[i];
		cells[i+1].fluxR=&fluxes[i];
	}
	//initial conditions
	double rho_L=1; double u_L = 0.0; double T_L=300; double p_L = 100000;
	double rho_R=0.125; double u_R = 0.0; double T_R=240; double p_R = 10000;
	gas_model air(1.4,0.02897,720);
	for(int i=0; i!=n_cells; ++i){
		if (i < 52) {
			cells[i].flow=flow_props_from_rho_p(rho_L, p_L, u_L, &air);
			;}
		else {cells[i].flow=flow_props_from_rho_p(rho_R, p_R, u_R, &air);}
	}
	//start updating in time
	double max_a=0;
	double t_elapsed=0;
	for(int j=0; j!=n_tsteps; ++j){
		for(int i=1; i!=n_cells-1; ++i){
			cells[i].interpolateL(&interpolateSecondOrderUpwind,&cells[i+1],&cells[i-1]);
			cells[i].interpolateR(&interpolateSecondOrderUpwind,&cells[i-1],&cells[i+1]);
		}
		for(int i=0; i!=n_cells-3; ++i){
			ausmdv(cells[i+1].interfR, cells[i+2].interfL, fluxes[i], &air);
		}
		for(int i=2; i!=n_cells-2; ++i){
			cells[i].update_in_time(dt);
			max_a=max(max_a, air.sound_speed(cells[i].flow));
		}
		t_elapsed += dt;
		dt=0.1*L/max_a;
		cout << "t: " << t_elapsed << "; dt: " << dt << "\n";
	}
	write_cells_to_file(cells,n_cells,"myfile.dat");
}