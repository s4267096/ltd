gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Density Error (kg/m^3)'; set title 'Error in Density from Analytical'; 
 plot 'celldiff160.dat' using 1:2 title 'N = 160' lc 2 with lines, 
 'celldiff80.dat' using 1:2 title 'N = 80' lc 1 with lines, 
 'celldiff40.dat' using 1:2 title 'N = 40' lc 4 with lines, 
 'celldiff20.dat' using 1:2 title 'N = 20' lc 6 with lines, 
 'celldiff10.dat' using 1:2 title 'N = 10' lc 7 with lines;
 set term png; 
 set output 'density_error.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Velocity Error (m/s)'; set title 'Error in Velocity from Analytical'; 
 plot 'celldiff160.dat' using 1:4 title 'N = 160' lc 2 with lines, 
 'celldiff80.dat' using 1:4 title 'N = 80' lc 1 with lines, 
 'celldiff40.dat' using 1:4 title 'N = 40' lc 4 with lines, 
 'celldiff20.dat' using 1:4 title 'N = 20' lc 6 with lines, 
 'celldiff10.dat' using 1:4 title 'N = 10' lc 7 with lines;
 set term png; 
 set output 'Velocity_error.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Pressure (Pa)'; set title 'Error in Pressure from Analytical'; 
 plot 'celldiff160.dat' using 1:3 title 'N = 160' lc 2 with lines, 
 'celldiff80.dat' using 1:3 title 'N = 80' lc 1 with lines, 
 'celldiff40.dat' using 1:3 title 'N = 40' lc 4 with lines, 
 'celldiff20.dat' using 1:3 title 'N = 20' lc 6 with lines, 
 'celldiff10.dat' using 1:3 title 'N = 10' lc 7 with lines;
 set term png; 
 set output 'Pressure_error.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Temperature (K)'; set title 'Error in Temperature from Analytical'; 
 plot 'celldiff160.dat' using 1:5 title 'N = 160' lc 2 with lines, 
 'celldiff80.dat' using 1:5 title 'N = 80' lc 1 with lines, 
 'celldiff40.dat' using 1:5 title 'N = 40' lc 4 with lines, 
 'celldiff20.dat' using 1:5 title 'N = 20' lc 6 with lines, 
 'celldiff10.dat' using 1:5 title 'N = 10' lc 7 with lines;
 set term png; 
 set output 'Temperature_error.png'; replot; set term x11"
