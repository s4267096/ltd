#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "thermo.h"
#include "cell.h"
using namespace std;
cell_oneD::cell_oneD(double L_in, double areaL_in, double areaR_in)
	:L{L_in}, areaL{areaL_in}, areaR{areaR_in}
{
}
cell_oneD::cell_oneD()
	:L{0}, areaL{0}, areaR{0}
{
}
ostream& operator<<(ostream& info, const cell_oneD& c){
	info << "Cell with L: " << c.L << " area_L: " << c.areaL << " area_R: " << c.areaR << "\n";
	return info;
}
double cell_oneD::get_volume(){
	return 0.5*L*(areaL + areaR);
}
void cell_oneD::interpolateL(interpFunc f, cell_oneD* c_1, cell_oneD* c_2){
	interfL.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interfL.m_p = f(c_2->flow.m_p, flow.m_p, c_1->flow.m_p);
	interfL.m_u = f(c_2->flow.m_u, flow.m_u, c_1->flow.m_u);
	flow.gm->update_from_rho_p(interfL);
}
void cell_oneD::interpolateR(interpFunc f, cell_oneD* c_1, cell_oneD* c_2){
	interfR.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interfR.m_p = f(c_2->flow.m_p, flow.m_p, c_1->flow.m_p);
	interfR.m_u = f(c_2->flow.m_u, flow.m_u, c_1->flow.m_u);
	flow.gm->update_from_rho_p(interfR);
}
void cell_oneD::update_in_time(double dt){
	double rhodash = 1.0/get_volume()*(fluxL->mass*areaL - fluxR->mass*areaR);
	double momentumdash = 1.0/get_volume()*(fluxL->momentum*areaL - fluxR->momentum*areaR);
	double totalEdash = 1.0/get_volume()*(fluxL->total_energy*areaL - fluxR->total_energy*areaR);
	double oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	flow.m_rho += rhodash*dt;
	flow.m_u = (oldmomentum + momentumdash*dt)/flow.m_rho;
	flow.m_e = (oldenergy + totalEdash*dt)/flow.m_rho - 0.5*flow.m_u*flow.m_u;
	flow.gm->update_from_rho_e(flow);
}
void cell_oneD::update_in_time3(cell_oneD &newcell, double dt){
	double rhodash = 1.0/get_volume()*(fluxL->mass*areaL - fluxR->mass*areaR);
	double momentumdash = 1.0/get_volume()*(fluxL->momentum*areaL - fluxR->momentum*areaR);
	double totalEdash = 1.0/get_volume()*(fluxL->total_energy*areaL - fluxR->total_energy*areaR);
	double oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	newcell.flow.m_rho = flow.m_rho + rhodash*dt;
	newcell.flow.m_u = (oldmomentum + momentumdash*dt)/newcell.flow.m_rho;
	newcell.flow.m_e = (oldenergy + totalEdash*dt- newcell.flow.get_tke())/newcell.flow.m_rho ;
	newcell.flow.gm->update_from_rho_e(newcell.flow);
}