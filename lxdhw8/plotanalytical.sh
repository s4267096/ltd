gnuplot -e "unset key;
 set xlabel 'x (m)'; set ylabel 'Density (kg/m^3)'; set title 'Density'; 
  plot 'analytical_shock_tube.dat' using 1:2 title 'Analytical' lc 8 with lines;
 set term png; 
 set output 'density.png'; replot; set term x11"
gnuplot -e "unset key;
 set xlabel 'x (m)'; set ylabel 'Velocity (m/s)'; set title 'Velocity'; 
  plot 'analytical_shock_tube.dat' using 1:4 title 'Analytical' lc 8 with lines;
 set term png; 
 set output 'Velocity.png'; replot; set term x11"
gnuplot -e "unset key;
 set xlabel 'x (m)'; set ylabel 'Pressure (Pa)'; set title 'Pressure'; 
  plot 'analytical_shock_tube.dat' using 1:3 title 'Analytical' lc 8 with lines;
 set term png; 
 set output 'Pressure.png'; replot; set term x11"
gnuplot -e "unset key;
 set xlabel 'x (m)'; set ylabel 'Temperature (K)'; set title 'Temperature'; 
  plot 'analytical_shock_tube.dat' using 1:5 title 'Analytical' lc 8 with lines;
 set term png; 
 set output 'Temperature.png'; replot; set term x11"
