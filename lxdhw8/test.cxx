//a slightly not so silly program
#include <iostream>
#include "analytical.h"
#include "flow_oneD.h"
#include "interpolate.h"
#include "cell.h"
#include "post.h"
#include "flow_oneD.h"
#include "ausmdv.h"
#define fluxFuncName stegerwarming
#define strfluxFuncName "stegerwarming"
#define orderinterp "secondorder"
#define interpFuncname interpolateSecondOrderUpwind
using fluxFunc = int (*)(flow_props, flow_props, flux&, gas_model*);
int roe(flow_props, flow_props, flux& face, gas_model*);
int stegerwarming(flow_props, flow_props, flux&, gas_model*);
void run_simulation(fluxFunc, double, double, double, double, int, int, cell_oneD*,  cell_oneD*, flux*, gas_model*);

using namespace std;
int main()
{
	double rho_L=1; double u_L = 100.0; double p_L = 100000;
	double rho_R=0.125; double u_R = 50.0; double p_R = 10000;
	gas_model air(1.4,0.02897);
	flow_props f_L = flow_props_from_rho_p(rho_L, p_L, u_L, &air);
	flow_props f_R = flow_props_from_rho_p(rho_R, p_R, u_R, &air);
	double L = 10.0; // half length
	double area = 1;
	double t_total = 0.01;
	int ns[5] = {160, 80, 40, 20, 10};
	int n_analytical = 160;
	cell_oneD cellsAnalytical[n_analytical];
	computeShockTubeAnalytical(n_analytical, L, t_total, f_L, f_R, cellsAnalytical, &air);
	write_cells_to_file(cellsAnalytical,n_analytical,L,"analytical_shock_tube.dat");
	for(int i_ndx = 0; i_ndx != 5; i_ndx++){
		int n = ns[i_ndx];
		cell_oneD* cells = new cell_oneD[n];
		cell_oneD* cellsAnalytical = new cell_oneD[n];
		cell_oneD* cells_halfstep = new cell_oneD[n];
		computeShockTubeAnalytical(n, L, t_total, f_L, f_R, cellsAnalytical, &air);
		flux* fluxes =  new flux[n-3];
		double dt_init = 0.000001;
		int n_tsteps = 10000;
		double dx = 2*L/n;
		std::fill(cells, cells + n, cell_oneD(dx,area,area));
		std::fill(cells_halfstep, cells_halfstep, cell_oneD(dx,area,area));

		//set up flux address linking
		for(int i=0; i!=n-3; i++){
			cells[i+2].fluxL=&fluxes[i];
			cells[i+1].fluxR=&fluxes[i];
		}
		//fill with initial conditions
		for(int i=0; i!=n; ++i){
			if (i < n/2) {
				cells[i].flow=f_L;
				cells_halfstep[i].flow=f_L;
				}
			else {
				cells[i].flow=f_R;
				cells_halfstep[i].flow=f_R;
			}
		}
		//start updating in time
		t_total = 0.01;
		double t_elapsed=0;
		run_simulation(&fluxFuncName, dt_init, t_total,L, dx, n, n_tsteps, cells, cells_halfstep, fluxes, &air);
		// take cell difference
		cell_oneD cellsDiff[n];
		for(int i=0; i!=n; ++i){
			cellsDiff[i].flow.m_u = cells[i].flow.m_u - cellsAnalytical[i].flow.m_u;
			cellsDiff[i].flow.m_p = cells[i].flow.m_p - cellsAnalytical[i].flow.m_p;
			cellsDiff[i].flow.m_rho = cells[i].flow.m_rho - cellsAnalytical[i].flow.m_rho;
			cellsDiff[i].flow.m_e = cells[i].flow.m_e - cellsAnalytical[i].flow.m_e;
			cellsDiff[i].flow.m_T = cells[i].flow.m_T - cellsAnalytical[i].flow.m_T;
		}
		write_cells_to_file(cellsDiff,n,L,strfluxFuncName orderinterp "/celldiff"+to_string(n)+".dat");
		delete cells;
		delete cells_halfstep;
		delete cellsAnalytical;
		delete fluxes;
	}
}
void run_simulation(fluxFunc fluxf, double dt, double t_total, double L, double dx, int n, int n_tsteps, cell_oneD* cells,  cell_oneD* cells_halfstep, flux* fluxes, gas_model* gmodel)
{
	double max_a; double t_elapsed = 0; 
for(int j=0; j!=n_tsteps; ++j){
	//half step
		for(int i=1; i!=n-1; ++i){
			cells[i].interpolateL(&interpFuncname,&cells[i+1],&cells[i-1]);
			cells[i].interpolateR(&interpFuncname,&cells[i-1],&cells[i+1]);
		}
		for(int i=0; i!=n-3; ++i){
			fluxf(cells[i+1].interfR, cells[i+2].interfL, fluxes[i], gmodel);
		}
		for(int i=2; i!=n-2; ++i){
			cells[i].update_in_time3(cells_halfstep[i],dt/2);
		}
		//full step
		for(int i=1; i!=n-1; ++i){
			cells_halfstep[i].interpolateL(&interpFuncname,&cells_halfstep[i+1],&cells_halfstep[i-1]);
			cells_halfstep[i].interpolateR(&interpFuncname,&cells_halfstep[i-1],&cells_halfstep[i+1]);
		}
		for(int i=0; i!=n-3; ++i){
			fluxf(cells_halfstep[i+1].interfR, cells_halfstep[i+2].interfL, fluxes[i], gmodel);
		}
		for(int i=2; i!=n-2; ++i){
			cells[i].update_in_time(dt);
			max_a=max(max_a, gmodel->sound_speed(cells[i].flow));
		}
		t_elapsed += dt;
		dt=0.1*dx/max_a;
		cout << "t: " << t_elapsed << "; dt: " << dt << "\n";
		if (t_elapsed > t_total){
			write_cells_to_file(cells,n,L,strfluxFuncName orderinterp "/sol"+to_string(n)+".dat");
			break;
		}
	}
	write_cells_to_file(cells,n,L,to_string(n)+".dat");
}