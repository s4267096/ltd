/** \file stegerwarming.cxx
**/
#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include "thermo.h"
using namespace std;
/*------------------------------------------------------------*/


/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int stegerwarming(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel){
    double eps=1e-6;
    double rL, rR;
    double pL, pR;
    double uL, uR;
    double cL, cR;
    double ML, MR;

    gas_model m_gas_model = *gmodel;
    double g = gmodel->m_gamma;
    rL = f_L.m_rho;
    pL = f_L.m_p;
    uL = f_L.m_u;
    cL = m_gas_model.sound_speed(f_L);

    rR = f_R.m_rho;
    pR = f_R.m_p;
    uR = f_R.m_u;
    cR = m_gas_model.sound_speed(f_R);
    double gmin1 = g - 1;

    ML = uL/cL; MR = uR/cR;
    face.mass = 0; face.momentum = 0; face.total_energy = 0;
    double lam1; double lam2; double lam3;
    if (MR <= -1){
        face.mass += rR*uR;
        face.momentum += rR*uR*uR + pR;
        face.total_energy += (f_R.get_total_energy() + pR)*uR;
    }
    else if (MR <= 0){
        lam2 = (g-1)/g*rR*uR;
        lam2 = 0.5*(lam2 - sqrt(lam2*lam2 + eps * eps));
        lam3 = rR/2/g*(uR - cR);
        face.mass +=lam2 + lam3;
        face.momentum += lam2*uR + lam3*(uR - cR);
        face.total_energy += lam2*0.5*uR*uR
                            +lam3*(0.5*uR*uR + cR*cR/(g-1) - cR*uR);
    }
    else if (MR <= 1){
        // cout << "M: " << M << "\n";
        lam3 = rR/2/g*(uR - cR);
        lam3 = 0.5*(lam3 - sqrt(lam3*lam3 + eps * eps));
        face.mass += lam3;
        face.momentum += lam3*(uR - cR);
        face.total_energy +=lam3*(0.5*uR*uR + cR*cR/(g-1) - cR*uR);

    }
    if (ML >= 1){
        face.mass += rL*uL;
        face.momentum += rL*uL*uL + pL;
        face.total_energy += (f_L.get_total_energy() + pL)*uL;
    }
    else if (ML >= 0){
        // cout << "M: " << M << "\n";
        lam1 = rL/2/g*(uL + cL);
        lam2 = (g-1)/g*rL*uL;
        lam2 = 0.5*(lam2 + sqrt(lam2*lam2 + eps * eps));
        face.mass += lam1 + lam2;
        face.momentum += lam1*(uL + cL) + lam2*uL;
        face.total_energy += lam1*(0.5*uL*uL + cL * cL/(g-1) + cL*uL)
                            +lam2*0.5*uL*uL;
                            

    }
    else if (ML >= -1){
        lam1 =rL/2/g*(uL + cL);
        lam1 = 0.5*(lam1 + sqrt(lam1*lam1 + eps * eps));
        face.mass += lam1;
        face.momentum += lam1*(uL + cL);
        face.total_energy += lam1*(0.5*uL*uL + cL * cL/(g-1) + cL*uL);

    }

}   /* end of stegerwarming() */

/*--------------------------------------------------------------*/
