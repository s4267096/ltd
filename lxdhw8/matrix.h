//some classes to represent 1D flow
#ifndef MAT
#define MAT
#include <iostream>
using namespace std;
class vector3{
	public:
		double x [3];
		vector3(double, double, double);
		vector3();
		vector3 scalarMult(double);
		void print();
};
class matrix3{
	public:
		double A [3][3];
		matrix3();
		void print();
		matrix3 transpose();

};
vector3 multiply(matrix3, vector3);
matrix3 multiply(matrix3, matrix3);
vector3 add(vector3, vector3);
vector3 subtract(vector3, vector3);
double dot(vector3, vector3);
#endif