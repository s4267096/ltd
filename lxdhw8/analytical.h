#ifndef ANALYTICAL
#define ANALYTICAL
#include "thermo.h"
#include "cell.h"
void computeShockTubeAnalytical(int, double, double, flow_props, flow_props, cell_oneD*, gas_model*);
double getP4_P1(double, double, double, double, double, double);
double getP2_P1(double, double, double, double, double, double);
flow_props getRarefactionProperties(double, double, double, double, double, gas_model*);
#endif