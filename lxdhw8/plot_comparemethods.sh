gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Density (kg/m^3)'; set title 'Error in Density with N = 160'; 
 plot 'stegerwarmingfirstorder/celldiff160.dat' using 1:2 title 'SW First Order' lc 2 with lines, 
 'roefirstorder/celldiff160.dat' using 1:2 title 'Roe First Order' lc 6 with lines, 
 'roesecondorder/celldiff160.dat' using 1:2 title 'Roe Second Order' lc 8 with lines;
 set term png; 
 set output 'density_comparemethods.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Velocity (m/s)'; set title 'Error in Velocity with N = 160'; 
 plot 'stegerwarmingfirstorder/celldiff160.dat' using 1:4 title 'SW First Order' lc 2 with lines, 
 'roefirstorder/celldiff160.dat' using 1:4 title 'Roe First Order' lc 6 with lines, 
 'roesecondorder/celldiff160.dat' using 1:4 title 'Roe Second Order' lc 8 with lines;
 set term png; 
 set output 'Velocity_comparemethods.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Pressure (Pa)'; set title 'Error in Pressure with N = 160'; 
 plot 'stegerwarmingfirstorder/celldiff160.dat' using 1:3 title 'SW First Order' lc 2 with lines, 
 'roefirstorder/celldiff160.dat' using 1:3 title 'Roe First Order' lc 6 with lines, 
 'roesecondorder/celldiff160.dat' using 1:3 title 'Roe Second Order' lc 8 with lines;
 set term png; 
 set output 'Pressure_comparemethods.png'; replot; set term x11"
gnuplot -e "set key on left top;
 set xlabel 'x (m)'; set ylabel 'Temperature (K)'; set title 'Error in Temperature with N = 160'; 
 plot 'stegerwarmingfirstorder/celldiff160.dat' using 1:5 title 'SW First Order' lc 2 with lines, 
 'roefirstorder/celldiff160.dat' using 1:5 title 'Roe First Order' lc 6 with lines, 
 'roesecondorder/celldiff160.dat' using 1:5 title 'Roe Second Order' lc 8 with lines;
 set term png; 
 set output 'Temperature_comparemethods.png'; replot; set term x11"
