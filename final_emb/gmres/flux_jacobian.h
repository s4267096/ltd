#ifndef FLUXJACOBIANCLASS
#define FLUXJACOBIANCLASS

#include "../solver.h"
#include "solution.h"

class flux_jacobian
{

public:
	flux_jacobian(solver*, Solution*, Solution*);        //< Default constructor for the flux_jacobian Class.
	flux_jacobian(const flux_jacobian& oldCopy);   //< Copy constructor for the flux_jacobian Class.
	~flux_jacobian();                        //< Destructor for the flux_jacobian Class.

	// Basic algebraic operators associated with the linearization of the operator.
	double& operator()(int row,int column);     //< The value of the linearization for the operator at a given row and column.
	Solution operator*(class Solution vector);  //< The linearized operator acting on a given Solution.

	
	/**
		 Method to get the value of the x coordinate for a given row number.

		 @param row The row number you want to access.
		 @return The value of x at the given row number.
	 */
	double getX(int row) const
	{
		return(x[row]);
	}

	/**
		 Method to get the number of elements that are in the approximation.

		 @return The number of elements in the grid.
	 */
	int  getN() const
	{
		return(N);
	}

	/**
		 Method to get one of the elements from the first derivative matrix.

		 @param row The row number in the matrix
		 @param col The column number in the matrix.
		 @return The value within the matrix at the given row and column.
	 */
	void set_dt(double dt_in){
		dt = dt_in;
	}
	Solution *F0; //flux at the point we supplied it
	
protected:

	Solution *perturbed_F;
 	solver *main_solver;
 	double dt;//time step
 	double V;//volume of all cells (assumed to be uniform)

private:

	// Define the resolution of the approximation. Also define the
	// first derivative matrix (d) and the second derivative matrix
	// (d2). The grid points are given by x.
	int N;        //< The number of grid points in the approximation.
	double **d1;  //< Pointer to the first derivative matrix.
	double **d2;  //< Pointer to the second derivative matrix.
	double *x;    //< Pointer to the set of x grid points


};


#endif
