#include "flux_jacobian.h"

#include <cmath>


/** ************************************************************************
 * Base constructor  for the flux_jacobian class. 
 *
 *
 *	@param number of grid points to use in the discretization.
 * ************************************************************************ */
flux_jacobian::flux_jacobian(solver* main_solver_in, Solution* F0_in, Solution* perturbed_F_in)
{
  main_solver = main_solver_in;
  N = 4*main_solver->N_nonghost;
  F0 = F0_in;
  perturbed_F = perturbed_F_in;
  main_solver->reconstruct_and_compute_fluxes(1);
  main_solver->compute_flux_vector(*F0,0);
  main_solver->compute_flux_auxiliary(*F0,0);
}


/** ************************************************************************
 *		Copy constructor  for the flux_jacobian class. 
 *
 *	@param oldCopy The flux_jacobian class member to make a copy of.
 * ************************************************************************ */
flux_jacobian::flux_jacobian(const flux_jacobian& oldCopy)
{
  N  = oldCopy.getN();
	// need to finish implementing
}

/** ************************************************************************
 *	Destructor for the flux_jacobian class. 
 *  ************************************************************************ */
flux_jacobian::~flux_jacobian()
{
	//need to put something in here
}


/** ************************************************************************
 * The parenthesis operator for the flux_jacobian class.
 * 
 * Returns the value of the coefficient for the linearized operator for the indicated row and column.
 *
 * @param row The row number to use.
 * @param column The column number to use.
 * @return a double precision value, L[row][column]
 * ************************************************************************ */
double& flux_jacobian::operator()(int row,int column)
{
	return(d2[row][column]);
}

/** ************************************************************************
 * The matrix/vector  multiplication operator for the flux_jacobian class.
 * 
 * Returns a new Solution object which is the matrix/vector product of
 * an object from the flux_jacobian class and an object from the Solution
 * class. This is the linearized version of the operator.
 *
 * @param vector  The Solution object to multiply by this matrix.
 * @return The result of the operation, an object from the Solution class.
 * ************************************************************************ */
Solution flux_jacobian::operator*(class Solution dW)
{	
	// fprintf(stderr, "%16.16f \n", dW.norm());
	main_solver->perturb_cells(dW, 1.0);
	main_solver->reconstruct_and_compute_fluxes(0);
	main_solver->compute_flux_vector(*perturbed_F,1);
	main_solver->compute_flux_auxiliary(*perturbed_F,1);
	Solution dF = (*perturbed_F - *F0);
	return dW*(1/dt) - dF;
}