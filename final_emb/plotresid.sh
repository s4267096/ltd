gnuplot -e "set key off;
 set xlabel 'No. Iterations'; set ylabel 'Residual'; set logscale y 10; set title 'Residual vs no. of iterations'; 
 plot 'residuallocaldt.dat' using 1:2 lc 6,;
 set term png; 
 set output 'resid_localfirst.png'; replot; set term x11"

