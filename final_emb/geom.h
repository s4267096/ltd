#ifndef GEOM
#define GEOM
#include <iostream>
using namespace std;
class vec{
	public:
		double x, y, z;
		double mag() const;
		vec();
		vec(double, double, double);
		void make_unit();
		void switch_y_x();
		void switch_z_x();
		vec operator+(const vec&);
		vec operator-(const vec&);
		void operator+=(const vec&);
		vec operator*(const double);
};
ostream& operator<<(ostream&, const vec);
void get_transpose(vec, vec, vec, vec&, vec&, vec&);
double distance(vec, vec);
double dot(vec, vec);
vec cross(vec, vec);
double get_gradient(vec, vec);
vec get_edge_point_x(vec, double, double);
vec get_edge_point_y(vec, double, double);
#endif