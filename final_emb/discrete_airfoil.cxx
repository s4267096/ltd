#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include <sstream>
#include "geom.h"
#include "embsurf.h"
#include "numerical.h"
#include "esp.h"
using namespace std;
#define cubic(a,b,c,d,x) ((d)*(x)*(x)*(x) + (c)*(x)*(x) + (b)*(x) + (a))
#define cubic_deriv(b,c,d,x) (3*(d)*(x)*(x) + 2*(c)*(x) + (b))
#define cubic_deriv_deriv(c,d,x) (6*(d)*(x) + 2*(c))
discrete_airfoil::discrete_airfoil(int loopflag){
    loop = loopflag;
    ifstream file("discrete_points.dat");
    std::string   line;
    // Read one line at a time into the variable line:
    Npoints = 0;
    while(std::getline(file, line))
    {
        std::stringstream  lineStream(line);

        double x ,y;
        // Read an integer at a time from the line
        lineStream >> x;
        lineStream >> y;
        p.push_back(vec(x,y,0.0));
        Npoints++;
    }
    if (!loop) Npoints--;
    esps = new embsurfpoint[Npoints];
    cout << "made new discrete surface with Npoints: " << Npoints << "\n";
}
double discrete_airfoil::phi(vec v){
	vec closest_point, normal;
	get_closestpoint_and_normal(v,closest_point,normal);
	int sign = (dot(v - closest_point, normal) < 0)? 1 : -1;
	return distance(v, closest_point)*sign;
}
void discrete_airfoil::generate_own_embsurfpoints(){
	vec p0, p1, dp0, dp1;
	int esps_i = 0;
	double xcoeffs[4];
	double ycoeffs[4];
	double x0, x1, y0, y1;
	int i_pls_1;
	p0 = p[0];
	for(int i = 0; i != Npoints; ++i){
    	if(loop) i_pls_1 = (i == Npoints - 1) ? 0 : i + 1;
        else i_pls_1 = i + 1;
    	p1 = p[i_pls_1];
    	double length = distance(p0,p1);
		esps[esps_i] = embsurfpoint((p0 + p1)*0.5, vec(p1.y - p0.y, p0.x - p1.x, 0.0),length);
		p0 = p1;
		esps_i++;
	}
}

inline double FF(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	return dx*(v.x - x) + dy*(v.y-y);
}
inline double dFFdx(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double ddx = cubic_deriv_deriv(xcoeffs[2], xcoeffs[3],t);
	double ddy = cubic_deriv_deriv(ycoeffs[2], ycoeffs[3],t);
	return ddx*(v.x - x) - dx*dx + ddy*(v.y - y) - dy*dy;
}
inline int solve_closestpoint_and_normal(vec v, vec p0, vec t0, vec p1, vec t1, vec& closest_point, vec& normal){
	double xcoeffs[4];
	double ycoeffs[4];
	double length = distance(p1,p0);
	t0 = t0*length;
	t1 = t1*length;
	xcoeffs[0] = p0.x;
	xcoeffs[1] = t0.x;
	xcoeffs[2] = -3*p0.x + 3*p1.x - 2*t0.x - t1.x;
	xcoeffs[3] = 2*p0.x -2*p1.x + t0.x + t1.x;
	ycoeffs[0] = p0.y;
	ycoeffs[1] = t0.y;
	ycoeffs[2] = -3*p0.y + 3*p1.y - 2*t0.y - t1.y;
	ycoeffs[3] = 2*p0.y -2*p1.y + t0.y + t1.y;
	function<double(double)> f = bind(FF,v,xcoeffs, ycoeffs,placeholders::_1);
	function<double(double)> dfdx = bind(dFFdx,v,xcoeffs, ycoeffs,placeholders::_1);
	double t;
	int success = newton_solve(0.5, f, dfdx, t, 0.0);
	if (t < 0 || t > 1) success = 0;
	// cout << " t: " << t;
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	closest_point = vec(x,y,0.0);
	normal = vec(dy, -dx, 0.0);
	return success;
}
int discrete_airfoil::get_closestpoint_and_normal(vec v, vec& closest_point, vec& normal){
	double d = 1e20;
	double dtemp;
	int min_i;
	for (int i = 0; i != Npoints; i++){
		dtemp = distance(esps[i].pos,v);
		if (dtemp < d){
			d = dtemp;
			min_i = i;
		}
	}
	if (d > 2.0){//manual override if we are far away
		closest_point = esps[min_i].pos;
		normal = esps[min_i].dn;
		return 1;
	}
	int right = 0;
	int left = 0;
	int min_i_plus_1, min_i_minus_1;
        if(loop){
		int min_i_plus_1 = (min_i != Npoints - 1) ? min_i + 1: 0;
		int min_i_minus_1 = (min_i != 0) ? min_i - 1: Npoints - 1;
		//fprintf(stderr, "%d, %d, %d \n", min_i_minus_1, min_i, min_i_plus_1);
		right = solve_closestpoint_and_normal(v, esps[min_i].pos, esps[min_i].tangent, esps[min_i_plus_1].pos, esps[min_i_plus_1].tangent, closest_point, normal);
		if (!right) left = solve_closestpoint_and_normal(v, esps[min_i_minus_1].pos, esps[min_i_minus_1].tangent, esps[min_i].pos, esps[min_i].tangent, closest_point, normal);
		normal.make_unit();
	}
	else{
		int min_i_plus_1 =  min_i + 1;
		int min_i_minus_1 = min_i - 1;
		//fprintf(stderr, "%d, %d, %d \n", min_i_minus_1, min_i, min_i_plus_1);
		if(min_i != Npoints - 1) right = solve_closestpoint_and_normal(v, esps[min_i].pos, esps[min_i].tangent, esps[min_i_plus_1].pos, esps[min_i_plus_1].tangent, closest_point, normal);
		if (min_i != 0 && !right) left = solve_closestpoint_and_normal(v, esps[min_i_minus_1].pos, esps[min_i_minus_1].tangent, esps[min_i].pos, esps[min_i].tangent, closest_point, normal);
		normal.make_unit();
	}
	//cout << "point: " << v << "adjacent point: " << esps[min_i].pos << "closest: " << closest_point << "normal: " << normal << "right: " << right << ", left: " << left << "\n";
	return right + left;
}
