#include "solver.h"
#include <omp.h>
#define timesteppinglocal 0
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
#define fluxFuncName stegerwarming
solver::solver(int N, std::map<string,int> ioData, const flow_props& flow_freestream)
{
	sor = ioData["order"] == 2 ? 1 : 0;
	hor = ioData["higherorder"];
	restart = ioData["restart"];
	SI = ioData["SI"];
	normal = ioData["normal"];
	if(sor) interpolateUpwind = &interpolateSecondOrderUpwind;
	else interpolateUpwind = &interpolateFirstOrderUpwind;
	n_cells_x = N+4;
	n_cells_y = N+4;
	n_cells_z = 5;
	N_nonghost = N*N;
	//make the embedded surface
	if(ioData["naca"]){
		esf = new embsurf_naca();
	}
	else if (ioData["bezier"]){
		esf = new bezier_airfoil();
	}
	else if (ioData["discrete"]){
		esf = new discrete_airfoil(ioData["loop"]);
	}
	else{
		fprintf(stderr,"no embedded surface type specified \n"); 
		exit(-1);
	}

	esf->generate_own_embsurfpoints();
	esf->write_embsurf_to_vtk("post/emb_surf.vtk");
	esf->write_embpoints("emb_points.dat");
	esf->write_sdesign_input("sdesign/emb_points.sdesign");
	cells0 = new cell_threeD[n_cells_x*n_cells_y];
	cells_halfstep = new cell_threeD[n_cells_x*n_cells_y];
	//do some cell filling
	double dL = 3.0/N;
	L = vec(dL, dL, dL);
	const double min_L = min(min(L.x, L.y), L.z);	
	#pragma omp parallel for collapse(2)
	for(int i=0; i<n_cells_x; i++){
		for(int j=0; j<n_cells_y; j++){
				vec pos(L.x*(i-2+0.5), L.y*(j-2+0.5),0);
				cells0[INDX(i,j,n_cells_x)]=cell_threeD(L, pos);
				cells0[INDX(i,j,n_cells_x)].kstar=ioData["kstar"]/100.0;
		}
	}
	int cell_indx = 0;
	for(int i=2; i<n_cells_x-2; i++){
		for(int j=2; j<n_cells_y-2; j++){
				cells0[INDX(i,j,n_cells_x)].indx = cell_indx;
				cell_indx++;
		}
	}
	//Initialise flux objects in an array and link to relevant cells (left and right) by assigning poitners 
	int nfluxes = 2*n_cells_x*n_cells_y  - 7*(n_cells_x + n_cells_y) + 24;
	fluxes = new flux[nfluxes];
	//x-direction fluxes
	#pragma omp parallel for
	for(int i_flux = 0; i_flux < (n_cells_x -1 - 2)*(n_cells_y - 2 - 2); ++i_flux){
		int i = i_flux / (n_cells_y - 2 - 2) + 2;
		int j = i_flux % (n_cells_y - 2 - 2) + 2;
		cells0[INDX(i,j,n_cells_x)].fluxL_x=&fluxes[i_flux];
		cells0[INDX(i-1,j,n_cells_x)].fluxR_x=&fluxes[i_flux];
	}

	#pragma omp parallel for
	for(int i_flux_offset = 0; i_flux_offset < (n_cells_x -2 - 2)*(n_cells_y - 1 - 2); ++i_flux_offset){
		int i = i_flux_offset / (n_cells_y - 1 - 2) + 2;
		int j = i_flux_offset % (n_cells_y - 1 - 2) + 2;
		int i_flux = i_flux_offset + (n_cells_x -1 - 2)*(n_cells_y - 2 - 2);
		cells0[INDX(i,j,n_cells_x)].fluxL_y=&fluxes[i_flux];
		cells0[INDX(i,j-1,n_cells_x)].fluxR_y=&fluxes[i_flux];
		
	}
	// int i_flux = 0;
	// for(int i=2; i<n_cells_x-1; i++){
	// 	for(int j=2; j<n_cells_y-2; j++){
	// 			cells0[INDX(i,j,n_cells_x)].fluxL_x=&fluxes[i_flux];
	// 			cells0[INDX(i-1,j,n_cells_x)].fluxR_x=&fluxes[i_flux];
	// 			i_flux++;
	// 	}
	// }
	// for(int i=2; i<n_cells_x-2; i++){
	// 	for(int j=2; j<n_cells_y-1; j++){
	// 			cells0[INDX(i,j,n_cells_x)].fluxL_y=&fluxes[i_flux];
	// 			cells0[INDX(i,j-1,n_cells_x)].fluxR_y=&fluxes[i_flux];
	// 			i_flux++;
	// 	}
	// }
	// fill with initial conditions and calculate embedded surface
	//Set up flow state object to represent free stream conditions
	cout << "freestream flow \n" << flow_freestream;
	cout << "c_v: " << flow_freestream.gm->get_c_v() << ", R: " << flow_freestream.gm->get_R() << "\n";
	air = *(flow_freestream.gm);
	//fill entire domain with freestream conditions thereby setting intial and boundary conditions
	n_active = 0;
	contains_boundary_idxs_x = new int[(N+4)*(N+4)];
	contains_boundary_idxs_y = new int[(N+4)*(N+4)];
	if (restart){
		fill_cells_from_saved_txt("celldata.rst",&air,(N+4)*(N+4),cells0);
	}
	#pragma omp parallel for collapse(2)
	for(int i=0; i<n_cells_x; i++){
		for(int j=0; j<n_cells_y; j++){
			if(!restart){
				cells0[INDX(i,j,n_cells_x)].flow=flow_freestream;//left
				cells0[INDX(i,j,n_cells_x)].interfL_x=flow_freestream;//left
				cells0[INDX(i,j,n_cells_x)].interfR_x=flow_freestream;//left
				cells0[INDX(i,j,n_cells_x)].interfL_y=flow_freestream;//left
				cells0[INDX(i,j,n_cells_x)].interfR_y=flow_freestream;//left
			}
			cells0[INDX(i,j,n_cells_x)].determineState(esf);
		}
	}
	int ii = 0;
	for(int i=0; i<n_cells_x; i++){
		for(int j=0; j<n_cells_y; j++){
			if (cells0[INDX(i,j,n_cells_x)].is_active) n_active += 1;
			if (cells0[INDX(i,j,n_cells_x)].contains_boundary){
				contains_boundary_idxs_x[ii] = i;
				contains_boundary_idxs_y[ii] = j;
				ii++;
			}
		}
	}
	n_cells_containingboundary = ii;
	#pragma omp parallel for collapse(2)
	for(int i=1; i<n_cells_x-1; i++){
		for(int j=1; j<n_cells_y-1; j++){
			if (cells0[INDX(i,j,n_cells_x)].contains_boundary) cells0[INDX(i,j,n_cells_x)].determine_convex_neighbours(cells0, i, j, n_cells_x);
			cells0[INDX(i,j,n_cells_x)].setup_emb_points(esf, SI);
		}
	}

	if (hor == 2){
		for(int i=10; i<n_cells_x-11; i++){
			for(int j=10; j<n_cells_y-11; j++){
				if (!cells0[INDX(i,j,n_cells_x)].is_active && (cells0[INDX(i+1,j,n_cells_x)].contains_boundary + cells0[INDX(i+1,j,n_cells_x)].is_active))
					cells0[INDX(i,j,n_cells_x)].get_interp_operators(cells0, i, j, n_cells_x, 'E');
				if (!cells0[INDX(i,j,n_cells_x)].is_active && (cells0[INDX(i-1,j,n_cells_x)].contains_boundary + cells0[INDX(i-1,j,n_cells_x)].is_active))
					cells0[INDX(i,j,n_cells_x)].get_interp_operators(cells0, i, j, n_cells_x, 'W');
				if (!cells0[INDX(i,j,n_cells_x)].is_active && (cells0[INDX(i,j+1,n_cells_x)].contains_boundary + cells0[INDX(i,j+1,n_cells_x)].is_active))
					cells0[INDX(i,j,n_cells_x)].get_interp_operators(cells0, i, j, n_cells_x, 'N');
				if (!cells0[INDX(i,j,n_cells_x)].is_active && (cells0[INDX(i,j-1,n_cells_x)].contains_boundary + cells0[INDX(i,j-1,n_cells_x)].is_active))
					cells0[INDX(i,j,n_cells_x)].get_interp_operators(cells0, i, j, n_cells_x, 'S');
			}
		}
	}
	//copy information into cells_halfstep
	#pragma omp parallel for collapse(2)
	for(int i=0; i<n_cells_x; i++){
		for(int j=0; j<n_cells_y; j++){
				cells_halfstep[INDX(i,j,n_cells_x)]=cells0[INDX(i,j,n_cells_x)];
		}
	}
	cout << "percentage active = " << n_active*1.0/N/N << "\n";

}
void solver::reconstruct_and_compute_fluxes(int halfstep){
//reconstruct flow state at cell interfaces in x,y direction for right and left
		cell_threeD* cells;
		if(halfstep) cells = cells0;
		else cells = cells_halfstep;
		#pragma omp parallel for collapse(2)
		for(int i=1; i<n_cells_x-1; ++i){
			for(int j=1; j<n_cells_y-1; ++j){
  					if (cells[INDX(i,j,n_cells_x)].is_active){
		  					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x, interpolateUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x, interpolateUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y, interpolateUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y, interpolateUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);					
					}
			}
		}
		// reconstruct flow state at cell interfaces in x,y direction for right and left using riemann problem if the cell is inactive and adjacent cell is contains_boundary or active
		#pragma omp parallel for collapse(2)
		for(int i=1; i<n_cells_x-1; ++i){
			for(int j=1; j<n_cells_y-1; ++j){
  					if (!cells[INDX(i,j,n_cells_x)].is_active){
	 					if (i!=n_cells_x-1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i+2,j,n_cells_x)],
	 						&cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], cells, n_cells_x,
	 						'E',SI,sor, hor, normal);
						if (i!=1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i-2,j,n_cells_x)],
	 						&cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], cells, n_cells_x,
							'W',SI,sor, hor, normal);
						if (j!=n_cells_y-1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j+2,n_cells_x)],
	 						&cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], cells, n_cells_x,
							'N',SI,sor, hor, normal);
						if (j!=1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j-2,n_cells_x)],
	 						&cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], cells, n_cells_x,
							'S',SI,sor, hor, normal,debugFlag);
					}
			}
		}
		// calculate fluxes based on these interface values, accounting for boundary conditions
		// int i_flux = 0;
		//x-direction fluxes
		#pragma omp parallel for
		for(int i_flux = 0; i_flux < (n_cells_x -1 - 2)*(n_cells_y - 2 - 2); ++i_flux){
			int i = i_flux / (n_cells_y - 2 - 2) + 2;
			int j = i_flux % (n_cells_y - 2 - 2) + 2;
			char BC;
			if (i == 2) BC = 'L'; //left boundary
			else if (i == n_cells_x - 3) BC = 'R';//right boundary
			else BC = 'N';
			if (cells[INDX(i-1,j,n_cells_x)].is_active || cells[INDX(i,j,n_cells_x)].is_active){
				fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, fluxes[i_flux], &air, BC);
			}
		}

		#pragma omp parallel for
		for(int i_flux_offset = 0; i_flux_offset < (n_cells_x -2 - 2)*(n_cells_y - 1 - 2); ++i_flux_offset){
			int i = i_flux_offset / (n_cells_y - 1 - 2) + 2;
			int j = i_flux_offset % (n_cells_y - 1 - 2) + 2;
			int i_flux = i_flux_offset + (n_cells_x -1 - 2)*(n_cells_y - 2 - 2);
			char BC;
			if (j == n_cells_y-3) BC = 'R'; //top boundary
			else if (j == 2) BC = 'L';
			else BC = 'N';
			if (cells[INDX(i,j-1,n_cells_x)].is_active || cells[INDX(i,j,n_cells_x)].is_active){
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, fluxes[i_flux], &air, BC);
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxes[i_flux].momentum.switch_y_x();
			}
		}
}
void solver::run_auxiliary_embedded(int halfstep, double dt, double& CFL){
	if(SI==2){
	cell_threeD* cells;
	cell_threeD* cells_out;
	if(halfstep){
		cells = cells0;
		cells_out = cells_halfstep;
	}
	else{
		cells = cells_halfstep;
		cells_out = cells0;
	}
	average_aux_resid = 0.0;
	for(int ii = 0; ii < n_cells_containingboundary; ++ii){
			int i = contains_boundary_idxs_x[ii]; int j = contains_boundary_idxs_y[ii];
			if (i > 1 && i < n_cells_x-2 && j > 1 && j < n_cells_y-2){
				//assumes active adjacent cells have already been populated
				// populate internal boundary faces
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x,interpolateUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x,interpolateUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y,interpolateUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y,interpolateUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);
				//solves rieman problem for those cells which are inactive
				cells[INDX(i+1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i+1,j+1,n_cells_x)], cells, n_cells_x,'W',normal,hor,cells[INDX(i,j,n_cells_x)].conv_E, debugFlag);
				cells[INDX(i-1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], cells, n_cells_x,'E',normal,hor,cells[INDX(i,j,n_cells_x)].conv_W, debugFlag);
				cells[INDX(i,j+1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i-1,j+1,n_cells_x)], cells, n_cells_x,'S',normal,hor,cells[INDX(i,j,n_cells_x)].conv_N, debugFlag);
				cells[INDX(i,j-1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], cells, n_cells_x,'N',normal,hor, cells[INDX(i,j,n_cells_x)].conv_S, debugFlag);
				//may have to add boundary checks here, but this does the thing for convex cells
				if(cells[INDX(i,j,n_cells_x)].conv_E == 2) cells[INDX(i+1,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)],
						&cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], cells, n_cells_x,
					'W',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_E == 3) cells[INDX(i+1,j,n_cells_x)].interpolate(cells[INDX(i+1,j,n_cells_x)].interfL_x,interpolateUpwind,&cells[INDX(i+2,j,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_W == 2) cells[INDX(i-1,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)],
						&cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], cells, n_cells_x,
						'E',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_W == 3) cells[INDX(i-1,j,n_cells_x)].interpolate(cells[INDX(i-1,j,n_cells_x)].interfR_x,interpolateUpwind,&cells[INDX(i-2,j,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_N == 2) cells[INDX(i,j+1,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)],
						&cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], cells, n_cells_x,
					'S',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_N == 3) cells[INDX(i,j+1,n_cells_x)].interpolate(cells[INDX(i,j+1,n_cells_x)].interfL_y,interpolateUpwind,&cells[INDX(i,j+2,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_S == 2)cells[INDX(i,j-1,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)],
						&cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], cells, n_cells_x,
					'N',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_S == 3) cells[INDX(i,j-1,n_cells_x)].interpolate(cells[INDX(i,j-1,n_cells_x)].interfR_y,interpolateUpwind,&cells[INDX(i,j-2,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				//flux computation
				fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, *(cells[INDX(i,j,n_cells_x)].fluxL_x), &air);
				fluxFuncName(cells[INDX(i,j,n_cells_x)].interfR_x, cells[INDX(i+1,j,n_cells_x)].interfL_x, *(cells[INDX(i,j,n_cells_x)].fluxR_x), &air);
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, *(cells[INDX(i,j,n_cells_x)].fluxL_y), &air);
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].fluxL_y->momentum.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j+1,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxFuncName(cells[INDX(i,j,n_cells_x)].interfR_y, cells[INDX(i,j+1,n_cells_x)].interfL_y, *(cells[INDX(i,j,n_cells_x)].fluxR_y), &air);
				cells[INDX(i,j,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j+1,n_cells_x)].interfL_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].fluxR_y->momentum.switch_y_x();
				//update in time
				double abs_vel_x = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.x);		
				double abs_vel_y = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.y);
				double max_abs_v = max(abs_vel_x, abs_vel_y);
				double c = air.sound_speed(cells[INDX(i,j,n_cells_x)].flow);
				if (timesteppinglocal)	dt=CFL*L.x/(max_abs_v + c);
				else max_a=max(max_a, max_abs_v + c);
				cells[INDX(i,j,n_cells_x)].update_in_time3(cells_out[INDX(i,j,n_cells_x)], dt, CFL,debugFlag);
				average_aux_resid += abs(cells[INDX(i,j,n_cells_x)].max_resid);
			}
	}
	average_aux_resid /= n_cells_containingboundary;
	// cout << "n: " << i_t_aux << "; auxiliary residual: " << average_aux_resid/init_aux_resid << "\n";
	}
}// end embedded weird stuff
void solver::update_in_time(int halfstep, double dt, double& CFL){
	double temp_average_resid = 0;
	#pragma omp parallel for collapse(2) reduction(+:temp_average_resid)
	for(int i=2; i<n_cells_x-2; ++i){
			for(int j=2; j<n_cells_y-2; ++j){
				if(cells0[INDX(i,j,n_cells_x)].is_active){
					//calculate maximum wave speed for each cell and assign dt if local time stepping
					double abs_vel_x = fabs(cells0[INDX(i,j,n_cells_x)].flow.m_vel.x);		
					double abs_vel_y = fabs(cells0[INDX(i,j,n_cells_x)].flow.m_vel.y);
					double max_abs_v = max(abs_vel_x, abs_vel_y);
					double c = air.sound_speed(cells0[INDX(i,j,n_cells_x)].flow);
					if (timesteppinglocal)	dt=CFL*L.x/(max_abs_v + c);
					else max_a=max(max_a, max_abs_v + c);
					if (halfstep) cells0[INDX(i,j,n_cells_x)].update_in_time3(cells_halfstep[INDX(i,j,n_cells_x)],dt,CFL, debugFlag);
					else {
						cells0[INDX(i,j,n_cells_x)].update_in_time(dt, CFL, debugFlag);//new half fluxes should be being used
						temp_average_resid += abs(cells0[INDX(i,j,n_cells_x)].max_resid);
					}
				}			
			}
		}
	average_resid = temp_average_resid/n_active;
}
void solver::perturb_cells(Solution& dW, double eps){
	for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				cells0[INDX(i,j,n_cells_x)].perturb_flow(cells_halfstep[INDX(i,j,n_cells_x)],dW,eps);
			}
		}
}
void solver::update_cells(Solution& dW){
	average_resid = 0;
	for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				cells0[INDX(i,j,n_cells_x)].update_flow(dW);
				double abs_vel_x = fabs(cells0[INDX(i,j,n_cells_x)].flow.m_vel.x);		
				double abs_vel_y = fabs(cells0[INDX(i,j,n_cells_x)].flow.m_vel.y);
				double max_abs_v = max(abs_vel_x, abs_vel_y);
				double c = air.sound_speed(cells0[INDX(i,j,n_cells_x)].flow);
				max_a=max(max_a, max_abs_v + c);
				average_resid += abs(cells0[INDX(i,j,n_cells_x)].max_resid);
			}
		}
	average_resid /= n_active;
}
void solver::compute_flux_vector(Solution& flux_vector, int perturb){
	cell_threeD* cells;
	if(perturb){
		cells = cells_halfstep;
	}
	else{
		cells = cells0;
	}
	for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				if(cells[INDX(i,j,n_cells_x)].is_active){
					cells[INDX(i,j,n_cells_x)].sum_fluxes(flux_vector);
				}
			}
		}
}
void solver::compute_flux_auxiliary(Solution& flux_vector, int perturb){
	if(SI == 2){
	cell_threeD* cells;
	if(perturb){
		cells = cells_halfstep;
	}
	else{
		cells = cells0;
	}

	for(int ii = 0; ii != n_cells_containingboundary; ii++){
			int i = contains_boundary_idxs_x[ii]; int j = contains_boundary_idxs_y[ii];
			if (i > 0 && i < n_cells_x && j > 0 && j < n_cells_y){
				//assumes active adjacent cells have already been populated
				// populate internal boundary faces
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x,interpolateUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x,interpolateUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y,interpolateUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
				cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y,interpolateUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);
				//solves rieman problem for those cells which are inactive
				cells[INDX(i+1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i+1,j+1,n_cells_x)], cells, n_cells_x,'W',normal,hor,cells[INDX(i,j,n_cells_x)].conv_E, debugFlag);
				cells[INDX(i-1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], cells, n_cells_x,'E',normal,hor,cells[INDX(i,j,n_cells_x)].conv_W, debugFlag);
				cells[INDX(i,j+1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i-1,j+1,n_cells_x)], cells, n_cells_x,'S',normal,hor,cells[INDX(i,j,n_cells_x)].conv_N, debugFlag);
				cells[INDX(i,j-1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], cells, n_cells_x,'N',normal,hor, cells[INDX(i,j,n_cells_x)].conv_S, debugFlag);
				//may have to add boundary checks here, but this does the thing for convex cells
				if(cells[INDX(i,j,n_cells_x)].conv_E == 2) cells[INDX(i+1,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)],
						&cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], cells, n_cells_x,
					'W',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_E == 3) cells[INDX(i+1,j,n_cells_x)].interpolate(cells[INDX(i+1,j,n_cells_x)].interfL_x,interpolateUpwind,&cells[INDX(i+2,j,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_W == 2) cells[INDX(i-1,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)],
						&cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], cells, n_cells_x,
						'E',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_W == 3) cells[INDX(i-1,j,n_cells_x)].interpolate(cells[INDX(i-1,j,n_cells_x)].interfR_x,interpolateUpwind,&cells[INDX(i-2,j,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_N == 2) cells[INDX(i,j+1,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)],
						&cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], cells, n_cells_x,
					'S',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_N == 3) cells[INDX(i,j+1,n_cells_x)].interpolate(cells[INDX(i,j+1,n_cells_x)].interfL_y,interpolateUpwind,&cells[INDX(i,j+2,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				if(cells[INDX(i,j,n_cells_x)].conv_S == 2)cells[INDX(i,j-1,n_cells_x)].getExactRiemann(&cells[INDX(i,j,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)],
						&cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], cells, n_cells_x,
					'N',SI,sor, hor, normal,debugFlag,1);
				else if (cells[INDX(i,j,n_cells_x)].conv_S == 3) cells[INDX(i,j-1,n_cells_x)].interpolate(cells[INDX(i,j-1,n_cells_x)].interfR_y,interpolateUpwind,&cells[INDX(i,j-2,n_cells_x)],&cells[INDX(i,j,n_cells_x)]);
				//flux computation
				fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, *(cells[INDX(i,j,n_cells_x)].fluxL_x), &air);
				fluxFuncName(cells[INDX(i,j,n_cells_x)].interfR_x, cells[INDX(i+1,j,n_cells_x)].interfL_x, *(cells[INDX(i,j,n_cells_x)].fluxR_x), &air);
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, *(cells[INDX(i,j,n_cells_x)].fluxL_y), &air);
				cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].fluxL_y->momentum.switch_y_x();
				cells[INDX(i,j,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j+1,n_cells_x)].interfL_y.m_vel.switch_y_x();
				fluxFuncName(cells[INDX(i,j,n_cells_x)].interfR_y, cells[INDX(i,j+1,n_cells_x)].interfL_y, *(cells[INDX(i,j,n_cells_x)].fluxR_y), &air);
				cells[INDX(i,j,n_cells_x)].interfR_y.m_vel.switch_y_x();
				cells[INDX(i,j+1,n_cells_x)].interfL_y.m_vel.switch_y_x();
				cells[INDX(i,j,n_cells_x)].fluxR_y->momentum.switch_y_x();
				//update in time
				cells[INDX(i,j,n_cells_x)].sum_fluxes(flux_vector);
			}
		}
	}
}// end embedded weird stuff
void solver::write_cells_no_ghost(cell_threeD* cells_no_ghost){
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-2; j++){
			cells_no_ghost[INDX(i-2,j-2,n_cells_x-4)]=cells0[INDX(i,j,n_cells_x)];
		}
	}
}
vec solver::calculate_force_hor(string filename){
	return calculate_force2(cells0, n_cells_x, n_cells_y, L, esf->esps, esf->Npoints, filename);
}
vec solver::calculate_force_zero(){
	return calculate_force0(cells0, n_cells_x, n_cells_y);
}

	
