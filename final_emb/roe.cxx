#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "thermo.h"
#include "matrix.h"
#include "flow_threeD.h"
#define eps 1e-1
using namespace std;
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int roe(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel, char BC = 'N'){
    double g = gmodel->m_gamma;
    double gmin1 = g - 1;
    double sq_rho_R = sqrt(f_R.m_rho);
    double sq_rho_L = sqrt(f_L.m_rho);
    double u_RL = (sq_rho_R*f_R.m_vel.x + sq_rho_L*f_L.m_vel.x)/(sq_rho_R + sq_rho_L);
    double h_RL = (sq_rho_R*f_R.m_h + sq_rho_L*f_L.m_h)/(sq_rho_R + sq_rho_L);

    vector3 W_L(f_L.m_rho, f_L.m_rho * f_L.m_vel.x, f_L.get_total_energy());
    vector3 W_R(f_R.m_rho, f_R.m_rho * f_R.m_vel.x, f_R.get_total_energy());
    vector3 deltaW = subtract(W_R, W_L);
    vector3 FluxVec;
    double c, M;
    double c_RL = sqrt((gmin1)*(h_RL - 0.5*u_RL*u_RL));
    double M_RL = u_RL/c_RL;
    if (BC == 'L'){
        c = gmodel->sound_speed(f_R);
        M = f_R.m_vel.x/c;
        //if (M < 1.0) cout << "left BC, M: " << M << "\n";

    }
    else if (BC == 'R'){
        c = gmodel->sound_speed(f_L);
        M = f_L.m_vel.x/c;
        //if (M < 1) cout << M << "shitcakes \n";
    }
    else{
        c = c_RL;
        M = M_RL;
    }
    // quick method of computing fluxes
        if (M > 1){
            vector3 Flux_L(f_L.m_rho * f_L.m_vel.x, 
               f_L.m_rho * f_L.m_vel.x * f_L.m_vel.x + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_vel.x);
            FluxVec = Flux_L;
        }
        else if ( M > 0.0){
            vector3 Flux_L(f_L.m_rho * f_L.m_vel.x, 
               f_L.m_rho * f_L.m_vel.x * f_L.m_vel.x + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_vel.x);
            double eig3 = u_RL - c_RL;
            eig3 = -sqrt(eig3*eig3 + eps*eps) + eps; //regularization
            vector3 l3((gmin1)*M_RL*M_RL/4 + 0.5*M_RL,
                (-0.5 - 0.5*M_RL*(gmin1))/c_RL,
                gmin1/2.0/c_RL/c_RL);
            vector3 r3(1,
                u_RL - c_RL,
                c_RL*c_RL/gmin1 - u_RL*(2*c_RL - u_RL)/2.0);
            FluxVec = add( Flux_L , r3.scalarMult(eig3*dot(l3, deltaW)));
        }
        else if (M > - 1.0){
            vector3 Flux_L(f_L.m_rho * f_L.m_vel.x, 
               f_L.m_rho * f_L.m_vel.x * f_L.m_vel.x + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_vel.x);
            double eig3 = u_RL - c_RL;
            eig3 = -sqrt(eig3*eig3 + eps*eps) + eps; //regularization
            vector3 l3((gmin1)*M_RL*M_RL/4 + 0.5*M_RL,
                (-0.5 - 0.5*M_RL*(gmin1))/c_RL,
                gmin1/2.0/c_RL/c_RL);
            vector3 r3(1,
                u_RL - c_RL,
                c_RL*c_RL/gmin1 - u_RL*(2*c_RL - u_RL)/2.0);
            FluxVec = add( Flux_L , r3.scalarMult(eig3*dot(l3, deltaW)));
            double eig2 = u_RL;
            eig2 = -sqrt(eig2*eig2 + eps*eps) + eps;
            vector3 l2(1.0 - gmin1*M_RL*M_RL/2.0,
                gmin1*M_RL/c_RL,
                -gmin1/c_RL/c_RL);
            vector3 r2(1.0,
                u_RL,
                u_RL*u_RL/2.0);
            FluxVec = add( FluxVec , r2.scalarMult(eig2*dot(l2, deltaW)));
            // vector3 Flux_R(f_R.m_rho * f_R.m_vel.x, 
            //     f_R.m_rho * f_R.m_vel.x * f_R.m_vel.x + f_R.m_p, 
            //     (f_R.get_total_energy() + f_R.m_p) * f_R.m_vel.x);
            // double eig1 = u_RL + c_RL;
            // eig1 = sqrt(eig1*eig1 + eps*eps) - eps;
            // vector3 l1((gmin1)*M_RL*M_RL/4 - 0.5*M_RL,
            //     (0.5 - 0.5*M_RL*(gmin1))/c_RL,
            //     gmin1/2.0/c_RL/c_RL);
            // vector3 r1(1,
            //     u_RL + c_RL,
            //     c_RL*c_RL/gmin1 + u_RL*(2*c_RL + u_RL)/2.0);
            // FluxVec = subtract( Flux_R , r1.scalarMult(eig1*dot(l1, deltaW)));
        }
        else {
            vector3 Flux_L(f_L.m_rho * f_L.m_vel.x, 
               f_L.m_rho * f_L.m_vel.x * f_L.m_vel.x + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_vel.x);
            double eig3 = u_RL - c_RL;
            eig3 = -sqrt(eig3*eig3 + eps*eps) + eps; //regularization
            vector3 l3((gmin1)*M_RL*M_RL/4 + 0.5*M_RL,
                (-0.5 - 0.5*M_RL*(gmin1))/c_RL,
                gmin1/2.0/c_RL/c_RL);
            vector3 r3(1,
                u_RL - c_RL,
                c_RL*c_RL/gmin1 - u_RL*(2*c_RL - u_RL)/2.0);
            FluxVec = add( Flux_L , r3.scalarMult(eig3*dot(l3, deltaW)));
            double eig2 = u_RL;
            eig2 = -sqrt(eig2*eig2 + eps*eps) + eps;
            vector3 l2(1.0 - gmin1*M_RL*M_RL/2.0,
                gmin1*M_RL/c_RL,
                -gmin1/c_RL/c_RL);
            vector3 r2(1.0,
                u_RL,
                u_RL*u_RL/2.0);
            FluxVec = add( FluxVec , r2.scalarMult(eig2*dot(l2, deltaW)));
            vector3 FluxVec(f_R.m_rho * f_R.m_vel.x, 
                f_R.m_rho * f_R.m_vel.x * f_R.m_vel.x + f_R.m_p, 
                (f_R.get_total_energy() + f_R.m_p) * f_R.m_vel.x);
            double eig1 = u_RL + c_RL;
            eig1 = -sqrt(eig1*eig1 + eps*eps) + eps;
            vector3 l1((gmin1)*M_RL*M_RL/4 - 0.5*M_RL,
                (0.5 - 0.5*M_RL*(gmin1))/c_RL,
                gmin1/2.0/c_RL/c_RL);
            vector3 r1(1,
                u_RL + c_RL,
                c_RL*c_RL/gmin1 + u_RL*(2*c_RL + u_RL)/2.0);
            // vector3 Flux_R(f_R.m_rho * f_R.m_vel.x, 
            //     f_R.m_rho * f_R.m_vel.x * f_R.m_vel.x + f_R.m_p, 
            //     (f_R.get_total_energy() + f_R.m_p) * f_R.m_vel.x);
            // FluxVec = Flux_R; 
        }
    double vel_y, vel_z;
    //Unpack fluxes into flux object
    if (face.mass > 0){
        vel_y = f_L.m_vel.y;
        vel_z = f_L.m_vel.z;
    }
    else {
        vel_y = f_R.m_vel.y;
        vel_z = f_R.m_vel.z;
    }
    face.mass = FluxVec.x[0];
    face.momentum.x = FluxVec.x[1];
    face.momentum.y = face.mass*vel_y;
    face.momentum.z = face.mass*vel_z;
    face.total_energy = FluxVec.x[2];

   return 1;
}   /* end of roe() */

/*--------------------------------------------------------------*/
