#include <cmath>
#include <fstream>
#include "geom.h"
using namespace std;
#define a (-0.1)
#define b 1.0
//for straight lines of the form ax + by + c = 0
double get_h_fromfile(string filename){
	ifstream inFile;
	inFile.open(filename);
	double h;
	inFile >> h;
	return h;
}
double embsurf(vec v){
	double c = -get_h_fromfile("height.dat");
	double phi = a * v.x + b * v.y + c;
	return phi;
}
vec get_closestpoint(vec v){
	double c = -get_h_fromfile("height.dat");
	return vec((b*(b*v.x - a*v.y) - a*c)/(a*a + b*b),
		(a*(-b*v.x + a*v.y)-b*c)/(a*a + b*b),
		0.0);
}
vec get_intersectedpoint(vec va, vec vb){
	double c = -get_h_fromfile("height.dat");
	double ny = vb.y - va.y; double nx = vb.x - va.x;
	return vec((b*(ny*va.x - nx*va.y) - nx*c)/(nx*a + ny*b),
		(a*(-ny*vb.x + nx*vb.y)-c*ny)/(nx*a + ny*b),
	0.0);
}
vec get_normal(vec va){
	return vec(-a, -b, 0);
}