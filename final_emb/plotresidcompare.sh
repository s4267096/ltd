gnuplot -e "set key on;
 set xlabel 'No. Iterations'; set ylabel 'Residual'; set logscale y 10; set title 'Residual development under different time stepping'; 
 plot 'residualglobaldt.dat' using 1:2 title 'global dt' lc 2, 
 'residuallocaldt.dat' using 1:2 title 'local dt' lc 6,;
 set term png; 
 set output 'resid.png'; replot; set term x11"

