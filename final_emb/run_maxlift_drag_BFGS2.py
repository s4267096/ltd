import sys
import numpy as np
import os
from math import *
from BFGS_B import *
def read_file_if_exists(filename):
    while not os.path.exists(filename):
        time.sleep(1)
    if os.path.isfile(filename):
        # read file
    else:
        raise ValueError("%s isn't a file!" % file_path)
    return np.genfromtxt(filename)
def objective_gradient(x):
    alpha = 2.0/180.0*pi;
    eps = 1e-6;
    k = 1e-6;
    x = np.array(x)[:,0]
    t = x[0]; c = x[1];
    #Perturb t
    with open('f_t/thickness.dat','w') as file1:
        file1.write(str(t + eps))
    with open('f_t/camber.dat','w') as file1:
        file1.write(str(c))
    os.system('rm f_t/force_output.dat')
    os.system('cp InletFile FluidFile cutoff.dat jho_fiver.out f_t/.')
    os.system('qsub -q sandybridge ./run_t.sh >& consoleout')
    #Perturb c
    with open('f_c/thickness.dat','w') as file1:
        file1.write(str(t))
    with open('f_c/camber.dat','w') as file1:
        file1.write(str(c + eps))
    os.system('rm f_c/force_output.dat')
    os.system('cp InletFile FluidFile cutoff.dat jho_fiver.out f_c/.')
    os.system('qsub -q sandybridge ./run_c.sh >& consoleout')
    #Main Simulation
    with open('thickness.dat','w') as file1:
        file1.write(str(t))
    with open('camber.dat','w') as file2:
        file2.write(str(c))
    os.system('qsub -q sandybridge ./run.sh >& consoleout')
    #Force0
    F = read_file_if_exists('force_output.dat')
    D = F[0]*cos(alpha) + F[1]*sin(alpha);
    L = -F[0]*sin(alpha) + F[1]*cos(alpha);
    #perturb t
    F = read_file_if_exists('f_t/force_output.dat')
    Dt = F[0]*cos(alpha) + F[1]*sin(alpha);
    Lt = -F[0]*sin(alpha) + F[1]*cos(alpha);
    #perturb c
    F = read_file_if_exists('f_c/force_output.dat')
    Dc = F[0]*cos(alpha) + F[1]*sin(alpha);
    Lc = -F[0]*sin(alpha) + F[1]*cos(alpha);
    #gradient calculation
    dD = (Dc - D)/eps; dL = (Lc - L)/eps;
    df_c = (L*dD - D*dL)/L/L
    dD = (Dt - D)/eps; dL = (Lt - L)/eps;
    df_t = (L*dD - D*dL)/L/L
    f = D/L + k/t/t/t;
    df_t = df_t - k/t/t; 
    return (f,np.matrix([df_t, df_c]).T)
#solve optimization problem
ic = np.array([0.12, 0.0])
bl = np.zeros(2)
bu = np.zeros(2)
bl[0] = 0.0
bu[0] = 1e6
bl[1] = 0.0
bu[1] = 0.1
bl = np.matrix(bl).T
bu = np.matrix(bu).T
xStar, fStar = BFGS_B(objective_gradient, np.matrix(ic).T, bl, bu, 0.01)
print("xStar", xStar)
