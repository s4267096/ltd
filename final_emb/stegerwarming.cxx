/** \file stegerwarming.cxx
**/
#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include "thermo.h"
using namespace std;
/*------------------------------------------------------------*/
//steger warming in 2 dimesnsions with a structured cartesian grid aligned in x-y directions

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int stegerwarming(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel, char BC = 'N'){// char argument is a dummy from roe
    double eps=1.0;
    double rL, rR;
    double pL, pR;
    double uL, uR;
    double vL, vR;
    double cL, cR;
    double ML, MR;

    gas_model m_gas_model = *gmodel;
    double g = gmodel->m_gamma;
    rL = f_L.m_rho;
    pL = f_L.m_p;
    uL = f_L.m_vel.x;
    vL = f_L.m_vel.y;
    cL = m_gas_model.sound_speed(f_L);

    rR = f_R.m_rho;
    pR = f_R.m_p;
    uR = f_R.m_vel.x;
    vR = f_R.m_vel.y;
    cR = m_gas_model.sound_speed(f_R);
    double gmin1 = g - 1;

    ML = uL/cL; MR = uR/cR;
    face.mass = 0; face.momentum.x = 0; face.momentum.y = 0; face.momentum.z = 0; face.total_energy = 0;
    double lam1; double lam3; double lam4;//lam2 is a duplicate of lam1
    if (MR < -1){
        lam1 = uR;
        lam3 = uR + cR;
        lam4 = uR - cR;
        lam1 = 0.5*rR/g*(eps - sqrt(lam1*lam1 + eps * eps));
        lam3 = 0.5*rR/g*(eps - sqrt(lam3*lam3 + eps * eps));
        lam4 = 0.5*rR/g*(eps - sqrt(lam4*lam4 + eps * eps));
        face.mass += 2*gmin1*lam1 + lam3 + lam4;
        face.momentum.x += 2*gmin1*lam1*uR + lam3*(uR + cR) + lam4*(uR - cR);
        face.momentum.y += 2*gmin1*lam1*vR + lam3*vR + lam4*vR;
        face.total_energy += gmin1*lam1*(uR*uR + vR*vR) + 0.5*lam3*((uR + cR)*(uR + cR) + vR*vR) + 0.5*lam4*((uR - cR)*(uR - cR) + vR*vR)
                            +(3-g)*(lam3 + lam4)*cR*cR/2/gmin1;
        // face.mass += rR*uR;
        // face.momentum.x += rR*uR*uR + pR;
        // face.momentum.y += rR*uR*vR;
        // face.total_energy += (f_R.get_total_energy() + pR)*uR;
    }
    else if (MR < 0){
        lam1 = uR;
        lam4 = uR - cR;
        lam1 = 0.5*rR/g*(eps - sqrt(lam1*lam1 + eps * eps));
        lam4 = 0.5*rR/g*(eps - sqrt(lam4*lam4 + eps * eps));
        face.mass += 2*gmin1*lam1 + lam4;
        face.momentum.x += 2*gmin1*lam1*uR + lam4*(uR - cR);
        face.momentum.y += 2*gmin1*lam1*vR + lam4*vR;
        face.total_energy += gmin1*lam1*(uR*uR + vR*vR) + 0.5*lam4*((uR - cR)*(uR - cR) + vR*vR)
                            +(3-g)*lam4*cR*cR/2/gmin1;
    }
    else if (MR < 1){
        lam4 = uR - cR;
        lam4 = 0.5*rR/g*(eps - sqrt(lam4*lam4 + eps * eps));
        face.mass += lam4;
        face.momentum.x += lam4*(uR - cR);
        face.momentum.y += lam4*vR;
        face.total_energy += 0.5*lam4*((uR - cR)*(uR - cR) + vR*vR)
                            +(3-g)*lam4*cR*cR/2/gmin1;

    }
    if (ML > 1){
        lam1 = uL;
        lam3 = uL + cL;
        lam4 = uL - cL;
        lam1 = 0.5*rL/g*(-eps + sqrt(lam1*lam1 + eps * eps));
        lam3 = 0.5*rL/g*(-eps + sqrt(lam3*lam3 + eps * eps));
        lam4 = 0.5*rL/g*(-eps + sqrt(lam4*lam4 + eps * eps));
        face.mass += 2*gmin1*lam1 + lam3 + lam4;
        face.momentum.x += 2*gmin1*lam1*uL + lam3*(uL + cL) + lam4*(uL - cL);
        face.momentum.y += 2*gmin1*lam1*vL + lam3* vL + lam4*vL;
        face.total_energy += gmin1*lam1*(uL*uL + vL*vL) + 0.5*lam3*((uL + cL)*(uL + cL) + vL*vL) + 0.5*lam4*((uL - cL)*(uL - cL) + vL*vL)
                            +(3-g)*(lam3 + lam4)*cL*cL/2/gmin1;
        // face.mass += rL*uL;
        // face.momentum.x += rL*uL*uL + pL;
        // face.momentum.y += rL*uL*vL;
        // face.total_energy += (f_L.get_total_energy() + pL)*uL;
    }
    else if (ML > 0.0){
        lam1 = uL;
        lam3 = uL + cL;
        lam1 = 0.5*rL/g*(-eps + sqrt(lam1*lam1 + eps * eps));
        lam3 = 0.5*rL/g*(-eps + sqrt(lam3*lam3 + eps * eps));
        face.mass += 2*gmin1*lam1 + lam3;
        face.momentum.x += 2*gmin1*lam1*uL + lam3*(uL + cL);
        face.momentum.y += 2*gmin1*lam1*vL + lam3*vL;
        face.total_energy += gmin1*lam1*(uL*uL + vL*vL) + 0.5*lam3*((uL + cL)*(uL + cL) + vL*vL)
                            +(3-g)*lam3*cL*cL/2/gmin1;
                            

    }
    else if (ML > -1){
        lam3 = uL + cL;
        lam3 = 0.5*rL/g*(-eps + sqrt(lam3*lam3 + eps * eps));
        face.mass += lam3;
        face.momentum.x += lam3*(uL + cL);
        face.momentum.y += lam3*vL;
        face.total_energy += 0.5*lam3*((uL + cL)*(uL + cL) + vL*vL)+(3-g)*lam3*cL*cL/2/gmin1;

    }
    return 1;
}   /* end of stegerwarming() */

/*--------------------------------------------------------------*/
