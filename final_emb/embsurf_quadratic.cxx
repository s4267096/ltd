#include <cmath>
#include <fstream>
#include <functional>
#include "geom.h"
#include "numerical.h"
using namespace std;
#define a 0.05
#define b 0.05
double get_h_fromfile(string filename){
	ifstream inFile;
	inFile.open(filename);
	double h;
	inFile >> h;
	return h;
}
double embsurf(vec v){
	double c = get_h_fromfile("quad_c.dat");
	double phi = v.y - a * v.x * v.x - b * v.x - c;
	return phi;
}
vec get_closestpoint(vec v){
	double c = get_h_fromfile("quad_c.dat");
	double coeffs[4] = {-2*a*a, -3*a*b, (1-2*a*(c- v.y)-b*b), -v.x -b*(c - v.y)};
	function<double(double)> f = bind(cubic, coeffs,  placeholders::_1);
	function<double(double)> fdash = bind(cubic_deriv, coeffs, placeholders::_1);
	double x = newton_solve(v.x, f, fdash);
	// double x = v.x;
	// cout << "x: " << x << "\n";
	return vec(x, a*x*x + b*x + c, 0.0);
}
vec get_intersectedpoint(vec va, vec vb){
	double c = get_h_fromfile("quad_c.dat");
	if (abs(va.y - vb.y) < 1e-4){
		double x;
		if ((vb.x - va.x)*a > 0) x = (-b + sqrt(b*b - 4*a*(c - va.y))/(2*a));
		else x = (-b - sqrt(b*b - 4*a*(c - va.y))/(2*a));
		return vec(x, a*x*x + b*x + c, 0.0);
	}
	else if (abs(va.x - vb.x) < 1e-4){
		return vec(va.x, a * va.x * va.x + b * va.x + c, 0.0);
	}
	else{
		fprintf(stderr, "mesh not cartesian, get_intersected_point for quadratic points not implemented yet, exiting\n");
		exit(-1);
	}
}
vec get_normal(vec v){
	return vec(2 * a * v.x + b, -1.0, 0);
}
