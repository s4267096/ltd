//some classes to represent 1D flow
#ifndef FLOW
#define FLOW
#include <iostream>
#include "geom.h"
using namespace std;
class gas_model;
class flow_props{
	public:
		flow_props();
		// flow_props(const flow_props&);
		vec m_vel; 
		double m_rho, m_e, m_p, m_T, m_h;
		//note that m_e is internal energy, and m_h is total stagnational enthalpy
		gas_model* gm;
		double get_tke();
		double get_total_energy();
		vec get_momentum();
		void write_to_file(string);
		flow_props getExactRiemann(vec, int = 0);
		void non_dimensionalise(const flow_props&);
};	
class flux{
		public:
		double mass;
		vec momentum;
		double total_energy;
		double tke;
		double omega;
		flux();
		void multiply_and_add(flux&, double);
		void operator=(double);
};
flow_props flow_props_from_rho_T(double, double, vec, gas_model*);
flow_props flow_props_from_rho_p(double, double, vec, gas_model*);
flow_props flow_props_from_p_T_M_alpha(double, double, double,double, gas_model*);
flow_props mirror_flow_along_y(flow_props);
ostream& operator<<(ostream&, const flow_props&);
ostream& operator<<(ostream&, const flux&);
#endif