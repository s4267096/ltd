#ifndef CELL
#define CELL
#include <iostream>
#include "geom.h"
#include "embsurf.h"
#include "gmres/solution.h"
// #include "int_emb.h"
using namespace std;
using interpFunc = double (*)(double, double, double);
using levelsetFunc = double (*)(vec);
using surrogatePointFunc = vec (*)(vec, vec);
using closestPointFunc = vec (*)(vec);
using normalFunc = vec (*)(vec);
class cell_threeD{
	public:
		flow_props flow;//flow properties at center
		//reconstructed flow interfaces
		flow_props interfR_x;
		flow_props interfL_x;
		flow_props interfR_y;
		flow_props interfL_y;
		flow_props interfR_z;
		flow_props interfL_z;
		//pointers to relevant fluxes
		flux* fluxL_x;
		flux* fluxR_x;
		flux* fluxL_y;
		flux* fluxR_y;
		flux* fluxL_z;
		flux* fluxR_z;
		//source term
		flux source;
		//geometric information about the cell
		vec pos;
		vec area;
		vec L;
		vec nodes[4];
		vec flux_point_L_x;
		vec flux_point_R_x;
		vec flux_point_L_y;
		vec flux_point_R_y;
		// some more incofmration about the closest and surrogate points
		vec closest_point_L_x;
		vec closest_point_R_x;
		vec closest_point_L_y;
		vec closest_point_R_y;
		int cl_success_L_x;
		int cl_success_R_x;
		int cl_success_L_y;
		int cl_success_R_y;
		vec normal_L_x;
		vec normal_R_x;
		vec normal_L_y;
		vec normal_R_y;
		// class for interpolating
		double op_L_x[200];
		double op_R_x[200];
		double op_L_y[200];
		double op_R_y[200];
		int n_stencil = 3;
		double kstar;
		int ii;// location of cell
		int jj;
		int indx=-1;
		// methods
		void get_interp_operators(cell_threeD*, int, int, int, char);
		flow_props interpolate(cell_threeD*, flow_props, int, char);

		//information related to the location of the surrogate surface
		double min_dist_surrogate; //if cell is active gives the closest point, if cell is inactive/contains boundary gives how far you need to move it to become active, generally measures how close you are to changing status
		// min_dist_surrogate basically gives an indication of how close a cell is to changing status (if it is closer to zero it is closer to changing)
		//other information
		double min_lose_convex;
		// in the case of a cell that is "constains_boudnary_convex" it will lose this status if any of the active cells next to it  become occluded
		double max_resid;//residual to measure steady convergence
		double get_volume();
		cell_threeD();		
		cell_threeD(vec, vec);
		//generic second order reconstruction function accepting an interpolation function
		void interpolate(flow_props&, interpFunc, cell_threeD*, cell_threeD*);
		void update_in_time(double, double&, int = 0);
		cell_threeD update_in_time2(double);//returns a copy of the cell for second order RK
		void update_in_time3(cell_threeD&, double, double&, int = 0);//modifies the flow in a cell for second order RK
		void perturb_flow(cell_threeD&, Solution&, double);
		void update_flow(Solution&);
		void sum_fluxes(Solution&);
		//active or inactive
		int is_active = 1;
		int contains_boundary = 0;
		void determineState(embsurf*);
		void setup_emb_points(embsurf*, int);
		void getExactRiemann(cell_threeD*, cell_threeD*,
			cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, int,
			char, int, int, int, int, int = 0, int = 0);
		void getExactRiemannAux(cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, int,
			char, int, int, int = 0, int = 0);
		void determine_convex_neighbours(cell_threeD*, int, int, int); // determines convex neighbours
		// indicator for what to do with cells containing boundary -- 1 -- aux, 2-- exact riemann, 3 - interpolate
		int conv_E = 0;
		int conv_W = 0;
		int conv_S = 0;
		int conv_N = 0;
	private:
		void get_areas();
};
ostream& operator<<(ostream& info,const cell_threeD&);
flow_props hor_interpolatestate(cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, cell_threeD*, char, flow_props&, int = 0);
void interpolateState(flow_props&, const flow_props&, const flow_props&, vec, vec, vec, int = 0); //for interpolating a riemann state back to a flux interface
void interpolateState(flow_props&, double, double, const flow_props&, const flow_props&); //for interpolating a riemann state back to a flux interface
#endif