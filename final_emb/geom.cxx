#include <cmath>
#include "geom.h"
using namespace std;
//a simple class for three dimensional vectors in x, y, z
double vec::mag() const {
	return sqrt(x*x + y*y + z*z);
}
vec::vec()
	:x{0}, y{0}, z{0}
{
}

vec::vec(double xx, double yy, double zz)
	:x{xx}, y{yy}, z{zz}
{
}
void vec::make_unit(){
	double mag = this->mag();
	x /= mag;
	y /= mag;
	z /= mag;
}
void vec::switch_y_x(){
	double a=x;
	x=y;
	y=a;
}
void vec::switch_z_x(){
	double a=x;
	x=z;
	z=a;
}
double distance(vec a, vec b){
	return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}
ostream& operator<<(ostream& info, const vec c){
	info << "{" << c.x << ", " << c.y << ", " << c.z <<"}";
	return info;
}
vec vec::operator+(const vec& b){
	return vec(x + b.x, y + b.y, z + b.z);
}
vec vec::operator-(const vec& b){
	return vec(x - b.x, y - b.y, z - b.z);
}
vec vec::operator*(const double k){
	return vec(x*k, y*k, z*k);
}

void vec::operator+=(const vec& b){
	x += b.x;
	y += b.y;
	z += b.z;
}
double dot(vec a, vec b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}
vec cross(vec a, vec b){
	return vec(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - b.x*a.y);
}
double get_gradient(vec a, vec b){
	return (b.y - a.y)/(b.x - a.x);
}
vec get_edge_point_x(vec flux_point, double m, double x){
	return vec(x, flux_point.y + m*(x - flux_point.x),0.0);
}
vec get_edge_point_y(vec flux_point, double m, double y){
	return vec(flux_point.x + (y - flux_point.y)/m, y, 0.0);
}

void get_transpose(vec x, vec y, vec z, vec& xdash, vec& ydash, vec& zdash){
	xdash.x  = x.x; xdash.y = y.x; xdash.z = z.y;
	ydash.x = x.y; ydash.y = y.y; ydash.z = y.z;
	zdash.x = x.z; zdash.y = y.z; zdash.z = z.z;
}
