import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
rc('text',usetex = True)
dat0 = np.genfromtxt("SI0_bigdomain_naca/p_vs_embsurfheight.dat");
dat1 = np.genfromtxt("SI1_bigdomain_naca/p_vs_embsurfheight.dat");
dat2 = np.genfromtxt("SI2_bigdomain_naca/p_vs_embsurfheight.dat");
plt.plot(dat0[:,0],dat0[:,1],label='SI0')
plt.plot(dat1[:,0],dat1[:,1],label='SI1')
plt.plot(dat2[:,0],dat2[:,1],label='SI2')
plt.xlabel('height of embedded surface')
plt.ylabel('pressure (MPa) at selected node')
plt.legend()
plt.savefig('nacacomparison_bigdomain',dpi=600)
plt.show()