#ifndef FORCE
#define FORCE
#include <iostream>
#include "thermo.h"
#include "cell.h"
#include "esp.h"
using namespace std;
vec calculate_force0(cell_threeD*, int, int);
vec calculate_force2(cell_threeD*, int, int, vec, embsurfpoint*, int, string = "");
#endif