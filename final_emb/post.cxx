#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "post.h"
#include "geom.h"
#define INDX(x,y,n_x) (((n_x)*(x) + (y)))
void write_to_vtk(int n_x, int n_y, int n_z, double dx, double dy, double dz, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	int n_points = (n_x + 1)*(n_y + 1)*(n_z +1);
	m_file<<"POINTS " << n_points << " float\n";
	for(int i = 0; i!=n_x+1; i++){
		for(int j = 0; j!=n_y+1; j++){
			for(int k = 0; k!=n_z+1; k++){
				m_file<<setw(8)<<i*dx;
				m_file<<" ";
				m_file<<setw(8)<<j*dy;
				m_file<<" ";
				m_file<<setw(8)<<k*dz;
				m_file<<"\n";
			}
		}
	}
	int n_zy_one = (n_z + 1)*(n_y+1);
	int n_cells = n_x*n_y*n_z;
	m_file << "CELLS " << n_cells << " " << 9*n_cells << "\n";
	for(int i = 0; i!=n_x; i++){
		for(int j = 0; j!=n_y; j++){
			for(int k = 0; k!=n_z; k++){
				m_file << "8 " << k +j*(n_z+1) + i*n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 + n_zy_one << "\n";
			}
		}
	}
	m_file<<"CELL_TYPES "<< n_cells << "\n";
	for(int i = 0; i!=n_cells; i++){
		m_file <<"12\n";
	}
}
void write_cell_data(cell_threeD* cells, const int n_cells, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out | ios::app);
		m_file<<"CELL_DATA "<< n_cells <<"\n";
		m_file<<"SCALARS pressure float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_p<<"\n";
		m_file<<"SCALARS Temperature float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_T<<"\n";
		m_file<<"SCALARS rho float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_rho<<"\n";
		m_file<<"VECTORS vel float\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_vel.x<<" "<<(cells+i)->flow.m_vel.y<<" "<<(cells+i)->flow.m_vel.z<<"\n";
		// m_file<<"VECTORS closest_L_x float\n";
		// for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->closest_point_L_x.x - (cells+i)->flux_point_L_x.n_x<<" "<<(cells+i)->closest_point_L_x.y - (cells+i)->flux_point_L_x.y<<" "<<(cells+i)->closest_point_L_x.z - (cells+i)->flux_point_L_x.z<<"\n";
		m_file<<"SCALARS M float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) {
			double M  = (cells+i)->flow.m_vel.mag()/(cells+i)->flow.gm->sound_speed((cells+i)->flow);
			m_file<<M<<"\n";
		}
	//	m_file<<"SCALARS residual float 1\n";
	//	m_file<<"LOOKUP_TABLE default\n";
	//	for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->max_resid<<"\n";
		m_file<<"SCALARS status float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) {
			if ((cells + i)->contains_boundary) m_file<<0.5<<"\n";
			else m_file<<(cells+i)->is_active<<"\n";
		}
		m_file<<"SCALARS min_dist_surrogate float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->min_dist_surrogate<<"\n";
		m_file<<"SCALARS min_lose_convex float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->min_lose_convex<<"\n";
		m_file<<"SCALARS conv_S float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_S<<"\n";
		m_file<<"SCALARS conv_N float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_N<<"\n";
		m_file<<"SCALARS conv_E float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_E<<"\n";
		m_file<<"SCALARS conv_W float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_W<<"\n";

}
void save_cell_data(cell_threeD* cells, const int n_cells, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		m_file.precision(17);
		for(int i = 0; i!=n_cells; i++) {
			m_file << cells[i].flow.m_rho << " "
			<< cells[i].flow.m_vel.x << " "
			<< cells[i].flow.m_vel.y << " "
			<< cells[i].flow.m_vel.z << " "
			<< cells[i].flow.m_p << "\n";
		}
}
void fill_cells_from_saved_txt(string filename, gas_model* gm_in, const int n_cells, cell_threeD* cells){
	ifstream inFile;
	ifstream file("celldata.rst");
	string line;
	int i = 0;
	while(std::getline(file, line))
	{	
		stringstream lineStream(line);
		lineStream >> cells[i].flow.m_rho;
		lineStream >> cells[i].flow.m_vel.x;
		lineStream >> cells[i].flow.m_vel.y;
		lineStream >> cells[i].flow.m_vel.z;
		lineStream >> cells[i].flow.m_p;
		cells[i].flow.gm = gm_in;
		gm_in->update_from_rho_p(cells[i].flow);
		cells[i].interfL_x = cells[i].flow;
		cells[i].interfR_x = cells[i].flow;
		cells[i].interfL_y = cells[i].flow;
		cells[i].interfR_y = cells[i].flow;
		i++;
		if (i == n_cells) break;
	}
	cout << "finished filling " << i + 1 << " cells \n";
}
void write_cell_data(cell_threeD* cells, const flow_props& f_ref, const int n_cells, string filename){
		double rho_ref = f_ref.m_rho;
		double u_ref = f_ref.m_vel.mag();
		double T_ref = u_ref*u_ref/f_ref.gm->get_c_v();
		double p_ref = rho_ref*u_ref*u_ref;
		ofstream m_file;
		m_file.open(filename, ios::out | ios::app);
		m_file<<"CELL_DATA "<< n_cells <<"\n";
		m_file<<"SCALARS pressure float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_p*p_ref<<"\n";
		m_file<<"SCALARS Temperature float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_T*T_ref<<"\n";
		m_file<<"SCALARS rho float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_rho*rho_ref<<"\n";
		m_file<<"VECTORS vel float\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_vel.x*u_ref<<" "<<(cells+i)->flow.m_vel.y*u_ref<<" "<<(cells+i)->flow.m_vel.z*u_ref<<"\n";
		// m_file<<"VECTORS closest_L_x float\n";
		// for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->closest_point_L_x.x - (cells+i)->flux_point_L_x.n_x<<" "<<(cells+i)->closest_point_L_x.y - (cells+i)->flux_point_L_x.y<<" "<<(cells+i)->closest_point_L_x.z - (cells+i)->flux_point_L_x.z<<"\n";
		m_file<<"SCALARS M float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) {
			double M  = (cells+i)->flow.m_vel.mag()/(cells+i)->flow.gm->sound_speed((cells+i)->flow);
			m_file<<M<<"\n";
		}
		//m_file<<"SCALARS residual float 1\n";
		//m_file<<"LOOKUP_TABLE default\n";
		//for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->max_resid<<"\n";
		m_file<<"SCALARS status float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) {
			if ((cells + i)->contains_boundary) m_file<<0.5<<"\n";
			else m_file<<(cells+i)->is_active<<"\n";
		}
		m_file<<"SCALARS min_dist_surrogate float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->min_dist_surrogate<<"\n";
		m_file<<"SCALARS min_lose_convex float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->min_lose_convex<<"\n";
		m_file<<"SCALARS conv_S float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_S<<"\n";
		m_file<<"SCALARS conv_N float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_N<<"\n";
		m_file<<"SCALARS conv_E float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_E<<"\n";
		m_file<<"SCALARS conv_W float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->conv_W<<"\n";

}
void write_pressure_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].flow.m_p<<" ";		
			}
			m_file << "\n";
		} 
}
void write_pos_x_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].pos.x<<" ";		
			}
			m_file << "\n";
		} 
}
void write_pos_y_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].pos.y<<" ";		
			}
			m_file << "\n";
		} 
}
void write_point_data_vs_height(const char* infilename, const double pointdata, const char* filename){
	ifstream inFile;
	inFile.open(infilename);
	double h;
	inFile >> h;
	FILE *m_file;
	m_file=fopen(filename,"a");
	fprintf(m_file,"%16.16f %16.16f \n",h, pointdata);
	// m_file << h << " " << pointdata << "\n";
}
void write_point_data_vs_iteration(const int i_t, const double pointdata, const char* filename){
	ofstream m_file;
	m_file.precision(17);
	m_file.open(filename,ios::app);
	m_file << i_t << " " << pointdata << "\n";
}

void write_vector(const vec F, const char* filename){
	FILE *m_file;
	m_file=fopen(filename,"a");
	fprintf(m_file,"%16.16f %16.16f %16.16f \n",F.x, F.y, F.z);
}
