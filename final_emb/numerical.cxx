#include <cmath>
#include <stdio.h>
#include "numerical.h"
using namespace std;
int newton_solve(double x0, dbl_dbl_func f, dbl_dbl_func fdash, double &x, double dummy){
	double tol = 1e-8;
	double diff = 1.0;
	int maxits = 100000;
	int its = 0;
	double f_val;
	x = x0;
	int success = 1;
	while (diff > tol){
		f_val = f(x);
		diff = abs(f_val);
		x -= 0.1*f_val/fdash(x);
		// fprintf(stderr, "i: %d, x: %f, diff: %f, f_val: %f \n", its, x, diff, f_val);
		if (its > maxits) {
			fprintf(stderr, "exceeded maximum number of iterations \n");
			fprintf(stderr,"x0: %f, y0: %f \nx: %f, diff: %f\n", x0, dummy, x, diff);
			success = 0;
			break;
		}
		its++;
	}
	// if(std::isnan(x))
	return success;
}
double cubic(double a[4], double x){
	return a[0] * x * x * x + a[1] * x * x + a[2] *x + a[3];
}
double cubic_deriv(double a[4], double x){
	return 3*a[0] * x * x + 2 * a[1] *x + a[2];
}
void newton_solve(double x0, double y0, bi_var_func f1, bi_var_func f2, 
	bi_var_func f1_xdash, bi_var_func f1_ydash,
	bi_var_func f2_xdash, bi_var_func f2_ydash,
	double &x, double &y){
	double tol = 1e-4;
	double diff = 1.0;
	int maxits = 1000;
	int its = 0;
	double f1val, f2val, df1x, df1y, df2x, df2y;
	x = x0; y = y0;
	while (diff > tol){
		f1val = f1(x,y); f2val = f2(x,y);
		df1x = f1_xdash(x,y); df1y = f1_ydash(x,y); df2x = f2_xdash(x,y); df2y = f2_ydash(x,y);
		diff = sqrt(f1val * f1val + f2val * f2val);
		double det = df1x * df2y - df1y * df2x;
		x -= 1.0 / det * (df2y * f1val - df1y *f2val);
		y -= 1.0 / det * (-df2x * f1val + df1x * f2val);
		// fprintf(stderr, "i: %d, x: %f, diff: %f, f_val: %f \n", its, x, diff, f1val);
		if (its > maxits) {
			fprintf(stderr, "exceeded maximum number of iterations \n");
			fprintf(stderr,"x0: %f, y0: %f, \nx: %f, y: %f, diff: %f\n", x0, y0, x, y, diff);
			break;
		}	
		its++;
	}
	// if (abs(x0 - 0.5) < 0.05 && abs(y0 - 1.4) < 0.05) {
	// 	fprintf(stderr, "close to leading edge \n");
	// 	fprintf(stderr,"x0: %f, y0: %f, \nx: %f, y: %f, diff: %f\n", x0, y0, x, y, diff);
	// }
	// fprintf(stderr,"x0: %f, y0: %f, \nx: %f, y: %f, diff: %f\n", x0, y0, x, y, diff);
}