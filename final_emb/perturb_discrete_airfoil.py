afternodelines=[]
nodelines=[]
with open('emb_points.dat') as meshfile:
    for fileline in meshfile:
        nodelines.append(fileline)

with open('./sdesign/input.vmo') as vmofile:
    vmofile.readline()
    vmofile.readline()
    vmofile.readline()
    vmolines_plus=vmofile.readlines()
    perturb_plus=[[float(nodeval)+float(vmoval) for nodeval, vmoval  in zip(nodeline.split(),vmolineplus.split())] for nodeline, vmolineplus in zip(nodelines,vmolines_plus)]

with open('discrete_points.dat','w') as f:
    for line in perturb_plus:
        printline=" ".join([str(n) for n in line])
        f.writelines(printline)
        f.write("\n")

