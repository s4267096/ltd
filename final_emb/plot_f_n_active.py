import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
fig, ax1 = plt.subplots()
dat= np.genfromtxt("F_vs_embsurfheight.dat");
dat2= np.genfromtxt("n_active_vs_embsurfheight.dat");
last = -1
ax1.plot(dat[0:last,0], dat[0:last,1], 'b-x')
ax1.set_xlabel('height embedded surface')
# Make the y-axis label, ticks and tick labels match the line color.
ax1.set_ylabel('F', color='b')
ax1.tick_params('y', colors='b')

ax2 = ax1.twinx()
ax2.plot(dat2[0:last,0],dat2[0:last,1], 'r-')
ax2.set_ylabel('n_active', color='r')
ax2.tick_params('y', colors='r')
plt.savefig('F_nactive.png',dpi=600)
plt.show()
