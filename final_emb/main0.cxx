#include <iostream>
#include <sstream> 
#include <fstream>
#include <iomanip> 
#include <cmath>
#include <map>
#include "thermo.h"
#include "cell.h"
#include "interpolate.h"
#include "post.h"
#include "force.h"
#include "embsurf.h"
//Set up overal parameters to govern solutioni
#define N 100 //size of grid is N x N
#define timesteppinglocal 0
#define fluxFuncName roe
#define strfluxFuncName "roe"
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
using fluxFunc = int (*)(flow_props, flow_props, flux&, gas_model*);
int roe(flow_props, flow_props, flux&, gas_model*,char);
int ausmdv(flow_props, flow_props, flux&, gas_model*);
void reconstruct_and_compute_fluxes(cell_threeD*, flux*, gas_model&, int, int, int, int, std::map<string,int>&);
void run_auxiliary_embedded(int&, int*, int*, cell_threeD*, int, int, int, int, int, double, double, double&, flux*, gas_model&, double&, double&, double, std::map<string,int>&);
void read_input_file(string,std::map<string,int>&);
using namespace std;
//level set function here
//main
int main()
{
	std::map<string,int> ioData;
	read_input_file("FluidFile",ioData);
	write_embsurf_to_vtk(100,"post/emb_surf.vtk");
	int n_esp = 100;
	embsurfpoint* esps = generate_embsurfpoints(n_esp);
	int sor = ioData["order"] == 2 ? 1 : 0;
	int hor = ioData["higherorder"];
	//set up temporal discretisation and intervals at which to print/ write solution
	double rel_resid;
	double initCFL = 0.4;
	double CFL = initCFL;
	int n_tsteps = ioData["ntsteps"];
	int n_tsteps_aux = 1;
	constexpr double resid_tol = 1e-8;
	int print_n_interval = 100;//interval at which to print residual to terminal
	int print_resid_interval = 100;//interval at which residual is written to text file
	double dt = 0.00001;//initial time step
	//set up spacial discretistation
	constexpr int n_cells_x = N+4;
	constexpr int n_cells_y = N+4;
	constexpr int n_cells_z = 5;
	double dL = 3.0/(N - 4);
	const vec L(dL, dL, dL);
	const double min_L = min(min(L.x, L.y), L.z);	
	//initialise cell objects in an array and fill with geometric informationge
	cell_threeD *cells = new cell_threeD[n_cells_x*n_cells_y];
	cell_threeD *cells_halfstep = new cell_threeD[n_cells_x*n_cells_y];
	constexpr int nfluxes = 2*n_cells_x*n_cells_y  - 7*(n_cells_x + n_cells_y) + 24;
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
				vec pos(L.x*(i-2+0.5), L.y*(j-2+0.5),0);
				cells[INDX(i,j,n_cells_x)]=cell_threeD(L, pos);
		}
	}
	//Initialise flux objects in an array and link to relevant cells (left and right) by assigning poitners 
	flux *fluxes = new flux[nfluxes];
	int i_flux = 0;
	for(int i=2; i!=n_cells_x-1; i++){
		for(int j=2; j!=n_cells_y-2; j++){
				cells[INDX(i,j,n_cells_x)].fluxL_x=&fluxes[i_flux];
				cells[INDX(i-1,j,n_cells_x)].fluxR_x=&fluxes[i_flux];
				i_flux++;
		}
	}
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-1; j++){
				cells[INDX(i,j,n_cells_x)].fluxL_y=&fluxes[i_flux];
				cells[INDX(i,j-1,n_cells_x)].fluxR_y=&fluxes[i_flux];
				i_flux++;
		}
	}
	//Set up flow state object to represent free stream conditions
	double p_inf = 100000; double T_inf = 300; double M_inf = 2.0; double delta = -0.0/180.0*3.14159;
	gas_model air(1.4,0.02897);
	flow_props flow_freestream = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf, delta, &air);
	cout << "freestream flow \n" << flow_freestream;
	//fill entire domain with freestream conditions thereby setting intial and boundary conditions
	int n_active = 0;
	int contains_boundary_idxs_x[(N+4)*(N+4)];
	int contains_boundary_idxs_y[(N+4)*(N+4)];
	int ii = 0;
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
			cells[INDX(i,j,n_cells_x)].flow=flow_freestream;//left
			cells[INDX(i,j,n_cells_x)].interfL_x=flow_freestream;//left
			cells[INDX(i,j,n_cells_x)].interfR_x=flow_freestream;//left
			cells[INDX(i,j,n_cells_x)].interfL_y=flow_freestream;//left
			cells[INDX(i,j,n_cells_x)].interfR_y=flow_freestream;//left
			cells[INDX(i,j,n_cells_x)].determineState(embsurf);
			if (cells[INDX(i,j,n_cells_x)].is_active) n_active += 1;
		}
	}
	// flow_props flow_freestream2 = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf*0.7, delta, &air);
	// for(int i=3; i!=n_cells_x-3; i++){
	// 	for(int j=3; j!=n_cells_y-3; j++){
	// 		cells[INDX(i,j,n_cells_x)].flow=flow_freestream2;//left
	// 		cells[INDX(i,j,n_cells_x)].interfL_x=flow_freestream2;//left
	// 		cells[INDX(i,j,n_cells_x)].interfR_x=flow_freestream2;//left
	// 		cells[INDX(i,j,n_cells_x)].interfL_y=flow_freestream2;//left
	// 		cells[INDX(i,j,n_cells_x)].interfR_y=flow_freestream2;//left
	// 	}
	// }
	for(int i=1; i!=n_cells_x-1; i++){
		for(int j=1; j!=n_cells_y-1; j++){
			cells[INDX(i,j,n_cells_x)].setup_emb_points(get_intersectedpoint, get_closestpoint, get_normal, ioData["SI"]);
			cells[INDX(i,j,n_cells_x)].determine_boundary_convex(cells[INDX(i,j+1,n_cells_x)].is_active,
				cells[INDX(i+1,j,n_cells_x)].is_active,
				cells[INDX(i,j-1,n_cells_x)].is_active,
				cells[INDX(i-1,j,n_cells_x)].is_active,
				get_normal,
				get_closestpoint);
			if (cells[INDX(i,j,n_cells_x)].contains_boundary_convex){
				contains_boundary_idxs_x[ii] = i;
				contains_boundary_idxs_y[ii] = j;
				ii++;
			}
		}
	}
	//copy information into cells_halfstep
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
				cells_halfstep[INDX(i,j,n_cells_x)]=cells[INDX(i,j,n_cells_x)];
		}
	}
	int n_cells_containingboundary = ii;
	cout << "percentage active = " << n_active*1.0/N/N << "\n";
	//Begin simulation by updating in time
	double max_a=0;//max wave speed over the domain
	double t_elapsed=0;
	//initialize cell array which excludes ghost cells outside the boundary
	cell_threeD* cells_no_ghost = new cell_threeD[(n_cells_x - 4)*(n_cells_y - 4)];
	//set up text file for writing residual at various time steps
	ofstream m_file;
	string residualfilename;
	if (timesteppinglocal) residualfilename = "residuallocaldt.dat";
	else residualfilename = "residualglobaldt.dat";
	m_file.open(residualfilename, ios::out);
	m_file << "# iteration average_residual";
	for(int i_t=0; i_t!=n_tsteps; ++i_t){	
		if (i_t % print_n_interval == 0){
			//assign correct flow properties to non-ghost cells for writing to text file
			for(int i=2; i!=n_cells_x-2; i++){
				for(int j=2; j!=n_cells_y-2; j++){
					cells_no_ghost[INDX(i-2,j-2,n_cells_x-4)]=cells[INDX(i,j,n_cells_x)];
					}
				}
			//write solution to *.vtk file for viewing in paraview
			stringstream vtkfilename;
			vtkfilename << "post/N"<<N<<"t00"<<i_t<<".vtk";
			write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
			write_cell_data(&cells_no_ghost[0],(n_cells_x-4)*(n_cells_y-4)*(n_cells_z-4),vtkfilename.str());
		}
		reconstruct_and_compute_fluxes(cells, fluxes, air, n_cells_x, n_cells_y, sor, hor, ioData);
		//integrate fluxes in time using forward euler explicit
		double average_resid = 0.0; 
		double init_resid;
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				if(cells[INDX(i,j,n_cells_x)].is_active){
					//calculate maximum wave speed for each cell and assign dt if local time stepping
					double abs_vel_x = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.x);		
					double abs_vel_y = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.y);
					double max_abs_v = max(abs_vel_x, abs_vel_y);
					double c = air.sound_speed(cells[INDX(i,j,n_cells_x)].flow);
					if (timesteppinglocal)	dt=CFL*min_L/(max_abs_v + c);
					else max_a=max(max_a, max_abs_v + c);
					cells[INDX(i,j,n_cells_x)].update_in_time(dt, CFL);
					average_resid += abs(cells[INDX(i,j,n_cells_x)].max_resid);
				}			
			}
		}
		if (CFL < 0.5) CFL *= 1.0002;
		if (CFL < 0.000001){
			fprintf(stderr, "CFL below 0.000001, exiting at n: %d \n", i_t);
			break;
		}
		average_resid /= n_active;
		if (i_t == 0) init_resid = average_resid;
		// fprintf(stderr, "average_resid: %f \n", average_resid);
		// fprintf(stderr, "init_resid: %f \n", init_resid);
		rel_resid = average_resid/init_resid;
		if (rel_resid < resid_tol) break;
		// if (average_resid < 0.5) break;
		// assign dt for next time step and total elapsed time if using global time stepping
		if (!timesteppinglocal) {
					t_elapsed += dt;
					dt=CFL*min_L/max_a;
		}
		//this is where the weird embedded stuff is going to happen 
		double average_aux_resid=0;
		double init_aux_resid;
		if (ioData["SI"]==2) {
			run_auxiliary_embedded(n_cells_containingboundary, contains_boundary_idxs_x, contains_boundary_idxs_y, 
			cells, n_cells_x, n_cells_y, i_t, n_tsteps_aux, hor, min_L, max_a, CFL, fluxes, air, average_aux_resid, init_aux_resid, dt, ioData);
		}//end weird embedded
		
		//write residual to file and print solution
		if (i_t % print_resid_interval == 0){
			vec F;
			if (ioData["forceorder"]==1) F = calculate_force1(cells, n_cells_x, n_cells_y, L, esps, 2*n_esp);
			else F = calculate_force0(cells, n_cells_x, n_cells_y);
			ofstream m_file;
			m_file.open(residualfilename, ios::out | ios::app);
			m_file << setw(8) << i_t << " " << setw(8) << average_resid << "\n";
			cout << "n: " << i_t << "; CFL: " << CFL << "; rel res: " << rel_resid << "; aux rel res: " << average_aux_resid/init_aux_resid << " abs res: " << average_resid << " aux abs res: " << average_aux_resid << "; F: " << F << "\n";
		}
		// write_point_data_vs_iteration(i_t, cells[INDX(34,40,n_cells_x)].flow.m_p,"p_vs_time.dat");//print out center cell pressure

	}
	//assign correct flow properties to non-ghost cells for writing to text file
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-2; j++){
			cells_no_ghost[INDX(i-2,j-2,n_cells_x-4)]=cells[INDX(i,j,n_cells_x)];
			}
		}
	//write soution at final time step to file
	stringstream vtkfilename;
	vtkfilename << "post/N"<<N<<"t00"<<n_tsteps<<".vtk";
	write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
	write_cell_data(&cells_no_ghost[0],(n_cells_x-4)*(n_cells_y-4),vtkfilename.str());
	//write solution to text file for MATLAB post processing
	stringstream p_final_filename;
	stringstream x_final_filename;
	stringstream y_final_filename;
	p_final_filename << "post_N/N" << N << "p.dat";
	x_final_filename << "post_N/N" << N << "x.dat";
	y_final_filename << "post_N/N" << N << "y.dat";
	write_pressure_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, p_final_filename.str());
	write_pos_x_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, x_final_filename.str());
	write_pos_y_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, y_final_filename.str());
	write_point_data_vs_height("height.dat", cells_no_ghost[INDX(n_cells_x/2,n_cells_y/2,n_cells_x)].flow.m_p,"p_vs_embsurfheight.dat");//print out center cell pressure
	write_point_data_vs_height("height.dat", n_active,"n_active_vs_embsurfheight.dat");//print out number of active cells
	write_point_data_vs_height("height.dat", rel_resid,"resid_vs_embsurfheight.dat");//print out number of active cells
	delete cells;
	delete cells_no_ghost;
	delete fluxes;
}
void reconstruct_and_compute_fluxes(cell_threeD* cells, flux* fluxes, gas_model &air, int n_cells_x, int n_cells_y, int sor, int hor, std::map<string,int> &ioData)
{
//reconstruct flow state at cell interfaces in x,y direction for right and left
		for(int i=1; i!=n_cells_x-1; i++){
			for(int j=1; j!=n_cells_y-1; j++){
  					if (cells[INDX(i,j,n_cells_x)].is_active){
  						if (sor){
		  					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x, &interpolateSecondOrderUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x, &interpolateSecondOrderUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y, &interpolateSecondOrderUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y, &interpolateSecondOrderUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);
						}
						else{
		  					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x, &interpolateFirstOrderUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x, &interpolateFirstOrderUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y, &interpolateFirstOrderUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y, &interpolateFirstOrderUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);					
						}
					}
			}
		}
		// reconstruct flow state at cell interfaces in x,y direction for right and left using riemann problem if the cell is inactive and adjacent cell is contains_boundary_convex or active
		for(int i=1; i!=n_cells_x-1; i++){
			for(int j=1; j!=n_cells_y-1; j++){
  					if (!cells[INDX(i,j,n_cells_x)].is_active){
	 					if (i!=n_cells_x-1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i+2,j,n_cells_x)],
	 						&cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)],
	 						'E',ioData["SI"],sor, hor, ioData["normal"]);
						if (i!=1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i-2,j,n_cells_x)],
	 						&cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)],
							'W',ioData["SI"],sor, hor, ioData["normal"]);
						if (j!=n_cells_y-1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j+2,n_cells_x)],
	 						&cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)],
							'N',ioData["SI"],sor, hor, ioData["normal"]);
						if (j!=1) cells[INDX(i,j,n_cells_x)].getExactRiemann(&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j-2,n_cells_x)],
	 						&cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)],
							'S',ioData["SI"],sor, hor, ioData["normal"]);
					}
			}
		}
		// calculate fluxes based on these interface values, accounting for boundary conditions
		char BC;
		int i_flux = 0;
		//x-direction fluxes
		for(int i=2; i!=n_cells_x-1; i++){
			for(int j=2; j!=n_cells_y-2; j++){
					if (i == 2) BC = 'L'; //left boundary
					else if (i == n_cells_x - 3) BC = 'R';//right boundary
					else BC = 'N';
					if (cells[INDX(i-1,j,n_cells_x)].is_active || cells[INDX(i,j,n_cells_x)].is_active){
						fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, fluxes[i_flux], &air, BC);
					}
					i_flux++;
			}
		}
		//y-direction fluxes
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-1; j++){
					if (j == n_cells_y-3) BC = 'R'; //top boundary
					else if (j == 2) BC = 'L';
					else BC = 'N';
					if (cells[INDX(i,j-1,n_cells_x)].is_active || cells[INDX(i,j,n_cells_x)].is_active){
						cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
						cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
						fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, fluxes[i_flux], &air, BC);
						cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
						cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
						fluxes[i_flux].momentum.switch_y_x();
					}
					i_flux++;
			}
		}
}
void run_auxiliary_embedded(int& n_cells_containingboundary, int* contains_boundary_idxs_x, int* contains_boundary_idxs_y, 
	cell_threeD* cells, int n_cells_x, int n_cells_y, int i_t, int n_tsteps_aux, int hor, double min_L, double max_a, double &CFL, flux* fluxes, gas_model& air, 
	double& average_aux_resid, double& init_aux_resid, double dt, std::map<string,int> &ioData){
			for(int i_t_aux = 0; i_t_aux != n_tsteps_aux; i_t_aux++){
				for(int ii = 0; ii != n_cells_containingboundary; ii++){
						int i = contains_boundary_idxs_x[ii]; int j = contains_boundary_idxs_y[ii];
						if (i > 0 && i < n_cells_x && j > 0 && j < n_cells_y){
							//assumes active adjacent cells have already been populated
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x,&interpolateFirstOrderUpwind,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x,&interpolateFirstOrderUpwind,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y,&interpolateFirstOrderUpwind,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
							cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y,&interpolateFirstOrderUpwind,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);
							cells[INDX(i+1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i+1,j+1,n_cells_x)],'W',ioData["normal"],hor);
							cells[INDX(i-1,j,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j+1,n_cells_x)], &cells[INDX(i,j+1,n_cells_x)], &cells[INDX(i,j-1,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)],'E',ioData["normal"],hor);
							cells[INDX(i,j+1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i+1,j+1,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i-1,j+1,n_cells_x)],'S',ioData["normal"],hor);
							cells[INDX(i,j-1,n_cells_x)].getExactRiemannAux(&cells[INDX(i,j,n_cells_x)], &cells[INDX(i-1,j-1,n_cells_x)], &cells[INDX(i-1,j,n_cells_x)], &cells[INDX(i+1,j,n_cells_x)], &cells[INDX(i+1,j-1,n_cells_x)],'N',ioData["normal"],hor);
							//probe some points
							// if(abs(cells[INDX(i,j,n_cells_x)].pos.x - 1.017) < 0.03 && abs(cells[INDX(i,j,n_cells_x)].pos.y - 1.2) < 0.03){
							// 	fprintf(stderr, "i: %d, j: %d, x: %f, y: %f \n", i, j, cells[INDX(i,j,n_cells_x)].pos.x, cells[INDX(i,j,n_cells_x)].pos.y);
							// }
							// if(i==34 && j==40){
							// 	 if (i_t>2496){
							// 	 	fprintf(stderr, "i: %d ------------------\n", i_t);
							// 	 	cout << "left_Rinterf: " << cells[INDX(i-1,j,n_cells_x)].interfR_x;
							// 	 	cout << "down_Uinterf: " << cells[INDX(i,j-1,n_cells_x)].interfR_y;
							// 	 }
							// }
						}
				}//end reconstruction
				//calculate fluxes based on these interface values, accounting for boundary conditions
				char BC;
				int i_flux = 0;
				//x-direction fluxes
				for(int i=2; i!=n_cells_x-1; i++){
					for(int j=2; j!=n_cells_y-2; j++){
							if (i == 2) BC = 'L'; //left boundary
							else if (i == n_cells_x - 3) BC = 'R';//right boundary
							else BC = 'N';//only compute fluxes for boundary cells
							if (cells[INDX(i-1,j,n_cells_x)].contains_boundary || cells[INDX(i,j,n_cells_x)].contains_boundary){
								fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, fluxes[i_flux], &air, BC);
							}
							i_flux++;
					}
				}
				//y-direction fluxes
				for(int i=2; i!=n_cells_x-2; i++){
					for(int j=2; j!=n_cells_y-1; j++){
							if (j == n_cells_y-3) BC = 'R'; //top boundary
							else if (j == 2) BC = 'L';
							else BC = 'N';
							if (cells[INDX(i,j-1,n_cells_x)].contains_boundary || cells[INDX(i,j,n_cells_x)].contains_boundary){
								cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
								cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
								fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, fluxes[i_flux], &air, BC);
								cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
								cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
								fluxes[i_flux].momentum.switch_y_x();
							}
							i_flux++;
					}
				}
				average_aux_resid = 0.0;
				for(int ii = 0; ii != n_cells_containingboundary; ii++){
						int i = contains_boundary_idxs_x[ii]; int j = contains_boundary_idxs_y[ii];
						if (i > 1 && i < n_cells_x - 2 && j > 1 && j < n_cells_y - 2){
							double abs_vel_x = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.x);		
							double abs_vel_y = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.y);
							double max_abs_v = max(abs_vel_x, abs_vel_y);
							double c = air.sound_speed(cells[INDX(i,j,n_cells_x)].flow);
							if (timesteppinglocal)	dt=CFL*min_L/(max_abs_v + c);
							else max_a=max(max_a, max_abs_v + c);
							cells[INDX(i,j,n_cells_x)].update_in_time(dt,CFL);
							average_aux_resid += abs(cells[INDX(i,j,n_cells_x)].max_resid);
						}	
				}//end update in time
			average_aux_resid /= n_cells_containingboundary;
			if (i_t_aux == 0 && i_t == 0) init_aux_resid = average_aux_resid;
			// cout << "n: " << i_t_aux << "; auxiliary residual: " << average_aux_resid/init_aux_resid << "\n";
			if (average_aux_resid/init_aux_resid < 1e-4) {
				// cout << "n_t_aux: " << i_t_aux;
				break;
			}
			}// end embedded weird stuff
	}
void read_input_file(string filename, std::map<string,int> &ioData){
	cout << "----Reading parameters from input file ------- \n";
	ifstream inputFile;
	inputFile.open(filename);
	string field;
	string val;
	if (inputFile.is_open()){
		while(!inputFile.eof()){
			getline(inputFile,field,'=');
			getline(inputFile,val);
			if (field=="SI"){
				cout << "SI value read from file\n";
				ioData[field] = stoi(val);
			}
			else if (field=="order"){
				cout << "order scheme read from file\n";
				ioData[field] = stoi(val);
			}
			else if (field=="normal"){
				cout << "normal read from file \n";
				ioData[field] = (val=="structure") ? 1 : 0;
			}
			else if (field=="ntsteps"){
				cout << "ntsteps read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="higherorder"){
				cout << "higher order read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="forceorder"){
				cout << "force order read from file \n";
				ioData[field] = stoi(val);
			}
		}
	}
	cout << "----Finished reading parameters from input file ------- \n";
}