#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include <sstream>
#include "geom.h"
#include "embsurf.h"
#include "numerical.h"
#include "esp.h"
using namespace std;
#define cubic(a,b,c,d,x) ((d)*(x)*(x)*(x) + (c)*(x)*(x) + (b)*(x) + (a))
#define cubic_deriv(b,c,d,x) (3*(d)*(x)*(x) + 2*(c)*(x) + (b))
#define cubic_deriv_deriv(c,d,x) (6*(d)*(x) + 2*(c))
bezier_airfoil::bezier_airfoil(){
	 // Replace 'Plop' with your file name.
    ifstream file("control_points.dat");

    std::string   line;
    // Read one line at a time into the variable line:
    Nbezier = 0;
    while(std::getline(file, line))
    {
        std::stringstream  lineStream(line);

        double x ,y;
        // Read an integer at a time from the line
        lineStream >> x;
        lineStream >> y;
        p.push_back(vec(x,y,0.0));
        Nbezier++;
    }
    vec tangent;
    int i_min_1, i_pls_1;
    double h_min, h_pls;
    for(int i = 0; i != Nbezier; i++){
    	i_min_1 = (i == 0) ? Nbezier - 1 : i - 1;
    	i_pls_1 = (i == Nbezier - 1) ? 0 : i + 1;
    	if (i == 0) h_min = distance(p[i_min_1],p[i]);
    	h_pls = distance(p[i],p[i_pls_1]);
    	tangent = ((p[i_pls_1]-p[i])*(h_min/h_pls) + (p[i] - p[i_min_1])*(h_pls/h_min))*(1/(h_pls+h_min));
    	h_min = h_pls;
    	dp.push_back(tangent);
    }
    Npoints = Nbezier*Nperbezier;
    esps = new embsurfpoint[Npoints];
    cout << "made new surface with Npoints: " << Npoints << "\n";
}
double bezier_airfoil::phi(vec v){
	vec closest_point, normal;
	get_closestpoint_and_normal(v,closest_point,normal);
	int sign = (dot(v - closest_point, normal) < 0)? 1 : -1;
	return distance(v, closest_point)*sign;
}
void bezier_airfoil::generate_own_embsurfpoints(){
	vec p0, p1, dp0, dp1;
	int esps_i = 0;
	double xcoeffs[4];
	double ycoeffs[4];
	double x0, x1, y0, y1;
	int i_pls_1;
	for(int i = 0; i != Nbezier; i++){
    	i_pls_1 = (i == Nbezier - 1) ? 0 : i + 1;
    	p0 = p[i];
    	p1 = p[i_pls_1];
    	double length = distance(p0,p1);
    	dp0 = dp[i]*length;
    	dp1 = dp[i_pls_1]*length;
		xcoeffs[0] = p0.x;
		xcoeffs[1] = dp0.x;
		xcoeffs[2] = -3*p0.x + 3*p1.x - 2*dp0.x - dp1.x;
		xcoeffs[3] = 2*p0.x -2*p1.x + dp0.x + dp1.x;
		ycoeffs[0] = p0.y;
		ycoeffs[1] = dp0.y;
		ycoeffs[2] = -3*p0.y + 3*p1.y - 2*dp0.y - dp1.y;
		ycoeffs[3] = 2*p0.y -2*p1.y + dp0.y + dp1.y;
		x0 = p0.x; y0 = p0.y;
		for(int t_i = 1; t_i != Nperbezier + 1; t_i++){
			double t = t_i*1.0/Nperbezier;
			x1 = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
			y1 = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
			esps[esps_i] = embsurfpoint(vec(0.5*(x0 + x1), 0.5*(y0 + y1), 0), vec(y1 - y0, x0 - x1, 0.0),sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
			x0 = x1; y0 = y1;
			esps_i++;
		}
	}
}

inline double FF(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	return dx*(v.x - x) + dy*(v.y-y);
}
inline double dFFdx(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double ddx = cubic_deriv_deriv(xcoeffs[2], xcoeffs[3],t);
	double ddy = cubic_deriv_deriv(ycoeffs[2], ycoeffs[3],t);
	return ddx*(v.x - x) - dx*dx + ddy*(v.y - y) - dy*dy;
}
inline int solve_closestpoint_and_normal(vec v, vec p0, vec t0, vec p1, vec t1, vec& closest_point, vec& normal){
	double xcoeffs[4];
	double ycoeffs[4];
	double length = distance(p1,p0);
	t0 = t0*length;
	t1 = t1*length;
	xcoeffs[0] = p0.x;
	xcoeffs[1] = t0.x;
	xcoeffs[2] = -3*p0.x + 3*p1.x - 2*t0.x - t1.x;
	xcoeffs[3] = 2*p0.x -2*p1.x + t0.x + t1.x;
	ycoeffs[0] = p0.y;
	ycoeffs[1] = t0.y;
	ycoeffs[2] = -3*p0.y + 3*p1.y - 2*t0.y - t1.y;
	ycoeffs[3] = 2*p0.y -2*p1.y + t0.y + t1.y;
	function<double(double)> f = bind(FF,v,xcoeffs, ycoeffs,placeholders::_1);
	function<double(double)> dfdx = bind(dFFdx,v,xcoeffs, ycoeffs,placeholders::_1);
	double t;
	int success = newton_solve(0.5, f, dfdx, t, 0.0);
	if (t < 0 || t > 1) success = 0;
	// cout << " t: " << t;
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	closest_point = vec(x,y,0.0);
	normal = vec(dy, -dx, 0.0);
	return success;
}
int bezier_airfoil::get_closestpoint_and_normal(vec v, vec& closest_point, vec& normal){
	double d = 1e20;
	double dtemp;
	int min_i;
	for (int i = 0; i != Npoints; i++){
		dtemp = distance(esps[i].pos,v);
		if (dtemp < d){
			d = dtemp;
			min_i = i;
		}
	}
	if (d > 2.0){//manual override if we are far away
		closest_point = esps[min_i].pos;
		normal = esps[min_i].dn;
		return 1;
	}
	int right = 0;
	int left = 0;
	int min_i_plus_1 = (min_i != Npoints - 1) ? min_i + 1: 0;
	int min_i_minus_1 = (min_i != 0) ? min_i - 1: Npoints - 1;
	// fprintf(stderr, "%d, %d, %d \n", min_i_minus_1, min_i, min_i_plus_1);
	right = solve_closestpoint_and_normal(v, esps[min_i].pos, esps[min_i].tangent, esps[min_i_plus_1].pos, esps[min_i_plus_1].tangent, closest_point, normal);
	if (!right) left = solve_closestpoint_and_normal(v, esps[min_i_minus_1].pos, esps[min_i_minus_1].tangent, esps[min_i].pos, esps[min_i].tangent, closest_point, normal);
	normal.make_unit();
	// cout << "point: " << esps[min_i].pos << "adjacent point: " << esps[min_i_plus_1].pos << "closest: " << closest_point << "right: " << right << ", left: " << left << "\n";
	return right + left;
}
