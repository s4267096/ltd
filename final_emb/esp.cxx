#include <iostream>
#include "geom.h"
#include "esp.h"
using namespace std;
embsurfpoint::embsurfpoint()
	:pos{vec(0.0, 0.0, 0.0)}, dn{vec(0.0, 0.0, 0.0)}, dA{0.0}, tangent{vec(0.0, 0.0, 0.0)}
{
}
embsurfpoint::embsurfpoint(vec pos_in, vec dn_in, double dA_in)
	:pos{pos_in}, dn{dn_in}, dA{dA_in}
{
	dn.make_unit();
	tangent = vec(-dn.y, dn.x, 0.0);
}