rm post/*.vtk
g++ -g -std=c++11 -o jho_fiver.out main4.cxx solver.cxx embsurf_naca_discrete.cxx esp.cxx thermo.cxx flow_threeD.cxx geom.cxx cell.cxx interpolate.cxx post.cxx roe.cxx stegerwarming.cxx matrix.cxx ausmdv.cxx numerical.cxx force.cxx gmres/solution.cxx gmres/util.cxx gmres/flux_jacobian.cxx gmres/GMRES.h gmres/prec.cxx
./jho_fiver.out

