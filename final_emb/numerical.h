#include <cmath>
#include <functional>
using namespace std;
using dbl_dbl_func = function<double(double)>;
using bi_var_func = function<double(double, double)>;
int newton_solve(double, dbl_dbl_func, dbl_dbl_func, double&, double);
double cubic(double[4], double);
double cubic_deriv(double[4], double);
void newton_solve(double, double,
	bi_var_func, bi_var_func,
	bi_var_func, bi_var_func,
	bi_var_func, bi_var_func,
	double&, double&);