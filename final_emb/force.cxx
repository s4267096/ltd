#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
using namespace std;
#include "force.h"
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
using namespace std;
vec calculate_force0(cell_threeD* cells, int n_cells_x, int n_cells_y){
	double p;
	vec F(0.0, 0.0, 0.0);
	double dF;
	//x-direction boundaries
		for(int i=2; i!=n_cells_x-1; i++){
			for(int j=2; j!=n_cells_y-2; j++){
					if (cells[INDX(i-1,j,n_cells_x)].is_active + cells[INDX(i,j,n_cells_x)].is_active == 1){
						if (cells[INDX(i-1,j,n_cells_x)].is_active) {
							dF = cells[INDX(i-1,j,n_cells_x)].flow.m_p*cells[INDX(i-1,j,n_cells_x)].area.x;
							F += vec(dF, 0, 0);
						}
						else{
							dF = cells[INDX(i,j,n_cells_x)].flow.m_p*cells[INDX(i,j,n_cells_x)].area.x;
							F += vec(-dF, 0, 0);
						}
					}
			}
		}
		//y-direction boundaries
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-1; j++){
					if (cells[INDX(i,j-1,n_cells_x)].is_active + cells[INDX(i,j,n_cells_x)].is_active == 1){
						if (cells[INDX(i,j-1,n_cells_x)].is_active) {
							dF = cells[INDX(i,j-1,n_cells_x)].flow.m_p*cells[INDX(i,j-1,n_cells_x)].area.y;
							F += vec(0, dF, 0);
						}
						else{
							dF = cells[INDX(i,j,n_cells_x)].flow.m_p*cells[INDX(i,j,n_cells_x)].area.y;
							F += vec(0, -dF, 0);
						}
					}
			}
		}
	return F;
}
double get_p(cell_threeD* cells, int ii, int jj, double x, double y, int n_cells_x, int debug){
	int row = 0;
	int n = 5;
	int NN = (2*n+1)*(2*n+1);
	int n_active = 0;
	double k;
	double dd;
	double A[NN][3];
	double b[NN];
	for(int i = -n; i != n+1; i++){
		for(int j = -n; j != n+1; j++){
			if(cells[INDX(ii+i,jj+j,n_cells_x)].is_active){
				k = min(cells[INDX(ii+i,jj+j,n_cells_x)].min_dist_surrogate, 1.0);//weights cells that are about to become inactive less
				k = k*exp(-0.4*((i - x)*(i - x) + (j - y)*(j - y))); //weight based on gaussian
				A[row][0] = k*1;
				A[row][1] = k*(i-x);
				A[row][2] = k*(j-y);
				b[row] = k*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_p;
				n_active++;
			}
			else{
				for(int col = 0; col != 3; col ++) A[row][col] = 0.0;
				b[row] = 0.0;
			}
			row++;
		}
	}
	double C[3][3];
	for(int i = 0; i != 3; i++){
		for(int j = 0; j != 3; j++){
			C[i][j] = 0;
			for(int k = 0; k != NN; k++){
			C[i][j] += A[k][i]*A[k][j];
			}
		}
	}
   	double det = C[0][0]*C[1][1]*C[2][2] - C[0][0]*C[1][2]*C[2][1] 
   	- C[0][1]*C[1][0]*C[2][2] + C[0][1]*C[1][2]*C[2][0]
   	+ C[0][2]*C[1][0]*C[2][1] - C[0][2]*C[1][1]*C[2][0];
	double D0[3]; //first row only
   	D0[0] = C[1][1]*C[2][2] - C[1][2]*C[2][1];
   	D0[1] = -C[1][0]*C[2][2] + C[1][2]*C[2][0];
   	D0[2] = C[1][0]*C[2][1] - C[1][1]*C[2][0];
   	double ATbs[3];
   	for(int i = 0; i != 3; i++){
   		ATbs[i] = 0.0;
		for(int j = 0; j != NN; j++){
			ATbs[i] += A[j][i]*b[j];
		}
	}
   	double dfs = 1.0/det*(D0[0]*ATbs[0] + D0[1]*ATbs[1] + D0[2]*ATbs[2]);
	return dfs;
}
vec calculate_force2(cell_threeD* cells, int n_cells_x, int n_cells_y, vec L_cell, embsurfpoint* esp, int n_esp, string filename){
	double x, y;
	double x_resid, y_resid;
	double i_double, j_double;
	double min_d;
	double dpdx, dpdy, p, d2pdxy;
	int min_i;
	int i, j;
	ofstream m_file;
	m_file.precision(17);
	if (filename.length()){
		m_file.open(filename, ios::out);
		m_file << "#ii x y i j dn dA p\n";
	}
	vec dL(0.0, 0.0, 0.0);
	for(int ii = 0; ii != n_esp; ii++){
		//identify location of cell that contains this point
		x = esp[ii].pos.x; y = esp[ii].pos.y;
		x_resid = modf((x + 2*L_cell.x)/L_cell.x, &i_double)-0.5;
		y_resid = modf((y + 2*L_cell.y)/L_cell.y, &j_double)-0.5;
		i = (int) i_double;
		j = (int) j_double;
		int debug = 0;
		p = get_p(cells, i, j, x_resid, y_resid, n_cells_x, debug);
		if(filename.length()){
			m_file << ii << " " << x << " " << y << " " << i << " " << j << " " << esp[ii].dn.x << " " << esp[ii].dn.y << " " << esp[ii].dn.z << " " << esp[ii].dA << " " << p << "\n";
		}
		dL += esp[ii].dn*p*esp[ii].dA;
	}
	return dL*L_cell.z;
}