#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include "geom.h"
#include "embsurf.h"
#include "numerical.h"
#include "esp.h"
using namespace std;
#define h 0.51 //x position of leading edge
#define c 1.5
#define p 0.2
#define naca_y(tt,x) (5*tt*(0.2969*negnegsqrt(x) - 0.1260*x - 0.3516*x*x + 0.2843*x*x*x - 0.1015*x*x*x*x))
#define cubic(a,b,c,d,x) ((d)*(x)*(x)*(x) + (c)*(x)*(x) + (b)*(x) + (a))
#define cubic_deriv(b,c,d,x) (3*(d)*(x)*(x) + 2*(c)*(x) + (b))
#define cubic_deriv_deriv(c,d,x) (6*(d)*(x) + 2*(c))
double naca_camberline(double x_c, double m){
	if (x_c < p) return m/p/p*(2*p*x_c - x_c*x_c);
	else return m/(1-p)/(1-p)*((1-2*p)+2*p*x_c - x_c*x_c);
}
double negnegsqrt(double x){
	if (x > 0) return sqrt(x);
	else return -sqrt(-x);
}
double negsqrt(double x){
	if (x > 0) return sqrt(x);
	else return sqrt(-x);
}
embsurf_naca::embsurf_naca(){
	ifstream inFile;
	inFile.open("height.dat");
	inFile >> k;
	inFile.close();
	inFile.open("thickness.dat");
	inFile >> t;
	inFile.close();
	inFile.open("camber.dat");
	inFile >> m;
	inFile.close();
	inFile.open("cutoff.dat");
	inFile >> cutoff_factor;
	Npoints = 100;
	esps = new embsurfpoint[Npoints];
	cout << "naca emb surf generated with k: " << k << ", t: " << t << ", m: " << m << ", cutoff: " << cutoff_factor << endl;
}
double embsurf_naca::phi(vec v){
	vec closest_point, normal;
	get_closestpoint_and_normal(v,closest_point,normal);
	int sign = (dot(v - closest_point, normal) < 0)? 1 : -1;
	return distance(v, closest_point)*sign;
}
void embsurf_naca::generate_own_embsurfpoints(){
	double x0, x1, y0, y1, x_c, y_c, theta;
	int n_points = Npoints/2;
	int cutoff = cutoff_factor*n_points;
	int t_half = n_points - cutoff+1;
	vec P_bez, dP_bez;
	// some stuff to calculate cutoff points
	double y0_upper, y0_lower, y1_upper, y1_lower;
	double P_upper_grad, P_lower_grad;
	vec P_upper, P_lower, P_end;
	vec P_upper_1, P_lower_1;
	theta = (cutoff-1)*M_PI/n_points;
	x_c = 0.5*(1 - cos(theta));
	x0 = x_c*c + h;
	y_c = naca_y(t,x_c);
	y0_upper = y_c*c + c*naca_camberline(x_c, m) + k;
	y0_lower = -y_c*c + c*naca_camberline(x_c, m) + k;
	theta = cutoff*M_PI/n_points;
	x_c = 0.5*(1 - cos(theta));
	x1 = x_c*c + h;
	y_c = naca_y(t,x_c);
	y1_upper = y_c*c + c*naca_camberline(x_c, m) + k;
	y1_lower = -y_c*c + c*naca_camberline(x_c, m) + k;
	P_upper = vec(0.5*(x0 + x1), 0.5*(y0_upper + y1_upper), 0);
	P_lower = vec(0.5*(x0 + x1), 0.5*(y0_lower + y1_lower), 0);
	P_upper_grad = (y1_upper - y0_upper)/(x1 - x0);
	P_lower_grad = (y1_lower - y0_lower)/(x1 - x0); 
	P_end = vec(P_upper.x - (P_upper.y - P_lower.y)/(P_upper_grad - P_lower_grad),
				(P_lower.y*P_upper_grad - P_upper.y*P_lower_grad)/(P_upper_grad - P_lower_grad), 
				0.0);
	double dx = 1.0*(c + h - P_upper.x);
	P_upper_1 = P_upper + vec(dx,P_upper_grad*dx,0.0);
	P_lower_1 = P_lower + vec(dx,P_lower_grad*dx,0.0);
	x0 = h; y0 = k;
	double tt;
	for(int i = 0; i != n_points; i++){
			if(i < cutoff){
				theta = (i+1)*M_PI/n_points;
				x_c = 0.5*(1 - cos(theta));
				x1 = x_c*c + h;
				y_c = naca_y(t,x_c);
				y1 = y_c*c + c*naca_camberline(x_c, m) + k;
				esps[i] = embsurfpoint(vec(0.5*(x0 + x1), 0.5*(y0 + y1), 0), vec(y1 - y0, x0 - x1, 0.0),sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
				x0 = x1; y0 = y1;
			}
			else{//bezier in here
				tt = 0.5*(i - cutoff + 1)/t_half;
				// P_bez = P_upper*((1 - tt)*(1 - tt)) + P_end*(2*(1-tt)*tt) + P_lower*(tt*tt);
				P_bez = P_upper*((1-tt)*(1 - tt)*(1 - tt)) 
						+ P_upper_1*(3*tt*(1-tt)*(1-tt))
						+ P_lower_1*(3*(1-tt)*tt*tt)
						+ P_lower*(tt*tt*tt);
				x1 = P_bez.x; y1 = P_bez.y;
				// dP_bez = P_upper*(-2*(1 - tt)) + P_end*(2 - 4*tt) + P_lower*(2*tt);
				dP_bez = (P_upper_1 - P_upper)*(3*(1-tt)*(1-tt))
						+(P_lower_1 - P_upper_1)*(6*(1-tt)*tt)
						+(P_lower - P_lower_1)*(3*tt*tt);
				esps[i] = embsurfpoint(P_bez, vec(dP_bez.y, -dP_bez.x, 0.0),sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
				x0 = x1; y0 = y1;
			}
	}
	x0 = h; y0 = k;
	for(int i = 0; i != n_points; i++){
			if(i < cutoff){
				theta = (i+1)*M_PI/n_points;
				x_c = 0.5*(1 - cos(theta));
				x1 = x_c*c + h;
				y_c = -naca_y(t,x_c);
				y1 = y_c*c + c*naca_camberline(x_c, m) + k;
				esps[2*n_points - 1 - i] = embsurfpoint(vec(0.5*(x0 + x1), 0.5*(y0 + y1), 0), vec(y0 - y1, x1 - x0, 0), sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
				x0 = x1; y0 = y1;
			}
			else{//bezier
				tt = 1 - 0.5*(i - cutoff + 1)/t_half;
				// P_bez = P_upper*((1 - tt)*(1 - tt)) + P_end*(2*(1-tt)*tt) + P_lower*(tt*tt);
				P_bez = P_upper*((1-tt)*(1 - tt)*(1 - tt)) 
						+ P_upper_1*(3*tt*(1-tt)*(1-tt))
						+ P_lower_1*(3*(1-tt)*tt*tt)
						+ P_lower*(tt*tt*tt);
				x1 = P_bez.x; y1 = P_bez.y;
				// dP_bez = P_upper*(-2*(1 - tt)) + P_end*(2 - 4*tt) + P_lower*(2*tt);
				dP_bez = (P_upper_1 - P_upper)*(3*(1-tt)*(1-tt))
						+(P_lower_1 - P_upper_1)*(6*(1-tt)*tt)
						+(P_lower - P_lower_1)*(3*tt*tt);
				esps[2*n_points - 1 - i] = embsurfpoint(P_bez, vec(dP_bez.y, -dP_bez.x, 0.0),sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
				x0 = x1; y0 = y1;
			}
	}
}

inline double FF(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	return dx*(v.x - x) + dy*(v.y-y);
}
inline double dFFdx(vec v, double xcoeffs[4], double ycoeffs[4], double t){
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double ddx = cubic_deriv_deriv(xcoeffs[2], xcoeffs[3],t);
	double ddy = cubic_deriv_deriv(ycoeffs[2], ycoeffs[3],t);
	return ddx*(v.x - x) - dx*dx + ddy*(v.y - y) - dy*dy;
}
inline int solve_closestpoint_and_normal(vec v, vec p0, vec t0, vec p1, vec t1, vec& closest_point, vec& normal){
	double xcoeffs[4];
	double ycoeffs[4];
	double length = distance(p1,p0);
	t0 = t0*length;
	t1 = t1*length;
	xcoeffs[0] = p0.x;
	xcoeffs[1] = t0.x;
	xcoeffs[2] = -3*p0.x + 3*p1.x - 2*t0.x - t1.x;
	xcoeffs[3] = 2*p0.x -2*p1.x + t0.x + t1.x;
	ycoeffs[0] = p0.y;
	ycoeffs[1] = t0.y;
	ycoeffs[2] = -3*p0.y + 3*p1.y - 2*t0.y - t1.y;
	ycoeffs[3] = 2*p0.y -2*p1.y + t0.y + t1.y;
	function<double(double)> f = bind(FF,v,xcoeffs, ycoeffs,placeholders::_1);
	function<double(double)> dfdx = bind(dFFdx,v,xcoeffs, ycoeffs,placeholders::_1);
	double t;
	int success = newton_solve(0.5, f, dfdx, t, 0.0);
	if (t < 0 || t > 1) success = 0;
	// cout << " t: " << t;
	double x = cubic(xcoeffs[0], xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double y = cubic(ycoeffs[0], ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	double dx = cubic_deriv(xcoeffs[1], xcoeffs[2], xcoeffs[3], t);
	double dy = cubic_deriv(ycoeffs[1], ycoeffs[2], ycoeffs[3], t);
	closest_point = vec(x,y,0.0);
	normal = vec(dy, -dx, 0.0);
	return success;
}
int embsurf_naca::get_closestpoint_and_normal(vec v, vec& closest_point, vec& normal){
	double d = 1e20;
	double dtemp;
	int min_i;
	for (int i = 0; i != Npoints; i++){
		dtemp = distance(esps[i].pos,v);
		if (dtemp < d){
			d = dtemp;
			min_i = i;
		}
	}
	if (d > 2.0){//manual override if we are far away
		closest_point = esps[min_i].pos;
		normal = esps[min_i].dn;
		return 1;
	}
	int right = 0;
	int left = 0;
	int min_i_plus_1 = (min_i != Npoints - 1) ? min_i + 1: 0;
	int min_i_minus_1 = (min_i != 0) ? min_i - 1: Npoints - 1;
	// fprintf(stderr, "%d, %d, %d \n", min_i_minus_1, min_i, min_i_plus_1);
	right = solve_closestpoint_and_normal(v, esps[min_i].pos, esps[min_i].tangent, esps[min_i_plus_1].pos, esps[min_i_plus_1].tangent, closest_point, normal);
	if (!right) left = solve_closestpoint_and_normal(v, esps[min_i_minus_1].pos, esps[min_i_minus_1].tangent, esps[min_i].pos, esps[min_i].tangent, closest_point, normal);
	normal.make_unit();
	// cout << "point: " << esps[min_i].pos << "adjacent point: " << esps[min_i_plus_1].pos << "closest: " << closest_point << "right: " << right << ", left: " << left << "\n";
	return right + left;
}
