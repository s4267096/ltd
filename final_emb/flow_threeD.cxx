#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include "thermo.h"
#include "geom.h"
using namespace std;
flow_props::flow_props()
	:m_rho{0}, m_vel{vec(0,0,0)},m_T{0}
{
}
// flow_props::flow_props(const flow_props& f)
// 	:m_rho{f.m_rho}, m_vel{vec(f.m_vel.x,f.m_vel.y,f.m_vel.z)},m_T{f.m_T}
// {
// 	this->gm = f.gm;
// 	this->gm->update_from_rho_T(*this);
// }
flow_props flow_props_from_rho_T(double rho, double T, vec vel, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_vel = vel; f.m_T=T; f.gm=gm_m;
		f.gm->update_from_rho_T(f);
		return f;
}
flow_props flow_props_from_p_T_M_alpha(double p, double T, double M, double alpha, gas_model* gm_m)
{
		//alpha is already in radians
		flow_props f;
		f.m_p = p; f.m_T=T; f.gm=gm_m;
		double c = f.gm->sound_speed(f);
		f.m_vel = vec(M*c*cos(alpha), M*c*sin(alpha), 0.0);
		f.gm->update_from_p_T(f);
		return f;
}
flow_props flow_props_from_rho_p(double rho, double p, vec vel, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_p = p; f.m_vel = vel; f.gm=gm_m;
		f.gm->update_from_rho_p(f);
		return f;
}
flow_props mirror_flow_along_y(flow_props f){
	f.m_vel = vec(f.m_vel.x, -f.m_vel.y, f.m_vel.z);
	return f;
}

flow_props flow_props::getExactRiemann(vec normal, int debug){
	double g = gm->m_gamma;
	flow_props f_star;
	//construct coordiantes with normal
	normal.make_unit();
	double vx = dot(normal, this->m_vel);
	vec zdash(0.0, 0.0, 1.0); 
	vec ydash = cross(normal, zdash);
	vec veldash(0.0, dot(this->m_vel, ydash), dot(this->m_vel, zdash));
	//transform veldash back into cartesian coordinates 
	vec xinvdash, yinvdash, zinvdash;
	get_transpose(normal, ydash, zdash, xinvdash, yinvdash, zinvdash);
	vec velstar(dot(veldash, xinvdash), dot(veldash, yinvdash), dot(veldash, zinvdash));
	// if (debug) cout << "vx: " << vx << "\n";
	if (vx > 0){//shock
		double b = (g+1)/2*vx/gm->sound_speed(*this);
		double M1 = (b + sqrt(b*b + 4))/2;
		double rho2_rho1 = (g+1)/2*M1*M1/(1+(g-1)/2*M1*M1);
		double p2_p1 = (2*g/(g-1)*M1*M1 - 1)/(g+1)*(g-1);
		f_star = flow_props_from_rho_p(rho2_rho1*this->m_rho, p2_p1*this->m_p, velstar, gm);
	} 
	else{//rarefaction
		double c1 = gm->sound_speed(*this);
		double c2 = vx*(g-1)/2 + c1;
		double p2 = this->m_p*pow(c2/c1,2*g/(g-1));
		double rho2 = this->m_rho*pow(p2/this->m_p,1/g);
		f_star = flow_props_from_rho_p(rho2, p2, velstar, gm);
	}
	return f_star;
}

void flow_props::non_dimensionalise(const flow_props& f){
	double u_ref = f.m_vel.mag();
	m_rho /= f.m_rho;
	m_vel = m_vel*(1/u_ref);
	m_p /= f.m_rho*u_ref*u_ref;
	m_T *= gm->get_c_v()/u_ref/u_ref;
	gm->non_dimensionalise();
	gm->update_from_rho_p(*this);
}

double flow_props::get_tke(){
	return 0.5*m_rho*m_vel.mag()*m_vel.mag();
}
double flow_props::get_total_energy(){
	return m_rho*m_e + 0.5*m_rho*m_vel.mag()*m_vel.mag();
}
vec flow_props::get_momentum(){
	return vec(m_rho*m_vel.x, m_rho*m_vel.y, m_rho*m_vel.z);
}
void flow_props::write_to_file(string filename){
	ofstream m_file;
	m_file.open(filename, ios::out | ios::app );
	m_file<<setw(12)<<m_rho<<setw(12)<<m_p
		  <<setw(12)<<m_vel.x<<setw(12)<<m_vel.y
		  <<setw(12)<<m_vel.z<<setw(12)<<m_T
		  <<setw(12)<<m_e<<"\n";
	m_file.close();
}

flux::flux()
	:mass{0}, momentum{vec(0,0,0)}, total_energy{0}, tke{0}, omega{0}
{}
void flux::multiply_and_add(flux& f, double k){
	mass += f.mass*k;
	momentum += f.momentum*k;
	total_energy += f.total_energy*k;
}
void flux::operator=(double c){
	mass = c;
	momentum.x = c;
	momentum.y = c;
	momentum.z = c;
	total_energy = c;
}
ostream& operator<<(ostream& info, const flow_props& c){
	info << "p: " << c.m_p << " T: " << c.m_T << " rho: "
	<< c.m_rho << " e: " << c.m_e << " vel: " << c.m_vel
	<<"\n";
	return info;
}
ostream& operator<<(ostream& info, const flux& c){
	info << "mass: " << c.mass << " mom: " << c.momentum 
	<<" TE: " << c.total_energy << "\n";
	return info;
}

