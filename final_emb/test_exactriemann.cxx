#include <iostream>
#include <sstream> 
#include <fstream>
#include <iomanip> 
#include <cmath>
#include "thermo.h"
#include "cell.h"
#include "interpolate.h"
#include "post.h"
//Set up overal parameters to govern solutioni
// #include "post.h"
using namespace std;
flow_props getExactRiemannState(flow_props, gas_model* );
int roe(flow_props, flow_props, flux&, gas_model*,char BC = 'N');

int main()
{
	//Set up flow state object to represent free stream conditions
	double p_inf = 100000; double T_inf = 300; double M_inf = 2; double delta = -0/180.0*3.14159;
	gas_model air(1.4,0.02897);
	flow_props flow_freestream = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf, delta, &air);
	cout << "freestream flow \n" << flow_freestream;
	flow_props flow_star = getExactRiemannState(flow_freestream, &air);
	cout << "W* \n" << flow_star;
	flow_props W_i_minus_one = flow_freestream;
	flow_props W_i(W_i_minus_one); // make sure this is a copy
	flux fluxL;
	flux fluxR;
	flux zeroFlux;
	cell_threeD cell(vec(1.0,1.0,1.0), vec(0,0,0));
	cell.fluxL_x = &fluxL;
	cell.fluxR_x = &fluxR;
	cell.fluxL_y = &zeroFlux;
	cell.fluxR_y = &zeroFlux;
	cell.flow = W_i;
	double initresid;
	for(int i = 0; i < 10000; i++){
		roe(W_i_minus_one, cell.flow, fluxL, &air);
		roe(cell.flow, getExactRiemannState(cell.flow, &air),fluxR,&air);
		cell.update_in_time(0.00005);
		if (i == 0) initresid = cell.max_resid;
		fprintf(stderr,"n: %d, resid: %e \n", i, cell.max_resid/initresid);
		if (fabs(cell.max_resid/initresid) < 1e-9) break;
	}
	cout << "W* " << flow_star;
	cout << "W_i_minus_one: " << W_i_minus_one;
	cout << "W_i: " << cell.flow;
	cout << "W_i_star: " << getExactRiemannState(cell.flow, &air);
	cout << "FluxL: " << fluxL;
	cout << "FluxR: " << fluxR;
}