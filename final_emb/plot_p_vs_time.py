import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
from math import *
rc('text',usetex = True)
dat= np.genfromtxt("p_vs_time.dat");
p_error = [log(abs(x - dat[-1,1])+1e-12) for x in dat[:,1]]
plt.plot(dat[:,0],p_error)
plt.xlabel('height of embedded surface')
plt.ylabel('pressure (MPa) at selected node')
plt.show()