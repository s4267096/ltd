rm post/*.vtk
gcc-8 -g -lstdc++ -std=c++11 -fopenmp -o jho_fiver.out main3.cxx solver.cxx embsurf_naca_discrete.cxx bezier_airfoil.cxx esp.cxx thermo.cxx flow_threeD.cxx geom.cxx cell.cxx interpolate.cxx post.cxx roe.cxx stegerwarming.cxx matrix.cxx ausmdv.cxx numerical.cxx force.cxx gmres/solution.cxx discrete_airfoil.cxx
./jho_fiver.out

