#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "thermo.h"
#include "cell.h"
#include "embsurf.h"
using namespace std;
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
cell_threeD::cell_threeD(vec L_in, vec pos_in)
	:L{vec(L_in.x, L_in.y, L_in.z)}, pos{vec(pos_in.x, pos_in.y, pos_in.z)}
//1------------0
//|            |
//|            |
//|            |
//|            |
//|            |
//2------------3
{
	get_areas();
	nodes[0] = vec(pos_in.x + 0.5*L_in.x, pos_in.y + 0.5*L_in.y, pos_in.z);
	nodes[1] = vec(pos_in.x - 0.5*L_in.x, pos_in.y + 0.5*L_in.y, pos_in.z);
	nodes[2] = vec(pos_in.x - 0.5*L_in.x, pos_in.y - 0.5*L_in.y, pos_in.z);
	nodes[3] = vec(pos_in.x + 0.5*L_in.x, pos_in.y - 0.5*L_in.y, pos_in.z);
	flux_point_L_x = vec(pos_in.x - 0.5*L_in.x, pos_in.y, pos_in.z);
	flux_point_R_x = vec(pos_in.x + 0.5*L_in.x, pos_in.y, pos_in.z);
	flux_point_L_y = vec(pos_in.x, pos_in.y - 0.5*L_in.y, pos_in.z);
	flux_point_R_y = vec(pos_in.x, pos_in.y + 0.5*L_in.y, pos_in.z);
}
cell_threeD::cell_threeD()
	:L{vec(0,0,0)}
{
}
ostream& operator<<(ostream& info, const cell_threeD& c){
	info << "Cell at: " << c.pos <<"\n";
	return info;
}
double cell_threeD::get_volume(){
	return L.x*L.y*L.z;
}
void cell_threeD::interpolate(flow_props &interf, interpFunc f, cell_threeD* c_1, cell_threeD* c_2){
	if (!c_1->is_active & !c_1->contains_boundary) c_1 = this;//override if c_1 is inactive
	if (!c_2->is_active & !c_2->contains_boundary) c_2 = this;//override if c_1 is inactive
	interf.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interf.m_p = f(c_2->flow.m_p, flow.m_p, c_1->flow.m_p);
	interf.m_vel.x = f(c_2->flow.m_vel.x, flow.m_vel.x, c_1->flow.m_vel.x);
	interf.m_vel.y = f(c_2->flow.m_vel.y, flow.m_vel.y, c_1->flow.m_vel.y);
	interf.m_vel.z = f(c_2->flow.m_vel.z, flow.m_vel.z, c_1->flow.m_vel.z);
	flow.gm->update_from_rho_p(interf);
}
void cell_threeD::update_in_time(double dt, double &CFL, int debug){
	//calculate sum of the fluxes for conserved variables
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y + source.mass);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y + source.momentum.x);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y+source.momentum.y);
	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y+source.momentum.z);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y + source.total_energy);
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//calculate maximum of residuals normalised by flow quantities
	double rho_resid = rhodash/flow.m_rho; double velx_resid = momentumdash_x/oldmomentum.mag();
	double vely_resid = momentumdash_y/oldmomentum.mag(); double e_resid = totalEdash/(oldenergy - flow.get_tke());
	max_resid = max({rho_resid, velx_resid, vely_resid, e_resid})/CFL;
	//integrate in time
	double m_rho;
	double m_vel_x;
	double m_vel_y;
	double m_vel_z;
	double m_e;
	int negative = 1;
	while(negative){
		m_rho = flow.m_rho + rhodash*dt;
		m_vel_x = (oldmomentum.x + momentumdash_x*dt)/m_rho;
		m_vel_y = (oldmomentum.y + momentumdash_y*dt)/m_rho;
		m_vel_z = (oldmomentum.z + momentumdash_z*dt)/m_rho;
		m_e = (oldenergy + totalEdash*dt- 0.5*m_rho*(m_vel_x*m_vel_x + m_vel_y*m_vel_y + m_vel_z*m_vel_z))/m_rho ;
		negative = 0;
		if (m_rho < 0) {
			fprintf(stderr, "negative density detected at (%f, %f), reducing CFL to %f ", pos.x, pos.y, CFL);
			if (is_active) fprintf(stderr, "cell is active \n");
			else if (contains_boundary) fprintf(stderr, "cell contains boundary convex \n");
			dt *= 0.5;
			CFL *= 0.5;
			negative = 1;
		}
		if (m_e < 0) {
			fprintf(stderr, "negative energy detected at (%f, %f), energy reduced by %f %% reducing CFL to %f\n ", pos.x, pos.y, 100*(1 - m_e/flow.m_e), CFL); 
			if (is_active) fprintf(stderr, "cell is active \n");
			else if (contains_boundary) fprintf(stderr, "cell contains boundary convex \n");
			dt *= 0.5;
			CFL *= 0.5;
			negative = 1;
		}
	}
	source = 0;
	flow.m_rho = m_rho;
	flow.m_vel.x = m_vel_x;
	flow.m_vel.y = m_vel_y;
	flow.m_vel.z = m_vel_z;
	flow.m_e = m_e; 
	flow.gm->update_from_rho_e(flow);
	if (debug){
		if (abs(pos.x - 0.7979735796) < 0.01 && abs(pos.y - 1.232)<0.01){
			cout << "printing out fluxes for cell: \n";
			cout << *fluxL_y << *fluxR_y << *fluxL_x << *fluxR_x;
			cout << flow;
		}
	}
}
void cell_threeD::get_areas(){
	area.x = L.y*L.z;
	area.y = L.x*L.z;
	area.z = 0;//L.x*L.y; 2D simulation
}
cell_threeD cell_threeD::update_in_time2(double dt){
	//calculate sum of the fluxes for conserved variables
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y);
	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y);
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//integrate in time and return a new cell
	cell_threeD newcell = cell_threeD(L, pos);
	newcell.flow.gm = flow.gm;
	newcell.flow.m_rho += flow.m_rho + rhodash*dt;
	newcell.flow.m_vel.x = (oldmomentum.x + momentumdash_x*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.y = (oldmomentum.y + momentumdash_y*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.z = (oldmomentum.z + momentumdash_z*dt)/newcell.flow.m_rho;
	newcell.flow.m_e = (oldenergy + totalEdash*dt- newcell.flow.get_tke())/newcell.flow.m_rho ;
	newcell.flow.gm->update_from_rho_e(newcell.flow);
	return newcell;
}
void cell_threeD::update_in_time3(cell_threeD &newcell, double dt, double &CFL, int debug){
	//calculate sum of the fluxes for conserved variables
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y);
	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y);
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//calculate maximum of residuals normalised by flow quantities
	double rho_resid = rhodash/flow.m_rho; double velx_resid = momentumdash_x/oldmomentum.mag();
	double vely_resid = momentumdash_y/oldmomentum.mag(); double e_resid = totalEdash/(oldenergy - flow.get_tke());
	max_resid = max({rho_resid, velx_resid, vely_resid, e_resid})/CFL;
	//integrate in time and modify the cell
	newcell.flow.m_rho = flow.m_rho + rhodash*dt;
	newcell.flow.m_vel.x = (oldmomentum.x + momentumdash_x*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.y = (oldmomentum.y + momentumdash_y*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.z = (oldmomentum.z + momentumdash_z*dt)/newcell.flow.m_rho;
	newcell.flow.m_e = (oldenergy + totalEdash*dt- newcell.flow.get_tke())/newcell.flow.m_rho ;
	newcell.flow.gm->update_from_rho_e(newcell.flow);
	if(debug){
		if (abs(pos.x - 0.516541) < 0.01 && abs(pos.y - 0.9820521517054)<0.01){
			cout << "printing out fluxes for cell: \n";
			cout << *fluxL_y << *fluxR_y << *fluxL_x << *fluxR_x;
			cout << newcell.flow;
		}
	}
}
void cell_threeD::perturb_flow(cell_threeD &newcell, Solution& dW, double eps){
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//integrate in time and modify the cell
	newcell.flow.m_rho = flow.m_rho + eps*dW(4*indx);
	newcell.flow.m_vel.x = (oldmomentum.x + eps*dW(4*indx+1))/newcell.flow.m_rho;
	newcell.flow.m_vel.y = (oldmomentum.y + eps*dW(4*indx+2))/newcell.flow.m_rho;
	newcell.flow.m_vel.z = (oldmomentum.z + 0)/newcell.flow.m_rho;
	newcell.flow.m_e = (oldenergy + eps*dW(4*indx+3)- newcell.flow.get_tke())/newcell.flow.m_rho ;
	newcell.flow.gm->update_from_rho_e(newcell.flow);
}
void cell_threeD::update_flow(Solution& dW){
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//integrate in time and modify the cell
	double rho_resid = dW(4*indx)/flow.m_rho; double velx_resid = dW(4*indx+1)/oldmomentum.mag();
	double vely_resid = dW(4*indx+2)/oldmomentum.mag(); double e_resid = dW(4*indx+3)/(oldenergy - flow.get_tke());
	max_resid = max({rho_resid, velx_resid, vely_resid, e_resid});
	flow.m_rho = flow.m_rho + dW(4*indx);
	flow.m_vel.x = (oldmomentum.x + dW(4*indx+1))/flow.m_rho;
	flow.m_vel.y = (oldmomentum.y + dW(4*indx+2))/flow.m_rho;
	flow.m_vel.z = (oldmomentum.z + 0)/flow.m_rho;
	flow.m_e = (oldenergy + dW(4*indx+3)- flow.get_tke())/flow.m_rho ;
	flow.gm->update_from_rho_e(flow);
}
void cell_threeD::sum_fluxes(Solution& flux_vector){
	//calculate sum of the fluxes for conserved variables
	flux_vector.setEntry(1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y),4*indx + 0);
	flux_vector.setEntry(1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y),4*indx +  1);
	flux_vector.setEntry(1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y),4*indx +  2);
	// double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
	//				  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y);
	flux_vector.setEntry(1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y),4*indx + 3);
}
void cell_threeD::getExactRiemannAux(cell_threeD* c, cell_threeD* c1, cell_threeD* c2, cell_threeD* c4, cell_threeD* c5, cell_threeD* cells, int n_cells_x,
	char orient, int struct_n, int hor, int conv_flag, int debug){// first order
	if ((!contains_boundary && !is_active)||conv_flag==1){//conv_flag is an override if we have a convex side from the other side of the surface
		vec normal;
		cell_threeD* c3 = c;
		switch(orient){
			case 'E' :
				normal = vec(-1.0, 0.0, 0.0);
				if (struct_n) normal = c->normal_L_x; 
				if (!hor) interfR_x = c->flow.getExactRiemann(normal);
				else if (hor == 1) interfR_x = hor_interpolatestate(c1, c2, c3, c4, c5, orient, c->flow);
				else if (hor == 2) interfR_x = interpolate(cells, c->flow.getExactRiemann(normal), n_cells_x, orient);
				// if (normal.x > 0){
				// 	fprintf(stderr, "bad normal, E aux \n");
				// 	cout << "pos: " << c->flux_point_L_x << "\n";
				// 	cout << "closest point " << c->closest_point_L_x << "\n";
				// 	cout << "normal: " << c->normal_L_x << "\n";
				// 	exit(-1);
				// }
				if (!c->cl_success_L_x) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_L_x << "\n";
					exit(-1);
				}

				break;
			case 'W' :
				normal = vec(1.0, 0.0, 0.0);
				if (struct_n) normal = c->normal_R_x;
				if (!hor) interfL_x = c->flow.getExactRiemann(normal);
				else if (hor == 1) interfL_x = hor_interpolatestate(c1, c2, c3, c4, c5, orient, c->flow);
				else if (hor == 2) interfL_x = interpolate(cells, c->flow.getExactRiemann(normal), n_cells_x, orient);
				// if (normal.x < 0){
				// 	fprintf(stderr, "bad normal, W aux \n");
				// 	cout << "pos: " << c->flux_point_R_x << "\n";
				// 	cout << "closest point " << c->closest_point_R_x << "\n";
				// 	cout << "normal: " << c->normal_R_x << "\n";
				// 	exit(-1);
				// }
				if (!c->cl_success_R_x) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_R_x << "\n";
					exit(-1);
				}

				break;
			case 'N':
				normal = vec(0.0, -1.0, 0.0);
				if (struct_n) normal = c->normal_L_y;
				if (!hor) interfR_y = c->flow.getExactRiemann(normal);
				else if (hor == 1) interfR_y = hor_interpolatestate(c1, c2, c3, c4, c5, orient, c->flow);
				else if (hor == 2) interfR_y = interpolate(cells, c->flow.getExactRiemann(normal), n_cells_x, orient);
				// if (normal.y > 0){
				// // if (abs(c->flux_point_L_y.x - 1.89062)< 0.001 && abs(c->flux_point_L_y.y - 1) < 0.01){
				// 	fprintf(stderr, "bad normal, N aux \n");
				// 	cout << "pos: " << c->flux_point_L_y << "\n";
				// 	cout << "closest point " << c->closest_point_L_y << "\n";
				// 	cout << "normal: " << c->normal_L_y << "\n";

				// 	// cout << "at flux point: " << c->flux_point_L_y << "\n";
				// 	// cout << "at closest point: " << c->closest_point_L_y << "\n";
				// 	exit(-1);
				// }
				if (!c->cl_success_L_y) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_R_x << "\n";
					exit(-1);
				}

				break;
			case 'S':
				int debugg = 0;
				if (abs(pos.x - 1.2653966) < 0.01 && abs(pos.y - 0.91947 - 0.03125)<0.01){
					debugg = 0;
				}
				normal = vec(0.0, 1.0, 0.0);
				if (struct_n) normal = c->normal_R_y;
				if (!hor) interfL_y = c->flow.getExactRiemann(normal);
				else if (hor == 1) interfL_y = hor_interpolatestate(c1, c2, c3, c4, c5, orient, c->flow, debugg);
				else if (hor == 2) interfL_y = interpolate(cells, c->flow.getExactRiemann(normal), n_cells_x, orient);
				// if (normal.y < 0){
				// 	fprintf(stderr, "bad normal, S aux \n");
				// 	cout << "pos: " << c->flux_point_R_y << "\n";
				// 	cout << "closest point " << c->closest_point_R_y << "\n";
				// 	cout << "normal: " << c->normal_R_y << "\n";
				// 	exit(-1);
				// }
				if (!c->cl_success_R_y) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_R_y << "\n";
					exit(-1);
				}

				// if(debug){
				// 	if (abs(pos.x - 0.5165410234) < 0.01 && abs(pos.y - 0.982052-0.03125)<0.01){
				// 		cout << "printing out S* state: \n";
				// 		cout << interfL_y;
				// 		cout << c->flow.getExactRiemann(normal);
				// 	}
				// }
				break;
		}
	}	
}
void cell_threeD::setup_emb_points(embsurf *esf, int SI){
	// if (is_active || contains_boundary){
		cl_success_L_x = esf->get_closestpoint_and_normal(flux_point_L_x, closest_point_L_x, normal_L_x);
		cl_success_R_x = esf->get_closestpoint_and_normal(flux_point_R_x, closest_point_R_x, normal_R_x);
		cl_success_L_y = esf->get_closestpoint_and_normal(flux_point_L_y, closest_point_L_y, normal_L_y);
		cl_success_R_y = esf->get_closestpoint_and_normal(flux_point_R_y, closest_point_R_y, normal_R_y);
		// if(abs(pos.x - 0.682) < 0.01 && abs(pos.y - 0.83 + 0.0153)<0.01){
		// 	fprintf(stderr, "cell @ (%f, %f) \n", pos.x, pos.y);
		// 	cout << "normal_R_y: " << normal_R_y << endl;
		// 	cout << "normal_R_x: " << normal_R_x << endl;
		// 	cout << "closest_point_R_y: " << closest_point_R_y << endl;
		// 	cout << "closest_point_R_x: " << closest_point_R_x << endl;
		// 	cout << "state: " << is_active << endl;
		// }
	// }
}
void cell_threeD::getExactRiemann(cell_threeD* c, cell_threeD* cb, 
	cell_threeD* c1, cell_threeD* c2, cell_threeD* c4, cell_threeD* c5, cell_threeD* cells, int n_cells_x,
	char orient, int SI, int sor, int hor, int struct_n, int debugFlag, int contains_boundary_Flag){
	if (!this->is_active){
	if (c->is_active || contains_boundary_Flag*c->contains_boundary){
	//SI = 0 first order fiver
	//SI = 1 second order fiver
	//SI = 2 my fiver
	/*East case
	  inactive        active       active
	|------------|-------------|------------|
	|       |    |             |            |
	|       |    |             |            |
	|l      s   r|      c      |     cb     |
	|       |    |             |            |
	|       |    |             |            |
	|------------|-------------|------------|
    Interpolated value at the face denoted by *
	*/
		cell_threeD* c3 = c;
		vec normal;
		switch(orient){
			case 'E' :
			// if (c->normal_L_x.x > 0){
			// 		fprintf(stderr, "bad normal, E \n");
			// 		cout << "pos: " << c->flux_point_L_x << "\n";
			// 		cout << "closest point " << c->closest_point_L_x << "\n";
			// 		cout << "normal: " << c->normal_L_x << "\n";
			// 		exit(-1);
			// 	}
				if (!c->cl_success_L_x) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_L_x << "\n";
					exit(-1);
				}
				if (SI == 0) {
					interfR_x = c->interfL_x.getExactRiemann(c->normal_L_x);
				}
				else if (SI == 1) {
					flow_props SI_interf;
					if (sor) {
						fprintf(stderr, "error sor for SI == 1 not implemented \n"); exit(-1);
						interpolateState(SI_interf,c->flow, cb->flow, c-> pos, cb->pos, c->closest_point_L_x);
					}
					else SI_interf = c->interfL_x;
					if (!hor){
						//interpolate some intermediate state first
						interpolateState(interfR_x, c->flow, SI_interf.getExactRiemann(c->normal_L_x),c->pos,c->closest_point_L_x,c->flux_point_L_x);
					}
					else{
						if(hor == 1) interfR_x = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if (hor == 2) interfR_x = interpolate(cells, c->flow.getExactRiemann(c->normal_L_x), n_cells_x, orient);
					}
				}
				else if (SI == 2) {//second order is taken care with reconstruction of interfL_x
					flow_props SI_interf = c->interfL_x;
					flow_props temp_interf_state;
					if (!hor){
						//interpolate some intermediate state first
						temp_interf_state = c->flow.getExactRiemann(c->normal_L_x);
					}
					else{
						int debug2 = 0;
						if (abs(pos.x - 1.2653966 +0.03125) < 0.01 && abs(pos.y - 0.91947)<0.01){
							debug2 = debugFlag;
						}
						if (hor == 1) temp_interf_state = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf, debug2);
						else if (hor == 2) temp_interf_state = interpolate(cells, c->flow.getExactRiemann(c->normal_L_x), n_cells_x, orient);
					}
					if (contains_boundary) interpolateState(interfR_x, 1/c->L.x*(c->min_lose_convex - min_lose_convex)*min_dist_surrogate, min_lose_convex, flow, temp_interf_state);
					else interfR_x = temp_interf_state;
					// if (abs(pos.x - 0.95176) < 0.01 && abs(pos.y - 0.7972)<0.01){
					// 	fprintf(stderr, "contains boundary convex: %d \n", contains_boundary);
					// 	fprintf(stderr, "interpolating getting E riemann -------\n");
					// 	fprintf(stderr, "min_dist_surrogate: %16.14e, min_lose_convex: %16.14e \n",min_dist_surrogate, min_lose_convex);
					// 	cout << "flow: " << flow;
					// 	cout << "temp_interf_state: " << temp_interf_state;
					// 	cout << "interfR_x: " << interfR_x;
					// }
				}
				break;
			case 'W' :
				// if (c->normal_R_x.x < 0){
				// 	fprintf(stderr, "bad normal, W \n");
				// 	cout << "pos: " << c->flux_point_R_x << "\n";
				// 	cout << "closest point " << c->closest_point_R_x << "\n";
				// 	cout << "normal: " << c->normal_R_x << "\n";
				// 	exit(-1);
				// }
				if (!c->cl_success_R_x) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_R_x << "\n";
					exit(-1);
				}

				if (SI == 0) {
					interfL_x = c->interfR_x.getExactRiemann(c->normal_R_x); 
					}
				else if (SI == 1) {
					flow_props SI_interf;
					if (sor){
						fprintf(stderr, "error sor for SI == 1 not implemented \n"); exit(-1);
						interpolateState(SI_interf,c->flow, cb->flow, c-> pos, cb->pos, c->closest_point_R_x);
					}
					else SI_interf = c->interfR_x;
					if (!hor){
						interpolateState(interfL_x, c->flow, SI_interf.getExactRiemann(c->normal_R_x),c->pos,c->closest_point_R_x,c->flux_point_R_x);
					}
					else{
						if(hor == 1) interfL_x =hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if(hor == 2) interfL_x =interpolate(cells, c->flow.getExactRiemann(c->normal_R_x), n_cells_x, orient);
					}
				}
				else if (SI == 2) {
					flow_props SI_interf = c->interfR_x;
					flow_props temp_interf_state;
					// interpolateState(interfL_x, flow, c->flow.getExactRiemann(c->normal_R_x),flux_point_R_x,flux_point_L_x,c->closest_point_R_x);
					if (!hor){
						temp_interf_state = c->flow.getExactRiemann(c->normal_R_x);
					}
					else{
						if (hor == 1) temp_interf_state = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if (hor == 2) temp_interf_state = interpolate(cells, c->flow.getExactRiemann(c->normal_R_x), n_cells_x, orient);
					}
					if (contains_boundary) interpolateState(interfL_x, 1/c->L.x*(c->min_lose_convex - min_lose_convex)*min_dist_surrogate, min_lose_convex, flow, temp_interf_state);
					else interfL_x = temp_interf_state;
					// if (abs(pos.x - 0.921) < 0.01 && abs(pos.y - 0.7972)<0.01){
					// 	fprintf(stderr, "contains boundary convex: %d \n", contains_boundary);
					// 	fprintf(stderr, "interpolating getting W riemann -------\n");
					// 	fprintf(stderr, "min_dist_surrogate: %16.14f, min_lose_convex: %16.14f \n",min_dist_surrogate, min_lose_convex);
					// 	cout << "flow: " << flow;
					// 	cout << "temp_interf_state: " << temp_interf_state;
					// 	cout << "interfL_x: " << interfL_x;
					// }
				}
				break;
			case 'N':
				normal = vec(0.0, -1.0, 0.0);
				// if (c->normal_L_y.y > 0){
				// 	fprintf(stderr, "bad normal, N \n");
				// 	exit(-1);
				// }
				if (!c->cl_success_L_y) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_L_y << "\n";
					exit(-1);
				}

				if (SI == 0) {
					interfR_y = c->interfL_y.getExactRiemann(c->normal_L_y);				
				}
				else if (SI == 1) {
					flow_props SI_interf;
					if (sor){
						fprintf(stderr, "error sor for SI == 1 not implemented \n"); exit(-1);
						interpolateState(SI_interf,cb->flow, c->flow, cb-> pos, c->pos, c->closest_point_L_y);
					}
					else SI_interf = c->interfL_y;
					if (!hor){
						interpolateState(interfR_y, c->flow, SI_interf.getExactRiemann(c->normal_L_y),c->pos,c->closest_point_L_y,c->flux_point_L_y);
					}
					else{
						if (hor == 1) interfR_y = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if(hor == 2) interfR_y = interpolate(cells, c->flow.getExactRiemann(c->normal_L_y), n_cells_x, orient);
					}
				}
				else if (SI == 2) {
					flow_props SI_interf = c->interfL_y;
					flow_props temp_interf_state;
					// interpolateState(interfR_y, flow, c->flow.getExactRiemann(c->normal_L_y),flux_point_L_y,flux_point_R_y,c->closest_point_L_y);
					if (!hor){
						temp_interf_state = c->flow.getExactRiemann(c->normal_L_y);
					}
					else{
						if (hor == 1) temp_interf_state = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if (hor == 2) temp_interf_state = interpolate(cells, c->flow.getExactRiemann(c->normal_L_y), n_cells_x, orient);
					}
					if (contains_boundary) interpolateState(interfR_y, 1/c->L.x*(c->min_lose_convex - min_lose_convex)*min_dist_surrogate, min_lose_convex, flow, temp_interf_state);
					else interfR_y = temp_interf_state;
					}
				break;
			case 'S':
				// if (c->normal_R_y.y < 0){
				// 	fprintf(stderr, "bad normal, S \n");
				// 	exit(-1);
				// }
				if (!c->cl_success_R_y) {
					fprintf(stderr, "unconverged closest point at: ");
					cout << c->flux_point_R_y << "\n";
					exit(-1);
				}

				if (SI == 0) {
					interfL_y = c->interfR_y.getExactRiemann(c->normal_R_y);
					}
				else if (SI == 1) {
					flow_props SI_interf;
					if (sor) {
						fprintf(stderr, "error sor for SI == 1 not implemented \n"); exit(-1);
						interpolateState(SI_interf,c->flow, cb->flow, c-> pos, cb->pos, c->closest_point_R_y);
					}
					else SI_interf = c->interfR_y;
					if (!hor){					
						interpolateState(interfL_y, c->flow, SI_interf.getExactRiemann(c->normal_R_y),c->pos,c->closest_point_R_y,c->flux_point_R_y);
					}
					else{
						if (hor == 1) interfL_y = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if (hor == 2) interfL_y = interpolate(cells, c->flow.getExactRiemann(c->normal_R_y), n_cells_x, orient);
					}
				}
				else if (SI == 2) {
					flow_props SI_interf = c->interfR_y;
					flow_props temp_interf_state;
					// interpolateState(interfL_y, flow, c->flow.getExactRiemann(c->normal_R_y),flux_point_R_y, flux_point_L_y,c->closest_point_R_y);
					if (!hor){					
						temp_interf_state = c->flow.getExactRiemann(c->normal_R_y);
					}
					else{
						if (hor == 1) temp_interf_state = hor_interpolatestate(c1, c2, c3, c4, c5, orient, SI_interf);
						else if (hor == 2) temp_interf_state = interpolate(cells, c->flow.getExactRiemann(c->normal_R_y), n_cells_x, orient);
					}
					if (contains_boundary) interpolateState(interfL_y, 1/c->L.x*(c->min_lose_convex - min_lose_convex)*min_dist_surrogate, min_lose_convex, flow, temp_interf_state);
					else interfL_y = temp_interf_state;
					if (debugFlag){					
						if (abs(pos.x - 0.516541) < 0.01 && abs(pos.y - 0.982052151-0.03125)<0.01){
							// fprintf(stderr, "contains_boundary: %d \n", contains_boundary);
							fprintf(stderr, "interpolating getting S riemann -------\n");
							// fprintf(stderr, "min_dist_surrogate: %16.14f, min_lose_convex: %16.14f \n",min_dist_surrogate, min_lose_convex);
							cout << "flow: " << flow;
							cout << "normal: " << c->normal_R_y;
							cout << "closest_point_R_y: " << c->closest_point_R_y;
							// cout << "temp_interf_state: " << temp_interf_state;
							cout << "interfL_y: " << interfL_y;
						}
					}
				}
				break;
		}
	}
}
}
void cell_threeD::determineState(embsurf *esf){
	int count_active = 0;
	int node_status;
	min_lose_convex = -1e20; min_dist_surrogate = 1e20;
	for(int i = 0; i < 4; i++){
		double phi = esf->phi(nodes[i]);
		node_status = (phi > 0) ? 1:0;
		min_dist_surrogate = min(phi, min_dist_surrogate); //reduce if we have a closer node
		min_lose_convex = max(phi,min_lose_convex);
		is_active *= node_status;
		count_active += node_status;
	}
	if (count_active > 0 && count_active < 4) {
		contains_boundary = 1;
	}
	min_dist_surrogate = abs(min_dist_surrogate);
}
void cell_threeD::get_interp_operators(cell_threeD* cells, int ii_in, int jj_in, int n_cells_x, char orient){
	fprintf(stderr,"hor == 2 not allowed without compiling Eigen/Dense \n");
	exit(-1);
	/*ii = ii_in; jj = jj_in;
	int row = 0;
	int n = n_stencil;
	double k;
	double dd;
	MatrixXf A((2*n+1)*(2*n+1)+3,3);
	//closest point
	double xc, yc, xx, yy, x, y;
	switch(orient){
		case 'E':
			xc = cells[INDX(ii+1,jj,n_cells_x)].closest_point_L_x.x; yc = cells[INDX(ii+1,jj,n_cells_x)].closest_point_L_x.y;
			xx = flux_point_R_x.x; yy = flux_point_R_x.y;
			x = 0.5; y = 0.0;
			break;
		case 'W':
			xc = cells[INDX(ii-1,jj,n_cells_x)].closest_point_R_x.x; yc = cells[INDX(ii-1,jj,n_cells_x)].closest_point_R_x.y;
			xx = flux_point_L_x.x; yy = flux_point_L_x.y;
			x = -0.5; y = 0.0;
			break;
		case 'N':
			xc = cells[INDX(ii,jj+1,n_cells_x)].closest_point_L_y.x; yc = cells[INDX(ii,jj+1,n_cells_x)].closest_point_L_y.y;
			xx = flux_point_R_y.x; yy = flux_point_R_y.y;
			x = 0.0; y = 0.5;
			break;
		case 'S':
			xc = cells[INDX(ii,jj-1,n_cells_x)].closest_point_R_y.x; yc = cells[INDX(ii,jj-1,n_cells_x)].closest_point_R_y.y;
			xx = flux_point_L_y.x; yy = flux_point_L_y.y;
			x = 0.0; y = -0.5;
			break;
	}
	double xr = (xc - xx)/L.x; double yr = (yc - yy)/L.y;
	k = kstar*exp(-0.4*(xr*xr + yr*yr))*L.x;
	A(row,0) = k*1;
	A(row,1) = k*xr;
	A(row,2) = k*yr;
	row++;
	//artificially inserting two points at the origin to skew it towards W*
	k = kstar*exp(-0.4*(xr*xr + yr*yr))*L.x;
	A(row,0) = k*1;
	A(row,1) = k*xr;
	A(row,2) = k*(yr - 1);
	row++;
	//artificially inserting two points at the origin to skew it towards W*
	k = kstar*exp(-0.4*(xr*xr + yr*yr))*L.x;
	A(row,0) = k*1;
	A(row,1) = k*(yr - 1);
	A(row,2) = k*yr;
	row++;
	for(int i = -n; i != n+1; i++){
		for(int j = -n; j != n+1; j++){
			if(cells[INDX(ii+i,jj+j,n_cells_x)].is_active||cells[INDX(ii+i,jj+j,n_cells_x)].contains_boundary){
				k = min(cells[INDX(ii+i,jj+j,n_cells_x)].min_lose_convex, L.x);//weights cells that are about to become inactive less
				k = k*exp(-0.4*((i - x)*(i - x) + (j - y)*(j - y))); //weight based on gaussian
				A(row,0) = k*1;
				A(row,1) = k*(i-x);
				A(row,2) = k*(j-y);
			}
			else{
				for(int col = 0; col != 3; col ++) A(row,col) = 0.0;
			}
			row++;
		}
	}
	// cout << "\n-----------------------------\n";
	// cout << A;
	MatrixXf A_plus = A.completeOrthogonalDecomposition().pseudoInverse();
	switch (orient){
		case 'E':
			for(int i = 0; i != (2*n+1)*(2*n+1)+3; i++){
				op_R_x[i] = A_plus(0,i)*A(i,0);
				// fprintf(stderr, "i: %d, opE[i]: %f \n", i, op_R_x[i]);
			}
			// if (abs(op_R_x[0] + op_R_x[1] + op_R_x[2] - 1)>0.1) cout << "----------\n" << "close: " << closest_point_R_x << ", flux_pnt: " << flux_point_R_x << "\n" << A << "\n";
			break;
		case 'W':
			for(int i = 0; i != (2*n+1)*(2*n+1)+3; i++){
				op_L_x[i] = A_plus(0,i)*A(i,0);
				// fprintf(stderr, "i: %d, opW[i]: %f \n", i, op_L_x[i]);
			}
			// if (abs(op_L_x[0] + op_L_x[1] + op_L_x[2] - 1)>0.1) cout << "----------\n" << "close: " << closest_point_L_x << ", flux_pnt: " << flux_point_L_x << "\n" <<  A << "\n";
			break;
		case 'N':
			for(int i = 0; i != (2*n+1)*(2*n+1)+3; i++){
				op_R_y[i] = A_plus(0,i)*A(i,0);
				// fprintf(stderr, "i: %d, opN[i]: %f \n", i, op_R_y[i]);
			}
			// if (abs(op_R_y[0] + op_R_y[1] + op_R_y[2] - 1)>0.1) cout << "----------\n" << "close: " << closest_point_R_y << ", flux_pnt: " << flux_point_R_y << "\n" <<  A << "\n";
			break;
		case 'S':
			for(int i = 0; i != (2*n+1)*(2*n+1)+3; i++){
				op_L_y[i] = A_plus(0,i)*A(i,0);
				// fprintf(stderr, "i: %d, opS[i]: %f \n", i, op_L_y[i]);
			}
			// if (abs(op_L_y[0] + op_L_y[1] + op_L_y[2] - 1)>0.1) cout << "----------\n" << "close: " << closest_point_L_y << ", flux_pnt: " << flux_point_L_y << "\n" <<  A << "\n";
			break;
	}*/
}
flow_props cell_threeD::interpolate(cell_threeD* cells, flow_props fstar, int n_cells_x, char orient){
	int row = 0;
	int n = n_stencil;
	flow_props f = fstar;
	switch (orient){
		case 'E':
			f.m_rho = f.m_rho*(op_R_x[row] + op_R_x[row+1] + op_R_x[row+2]);
			f.m_p = f.m_p*(op_R_x[row] + op_R_x[row+1] + op_R_x[row+2]);
			f.m_vel.x = f.m_vel.x*(op_R_x[row] + op_R_x[row+1] + op_R_x[row+2]);
			f.m_vel.y = f.m_vel.y*(op_R_x[row] + op_R_x[row+1] + op_R_x[row+2]);
			f.m_vel.z = f.m_vel.z*(op_R_x[row] + op_R_x[row+1] + op_R_x[row+2]);
			row++;
			row++;
			row++;
			for(int i = -n; i != n+1; i++){
				for(int j = -n; j != n+1; j++){
						f.m_rho +=op_R_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_rho;
						f.m_p +=op_R_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_p;
						f.m_vel.x +=op_R_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.x;
						f.m_vel.y +=op_R_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.y;
						f.m_vel.z +=op_R_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.z;
					row++;
				}
			}
			break;
		case 'W':
			f.m_rho = f.m_rho*(op_L_x[row] + op_L_x[row+1] + op_L_x[row+2]);
			f.m_p = f.m_p*(op_L_x[row] + op_L_x[row+1] + op_L_x[row+2]);
			f.m_vel.x = f.m_vel.x*(op_L_x[row] + op_L_x[row+1] + op_L_x[row+2]);
			f.m_vel.y = f.m_vel.y*(op_L_x[row] + op_L_x[row+1] + op_L_x[row+2]);
			f.m_vel.z = f.m_vel.z*(op_L_x[row] + op_L_x[row+1] + op_L_x[row+2]);
			row++;
			row++;
			row++;
			for(int i = -n; i != n+1; i++){
				for(int j = -n; j != n+1; j++){
						f.m_rho +=op_L_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_rho;
						f.m_p +=op_L_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_p;
						f.m_vel.x +=op_L_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.x;
						f.m_vel.y +=op_L_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.y;
						f.m_vel.z +=op_L_x[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.z;
					row++;
				}
			}
			break;
		case 'N':
			f.m_rho = f.m_rho*(op_R_y[row] + op_R_y[row+1] + op_R_y[row+2]);
			f.m_p = f.m_p*(op_R_y[row] + op_R_y[row+1] + op_R_y[row+2]);
			f.m_vel.x = f.m_vel.x*(op_R_y[row] + op_R_y[row+1] + op_R_y[row+2]);
			f.m_vel.y = f.m_vel.y*(op_R_y[row] + op_R_y[row+1] + op_R_y[row+2]);
			f.m_vel.z = f.m_vel.z*(op_R_y[row] + op_R_y[row+1] + op_R_y[row+2]);
			row++;
			row++;
			row++;
			for(int i = -n; i != n+1; i++){
				for(int j = -n; j != n+1; j++){
						f.m_rho +=op_R_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_rho;
						f.m_p +=op_R_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_p;
						f.m_vel.x +=op_R_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.x;
						f.m_vel.y +=op_R_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.y;
						f.m_vel.z +=op_R_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.z;
					row++;
				}
			}
			break;
		case 'S':
			f.m_rho = f.m_rho*(op_L_y[row] + op_L_y[row+1]+ op_L_y[row+1]);
			f.m_p = f.m_p*(op_L_y[row] + op_L_y[row+1]+ op_L_y[row+1]);
			f.m_vel.x = f.m_vel.x*(op_L_y[row] + op_L_y[row+1]+ op_L_y[row+1]);
			f.m_vel.y = f.m_vel.y*(op_L_y[row] + op_L_y[row+1]+ op_L_y[row+1]);
			f.m_vel.z = f.m_vel.z*(op_L_y[row] + op_L_y[row+1]+ op_L_y[row+1]);
			row++;
			for(int i = -n; i != n+1; i++){
				for(int j = -n; j != n+1; j++){
						f.m_rho +=op_L_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_rho;
						f.m_p +=op_L_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_p;
						f.m_vel.x +=op_L_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.x;
						f.m_vel.y +=op_L_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.y;
						f.m_vel.z +=op_L_y[row]*cells[INDX(ii+i,jj+j,n_cells_x)].flow.m_vel.z;
					row++;
				}
			}
			break;
	}
	f.gm->update_from_rho_p(f);
	return f;
}
void interpolateState(flow_props &interf, const flow_props &fa, const flow_props &fc, vec a, vec c, vec b, int debug){
	double alphax = (b.x - a.x)/(c.x - a.x+1e-12);
	double alphay = (b.y - a.y)/(c.y - a.y+1e-12);
	double alpha = max(alphax, alphay);
	double k = 3.0;
	double exp_kalpha_1_k;
	// if (debug) fprintf(stderr,"alpha_x: %f, alpha_y: %f\n", alphax, alphay);
	// if (alphax < 0 || alphay < 0) fprintf(stderr,"alpha_x: %f, alpha_y: %f\n", alphax, alphay);
	if (alpha < 0){
		exp_kalpha_1_k = (exp(k*alpha) - 1)/k;
		interf.m_rho = (fc.m_rho - fa.m_rho)*exp_kalpha_1_k + fa.m_rho;
		interf.m_p = (fc.m_p - fa.m_p)*exp_kalpha_1_k + fa.m_p;
		interf.m_vel.x = (fc.m_vel.x - fa.m_vel.x)*exp_kalpha_1_k + fa.m_vel.x;
		interf.m_vel.y = (fc.m_vel.y - fa.m_vel.y)*exp_kalpha_1_k + fa.m_vel.y;
		interf.m_vel.z = (fc.m_vel.z - fa.m_vel.z)*exp_kalpha_1_k + fa.m_vel.z;
	}
	else if (alpha > 1){
		exp_kalpha_1_k = (exp(-k*(alpha-1)) - 1)/k;
		interf.m_rho = (fa.m_rho - fc.m_rho)*exp_kalpha_1_k + fc.m_rho;
		interf.m_p = (fa.m_p - fc.m_p)*exp_kalpha_1_k + fc.m_p;
		interf.m_vel.x = (fa.m_vel.x - fc.m_vel.x)*exp_kalpha_1_k + fc.m_vel.x;
		interf.m_vel.y = (fa.m_vel.y - fc.m_vel.y)*exp_kalpha_1_k + fc.m_vel.y;
		interf.m_vel.z = (fa.m_vel.z - fc.m_vel.z)*exp_kalpha_1_k + fc.m_vel.z;
	}
	else{
		interf.m_rho = alpha*fc.m_rho + (1-alpha)*fa.m_rho;
		interf.m_p = alpha*fc.m_p + (1-alpha)*fa.m_p;
		interf.m_vel.x = alpha*fc.m_vel.x + (1-alpha)*fa.m_vel.x;
		interf.m_vel.y = alpha*fc.m_vel.y + (1-alpha)*fa.m_vel.y;
		interf.m_vel.z = alpha*fc.m_vel.z + (1-alpha)*fa.m_vel.z;
	}
	interf.gm=fa.gm;
	fa.gm->update_from_rho_p(interf);
	if(interf.m_e < 0) {
		fprintf(stderr, "m_e: %f, m_p: %f, m_rho,: %f \n", interf.m_e, interf.m_p, interf.m_rho);
		fprintf(stderr, "alpha: %f \n", alpha);
		fprintf(stderr, "negative energy detected during interpolation, try increasing k value \n");
		exit(-1);
	}	
}

void interpolateState(flow_props &interf, double a, double c, const flow_props &fa, const flow_props &fc){
	//a is typically min_dist_surrogate -- if this is zero it is how close the cell is to becoming active in which case we just want fa which is the state in the cell
	//c is typically min_dist_lose_convex -- if this is zero it suggest the cell is going to become truly inactive -- in which case we want fc which is a riemann solution
	double alpha = a/(a + c);
	// cout << "a: " << a << ", c: " << c << ", a+c: " << a + c << "\n";
	interf.m_rho = alpha*fc.m_rho + (1-alpha)*fa.m_rho;
	interf.m_p = alpha*fc.m_p + (1-alpha)*fa.m_p;
	interf.m_vel.x = alpha*fc.m_vel.x + (1-alpha)*fa.m_vel.x;
	interf.m_vel.y = alpha*fc.m_vel.y + (1-alpha)*fa.m_vel.y;
	interf.m_vel.z = alpha*fc.m_vel.z + (1-alpha)*fa.m_vel.z;
	fa.gm->update_from_rho_p(interf);
	interf.gm=fa.gm;
}

flow_props hor_interpolatestate(cell_threeD* c1, cell_threeD* c2, cell_threeD* c3, cell_threeD* c4, cell_threeD* c5,
	char orient, flow_props &SI_interf, int debug){
	flow_props edge_state;
	flow_props temp_interf_state;
	vec edge_point;
	cell_threeD* c = c3;
	double m;
	switch(orient){
			case 'E' :
				m = get_gradient(c->closest_point_L_x, c->flux_point_L_x);
				// if(debug) fprintf(stderr," hello E (%f, %f), m: %f\n", c->pos.x, c->pos.y, m);
				if (m > 2) {
					edge_point = get_edge_point_y(c->flux_point_L_x, m, c1->pos.y);
					interpolateState(edge_state, c1->flow, c2->flow, c1->pos, c2->pos, edge_point);
				}
				else if (m > 0){
					edge_point = get_edge_point_x(c->flux_point_L_x, m, c2->pos.x);
					interpolateState(edge_state, c2->flow, c3->flow, c2->pos, c3->pos, edge_point);
				} 
				else if (m > -2){
					edge_point = get_edge_point_x(c->flux_point_L_x, m, c3->pos.x);
					interpolateState(edge_state, c3->flow, c4->flow, c3->pos, c4->pos, edge_point);
				} 
				else {
					edge_point = get_edge_point_y(c->flux_point_L_x, m, c4->pos.y);
					interpolateState(edge_state, c4->flow, c5->flow, c4->pos, c5->pos, edge_point);
				}
				interpolateState(temp_interf_state, edge_state, SI_interf.getExactRiemann(c->normal_L_x), edge_point, c->closest_point_L_x, c->flux_point_L_x, debug);
				// if (debug){
				// 	cout << "normal: " << c->normal_R_y << endl;
				// 	cout << "star state: " << SI_interf.getExactRiemann(c->normal_R_y);
				// 	cout << "edge state: " << edge_state << endl;
				// 	cout << "temp_interf_state: " << temp_interf_state;

				// }
				// if (debug){
				// 	cout << "normalE: " << c->normal_R_y << endl;
				// 	cout << "star state: " << SI_interf.getExactRiemann(c->normal_R_y);
				// 	cout << "temp_interf_state: " << temp_interf_state;
				// 	cout << "edge_state: " << edge_state;
				// }
				break;
			case 'W' :
				m = get_gradient(c->closest_point_R_x, c->flux_point_R_x);
				if (m > 2) {
					edge_point = get_edge_point_y(c->flux_point_R_x, m, c1->pos.y);
					interpolateState(edge_state, c1->flow, c2->flow, c1->pos, c2->pos, edge_point);
				}
				else if (m > 0){
					edge_point = get_edge_point_x(c->flux_point_R_x, m, c2->pos.x);
					interpolateState(edge_state, c2->flow, c3->flow, c2->pos, c3->pos, edge_point);
				} 
				else if (m > -2){
					edge_point = get_edge_point_x(c->flux_point_R_x, m, c3->pos.x);
					interpolateState(edge_state, c3->flow, c4->flow, c3->pos, c4->pos, edge_point);
				} 
				else {
					edge_point = get_edge_point_y(c->flux_point_R_x, m, c4->pos.y);
					interpolateState(edge_state, c4->flow, c5->flow, c4->pos, c5->pos, edge_point);
				}
				interpolateState(temp_interf_state, edge_state, SI_interf.getExactRiemann(c->normal_R_x), edge_point, c->closest_point_R_x, c->flux_point_R_x);
				// if (debug){
				// 	cout << "normalW: " << c->normal_R_y << endl;
				// 	cout << "star state: " << SI_interf.getExactRiemann(c->normal_R_y);
				// 	cout << "temp_interf_state: " << temp_interf_state;
				// 	cout << "edge_state: " << edge_state;
				// }
				break;
			case 'N':
				m = get_gradient(c->closest_point_L_y, c->flux_point_L_y);
				if (m < -0.5) {
					edge_point = get_edge_point_y(c->flux_point_L_y, m, c2->pos.y);
					interpolateState(edge_state, c2->flow, c3->flow, c2->pos, c3->pos, edge_point);
				}
				else if (m < 0){
					edge_point = get_edge_point_x(c->flux_point_L_y, m, c1->pos.x);
					interpolateState(edge_state, c1->flow, c2->flow, c1->pos, c2->pos, edge_point);
				} 
				else if (m < 0.5){
					edge_point = get_edge_point_x(c->flux_point_L_y, m, c4->pos.x);
					interpolateState(edge_state, c4->flow, c5->flow, c4->pos, c5->pos, edge_point);
				} 
				else {
					edge_point = get_edge_point_y(c->flux_point_L_y, m, c3->pos.y);
					interpolateState(edge_state, c3->flow, c4->flow, c3->pos, c4->pos, edge_point);
				} 
				interpolateState(temp_interf_state, edge_state, SI_interf.getExactRiemann(c->normal_L_y), edge_point, c->closest_point_L_y, c->flux_point_L_y);
				// if (debug){
				// 	cout << "normalN: " << c->normal_R_y << endl;
				// 	cout << "star state: " << SI_interf.getExactRiemann(c->normal_R_y);
				// 	cout << "temp_interf_state: " << temp_interf_state;
				// 	cout << "edge_state: " << edge_state;
				// }
				break;
			case 'S':
				m = get_gradient(c->closest_point_R_y, c->flux_point_R_y);
				if (m < -0.5) {
					edge_point = get_edge_point_y(c->flux_point_R_y, m, c2->pos.y);
					interpolateState(edge_state, c2->flow, c3->flow, c2->pos, c3->pos, edge_point);
				}
				else if (m < 0.0){
					edge_point = get_edge_point_x(c->flux_point_R_y, m, c1->pos.x);
					interpolateState(edge_state, c1->flow, c2->flow, c1->pos, c2->pos, edge_point);
				} 
				else if (m < 0.5){
					edge_point = get_edge_point_x(c->flux_point_R_y, m, c4->pos.x);
					interpolateState(edge_state, c4->flow, c5->flow, c4->pos, c5->pos, edge_point);
				} 
				else {
					edge_point = get_edge_point_y(c->flux_point_R_y, m, c3->pos.y);
					interpolateState(edge_state, c3->flow, c4->flow, c3->pos, c4->pos, edge_point);
				} 
				interpolateState(temp_interf_state, edge_state, SI_interf.getExactRiemann(c->normal_R_y, debug), edge_point, c->closest_point_R_y, c->flux_point_R_y);
				if (debug){
					// cout << "normal: " << c->normal_R_y << endl;
					// cout << "star state: " << SI_interf.getExactRiemann(c->normal_R_y);
					// cout << "temp_interf_state: " << temp_interf_state;
					// cout << "edge_state: " << edge_state;
					cout << "edge_point: " << edge_point << "\n";
					cout << "closest_point: " << c->closest_point_R_y << "\n";
					cout << "flux_point: " << c->flux_point_R_y << "\n";
				}
				break;
			}
		return temp_interf_state;
}
void cell_threeD::determine_convex_neighbours(cell_threeD* cells, int i, int j, int n_cells_x){
	ii = i; jj = j;
	if (cells[INDX(i,j-1,n_cells_x)].contains_boundary){
		if (cells[INDX(i-1,j-1,n_cells_x)].is_active || cells[INDX(i+1,j-1,n_cells_x)].is_active || cells[INDX(i-1,j,n_cells_x)].is_active|| cells[INDX(i+1,j,n_cells_x)].is_active){
			if(cells[INDX(i,j-1,n_cells_x)].min_lose_convex < cells[INDX(i,j,n_cells_x)].min_lose_convex) conv_S = 2;
			else conv_S = 3;//is adjacent and next to boundary but wrong direction
		}
		else conv_S = 1;//is from the other side
	}
	else if (!cells[INDX(i,j-1,n_cells_x)].is_active) conv_S = 1;

	if (cells[INDX(i,j+1,n_cells_x)].contains_boundary){
		if (cells[INDX(i-1,j+1,n_cells_x)].is_active || cells[INDX(i+1,j+1,n_cells_x)].is_active|| cells[INDX(i-1,j,n_cells_x)].is_active|| cells[INDX(i+1,j,n_cells_x)].is_active){
			if(cells[INDX(i,j+1,n_cells_x)].min_lose_convex < cells[INDX(i,j,n_cells_x)].min_lose_convex) conv_N = 2;
			else conv_N = 3;//is adjacent and next to boundary but wrong direction
		}
		else conv_N = 1;//is from the other side
	}
	else if (!cells[INDX(i,j+1,n_cells_x)].is_active) conv_N = 1;

	if (cells[INDX(i-1,j,n_cells_x)].contains_boundary){
		if (cells[INDX(i-1,j+1,n_cells_x)].is_active || cells[INDX(i-1,j-1,n_cells_x)].is_active|| cells[INDX(i,j+1,n_cells_x)].is_active|| cells[INDX(i,j-1,n_cells_x)].is_active){
			if(cells[INDX(i-1,j,n_cells_x)].min_lose_convex < cells[INDX(i,j,n_cells_x)].min_lose_convex) conv_W = 2;
			else conv_W = 3;//is adjacent and next to boundary but wrong direction
		}
		else conv_W = 1;//is from the other side
	}
	else if (!cells[INDX(i-1,j,n_cells_x)].is_active) conv_W = 1;

	if (cells[INDX(i+1,j,n_cells_x)].contains_boundary){
		if (cells[INDX(i+1,j+1,n_cells_x)].is_active || cells[INDX(i+1,j-1,n_cells_x)].is_active|| cells[INDX(i,j+1,n_cells_x)].is_active|| cells[INDX(i,j-1,n_cells_x)].is_active){
			if(cells[INDX(i+1,j,n_cells_x)].min_lose_convex < cells[INDX(i,j,n_cells_x)].min_lose_convex) conv_E = 2;
			else conv_E = 0;//is adjacent and next to boundary but wrong direction
		}
		else conv_E = 3;//is from the other side
	}
	else if (!cells[INDX(i+1,j,n_cells_x)].is_active) conv_E = 1;
}