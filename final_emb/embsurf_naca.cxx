#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include "geom.h"
#include "embsurf.h"
#include "numerical.h"
#include "esp.h"
using namespace std;
#define h 0.51 //x position of leading edge
#define c 1.5
#define p 0.2
#define naca_y(tt,x) (5*tt*(0.2969*negnegsqrt(x) - 0.1260*x - 0.3516*x*x + 0.2843*x*x*x - 0.1015*x*x*x*x))
#define naca_deriv(t,c, x_c) (5*t/c*(0.5*0.2969/negsqrt(x_c) - 0.1260 - 2*0.3516*x_c + 3*0.2843*x_c*x_c - 4*0.1015*x_c*x_c*x_c))
#define naca_deriv_deriv(t,c, x_c) (1/c*5*t/c*(-0.5*0.5*0.2969/x_c/negsqrt(x_c) - 2*0.3516 + 2*3*0.2843*x_c - 4*3*0.1015*x_c*x_c))
double naca_camberline(double x_c, double m){
	if (x_c < p) return m/p/p*(2*p*x_c - x_c*x_c);
	else return m/(1-p)/(1-p)*((1-2*p)+2*p*x_c - x_c*x_c);
}
double naca_camberline_deriv(double x_c, double m){
	if (x_c < p) return 1/c*m/p/p*(2*p - 2*x_c);
	else return 1/c*m/(1-p)/(1-p)*(2*p - 2*x_c);
}
double naca_camberline_deriv_deriv(double x_c, double m){
	if (x_c < p) return 1/c/c*m/p/p*-2;
	else return 1/c/c*m/(1-p)/(1-p)*-2;
}
double negnegsqrt(double x){
	if (x > 0) return sqrt(x);
	else return -sqrt(-x);
}
double negsqrt(double x){
	if (x > 0) return sqrt(x);
	else return sqrt(-x);
}
double FF(vec v, double k, double m, double t, double x){
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m); 
	double y; 
	if (v.y - k - y_cam*c > 0) {
		y = y_c*c + k + y_cam*c;
		return (v.y - y)*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.x - x)*1/c;
	 }
	else {
		y = -y_c*c + k + y_cam*c;
		return (v.y - y)*(naca_deriv(t,c,x_c) -  naca_camberline_deriv(x_c, m)) - (v.x - x)*1/c;
	 }
}
double dFFdx(vec v, double k, double m, double t, double x){
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m); 
	double y;
	double dydx;
	if (v.y - k - y_cam*c > 0) {
		y = y_c*c + y_cam*c + k;
		dydx = c*naca_deriv(t,c,x_c) + c*naca_camberline_deriv(x_c, m);
		return -dydx*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.y - y)*(naca_deriv_deriv(t,c,x_c) + naca_camberline_deriv_deriv(x_c,m)) - 1/c;
	}
	else {
		y = -y_c*c + y_cam*c + k;
		dydx = -c*naca_deriv(t,c,x_c) + c*naca_camberline_deriv(x_c, m);
		return -dydx*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.y - y)*(naca_deriv_deriv(t,c,x_c) -  naca_camberline_deriv_deriv(x_c,m)) + 1/c;
	}

}
embsurf::embsurf(){
	ifstream inFile;
	inFile.open("height.dat");
	inFile >> k;
	inFile.close();
	inFile.open("thickness.dat");
	inFile >> t;
	inFile.close();
	inFile.open("camber.dat");
	inFile >> m;
	cout << "naca emb surf generated with k: " << k << ", t: " << t << ", m: " << m << endl;
}
double embsurf::phi(vec v){
	double x_c = (v.x - h)/c + 1e-12;
	if (x_c < 0.0 || x_c > 1.0) return abs(v.y - k)/c+1e-12;
	else return abs(v.y - k - c*naca_camberline(x_c, m)) / c - naca_y(t,x_c);
}

vec embsurf::get_closestpoint(vec v, int& success){
	function<double(double)> f = bind(FF,v,k,m,t,placeholders::_1);
	function<double(double)> dfdx = bind(dFFdx,v,k,m,t,placeholders::_1);
	double x = v.x; double y = v.y;
	if (x < h - 0.1*c || x > h + 1.1*c || y > k + 2*t*c || y < k - 2*t*c) return vec(0.0, 0.0, 0.0); // don't calculate closest poin if outisde some box that surrounds the airfoil
	success = newton_solve(v.x, f, dfdx, x, v.y);
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m);
	if (v.y - k - y_cam*c > 0) y = y_c*c + y_cam*c + k;
	else y = -y_c*c + y_cam*c + k;
	if(std::isnan(x)) x = 1e6;
	if(std::isnan(y)) y = 1e6;
	// if (phi(vec(x,y,0.0)) > 1e-9) fprintf(stderr, "closeset point not on surface: (%f, %f) \n", x,y);
	return vec(x, y, 0.0);
}
vec embsurf::get_intersectedpoint(vec va, vec vb){
	fprintf(stderr, "get_intersectedpoint not implemented for naca \n");
	exit(-1);
}
vec embsurf::get_normal(vec va){
	double x_c = (va.x - h)/c + 1e-12;
	double y_cam = naca_camberline(x_c, m);
	if (va.y - k -y_cam*c> 0) return vec(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m), -1/c, 0);
	else return vec(naca_deriv(t,c,x_c) - naca_camberline_deriv(x_c, m), 1/c, 0);

}
void embsurf::write_embsurf_to_vtk(int n_points, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	m_file<<"POINTS " << 4*n_points << " float\n";
	for(double i = 0; i!=n_points; i++){
		for(int k_z = 0; k_z!=2; k_z++){
			double x = i/n_points*c + h;
			double x_c = (x - h) / c;
			double y_c = naca_y(t,x_c);
			double y_cam = naca_camberline(x_c, m);
			double y = y_c*c + y_cam*c + k;
			m_file<<setw(12)<<x;
			m_file<<" ";
			m_file<<setw(12)<<y;
			m_file<<" ";
			m_file<<setw(12)<<k_z*0.05;
			m_file<<"\n";
		}
	}
	for(double i = 0; i!=n_points; i++){
		for(int k_z = 0; k_z!=2; k_z++){
			double x = -i/n_points*c + h + c;
			double x_c = (x - h) / c;
			double y_c = -naca_y(t,x_c);
			double y_cam = naca_camberline(x_c, m);
			double y = y_c*c + y_cam*c + k;
			m_file<<setw(12)<<x;
			m_file<<" ";
			m_file<<setw(12)<<y;
			m_file<<" ";
			m_file<<setw(12)<<k_z*0.05;
			m_file<<"\n";
		}
	}

	m_file << "CELLS " << 2*n_points << " " << 10*n_points << "\n";
	for(int i = 0; i!=2*n_points-1; i++){
			m_file << "4 " << 2*i << " "
					       << 2*i + 1 << " "
					       << 2*i + 3 << " "
					       << 2*i + 2 << "\n";
	}
	m_file << "4 " << 4*n_points -2 << " "
				  << 4*n_points - 1 << " "
				  << "1" << " "
				  << "0" << "\n";
	m_file<<"CELL_TYPES "<< 2*n_points << "\n";
	for(int i = 0; i!=2*n_points; i++){
		m_file <<"9\n";
	}
}
embsurfpoint* embsurf::generate_embsurfpoints(int n_points){
	embsurfpoint* esps = new embsurfpoint[2*n_points];
	double x0, x1, y0, y1, x_c, y_c;
	x0 = h; y0 = k;
	for(int i = 0; i!=n_points; i++){
			x1 = (i+1)*c/n_points + h;
			x_c = (x1 - h) / c;
			y_c = naca_y(t,x_c);
			y1 = y_c*c + c*naca_camberline(x_c, m) + k;
			esps[i] = embsurfpoint(vec(0.5*(x0 + x1), 0.5*(y0 + y1), 0), vec(y1 - y0, x0 - x1, 0.0),sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
			x0 = x1; y0 = y1;
	}
	x0 = h; y0 = k;
	for(int i = 0; i!=n_points; i++){
			x1 = (i+1)*c/n_points + h;
			x_c = (x1 - h) / c;
			y_c = -naca_y(t,x_c);
			y1 = y_c*c + c*naca_camberline(x_c, m) + k;
			esps[n_points+i] = embsurfpoint(vec(0.5*(x0 + x1), 0.5*(y0 + y1), 0), vec(y0 - y1, x1 - x0, 0), sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0)));
			x0 = x1; y0 = y1;
	}
	return esps;
}

double FF1(vec v, double k, double m, double t, int up, double x){
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m); 
	double y; 
	if (up) {
		y = y_c*c + k + y_cam*c;
		return (v.y - y)*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.x - x)*1/c;
	 }
	else {
		y = -y_c*c + k + y_cam*c;
		return (v.y - y)*(naca_deriv(t,c,x_c) -  naca_camberline_deriv(x_c, m)) - (v.x - x)*1/c;
	 }
}
double dFFdx1(vec v, double k, double m, double t, int up, double x){
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m); 
	double y;
	double dydx;
	if (up) {
		y = y_c*c + y_cam*c + k;
		dydx = c*naca_deriv(t,c,x_c) + c*naca_camberline_deriv(x_c, m);
		return -dydx*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.y - y)*(naca_deriv_deriv(t,c,x_c) + naca_camberline_deriv_deriv(x_c,m)) - 1/c;
	}
	else {
		y = -y_c*c + y_cam*c + k;
		dydx = -c*naca_deriv(t,c,x_c) + c*naca_camberline_deriv(x_c, m);
		return -dydx*(naca_deriv(t,c,x_c) + naca_camberline_deriv(x_c, m)) + (v.y - y)*(naca_deriv_deriv(t,c,x_c) -  naca_camberline_deriv_deriv(x_c,m)) + 1/c;
	}
}
vec embsurf::get_closestpoint(vec v, int upflag, int& success){
	function<double(double)> f = bind(FF1,v,k,m,t,upflag,placeholders::_1);
	function<double(double)> dfdx = bind(dFFdx1,v,k,m,t,upflag,placeholders::_1);
	double x = v.x; double y = v.y;
	if (x < h - 0.1*c || x > h + 1.1*c || y > k + 2*t*c || y < k - 2*t*c) return vec(0.0, 0.0, 0.0); // don't calculate closest poin if outisde some box that surrounds the airfoil
	success = newton_solve(v.x, f, dfdx, x, v.y);
	double x_c = (x - h)/c + 1e-12;
	double y_c = naca_y(t,x_c);
	double y_cam = naca_camberline(x_c, m);
	if (upflag) y = y_c*c + y_cam*c + k;
	else y = -y_c*c + y_cam*c + k;
	if(std::isnan(x)) x = 1e6;
	if(std::isnan(y)) y = 1e6;
	// if (phi(vec(x,y,0.0)) > 1e-9) fprintf(stderr, "closeset point not on surface: (%f, %f) \n", x,y);
	return vec(x, y, 0.0);
}