rm post/*.vtk
g++ -std=c++11 -I /usr/local/include/eigen/ -o jho_fiver.out main.cxx embsurf_naca_discrete.cxx esp.cxx thermo.cxx flow_threeD.cxx geom.cxx cell.cxx interpolate.cxx post.cxx roe.cxx stegerwarming.cxx matrix.cxx ausmdv.cxx numerical.cxx force.cxx
./jho_fiver.out

