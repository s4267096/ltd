#include <cmath>
#include <fstream>
#include <functional>
#include <iomanip>
#include "geom.h"
#include "numerical.h"
using namespace std;
#define a 0.8
#define b 0.2
#define h 1.1
double get_h_fromfile(string filename){
	ifstream inFile;
	inFile.open(filename);
	double hh;
	inFile >> hh;
	return hh;
}
double embsurf(vec v){
	double k = get_h_fromfile("quad_c.dat");
	double phi = ((v.x - h)/a)*((v.x - h)/a) + ((v.y - k)/b)*((v.y - k)/b) - 1.0;
	return phi;
}
double f1(double x, double y){
	return embsurf(vec(x, y, 0.0));
}
double df1dx(double x, double y){
	return 2*(x - h)/a/a;
}
double df1dy(double x, double y){
	double k = get_h_fromfile("quad_c.dat");
	return 2*(y - k)/b/b;
}
double f2(vec v, double x, double y){
	double k = get_h_fromfile("quad_c.dat");
	return -(v.y - y)*(x - h)/a/a + (v.x - x)*(y - k)/b/b;
}
double df2dx(vec v, double x, double y){
	double k = get_h_fromfile("quad_c.dat");
	return -(v.y - y)/a/a - (y - k)/b/b;
}
double df2dy(vec v, double x, double y){
	double k = get_h_fromfile("quad_c.dat");
	return (x - h)/a/a + (v.x - x)/b/b;
}
vec get_closestpoint(vec v){
	function<double(double, double)> f2_ = bind(f2, v, placeholders::_1, placeholders::_2);
	function<double(double, double)> df2dx_ = bind(df2dx, v, placeholders::_1, placeholders::_2);
	function<double(double, double)> df2dy_ = bind(df2dy, v, placeholders::_1, placeholders::_2);
	double x = v.x; double y = v.y;
	newton_solve(v.x, v.y, f1, f2_, df1dx, df1dy, df2dx_, df2dy_, x, y);
	return vec(x, y, 0.0);
}
vec get_intersectedpoint(vec va, vec vb){
	double k = get_h_fromfile("quad_c.dat");
	double x,y;
	if (abs(va.y - vb.y) < 1e-4){
		double square = (va.y - k)/b*(va.y - k)/b;
		if ((vb.x - va.x) > 0) x = h - a*sqrt(max(1 - square,0.0));
		else x = h + a*sqrt(max(1 - square,0.0));
		// cout << "pos: " << va.x << ", interf: " << vb.x << ", sur: " << x << "\n";
		if (square > 0) y = va.y;
		else y = (va.y < k) ? k - b : k + b;
		return vec(x, y, 0.0);
	}
	else if (abs(va.x - vb.x) < 1e-4){
		double square = (va.x - h)/a*(va.x - h)/a;
		if ((vb.y - va.y) > 0) y = k - b*sqrt(max(1 - square,0.0));
		else y = k + b*sqrt(max(1 - square,0.0));
		// cout << "pos: " << va.y << ", interf: " << vb.y << ", sur: " << y << "\n";
		if (square > 0) x = va.x;
		else x = (va.x < h) ? h - a : h + a;
		return vec(x, y, 0.0);
	}
	else{
		fprintf(stderr, "mesh not cartesian, get_intersected_point for elliptical embedded surface not implemented yet, exiting\n");
		exit(-1);
	}
}
vec get_normal(vec v){
	double k = get_h_fromfile("quad_c.dat");
	return vec(-2*(v.x - h)/a/a, -2*(v.y - k)/b/b, 0);
}
void write_embsurf_to_vtk(int n_points, string filename){
	double k = get_h_fromfile("quad_c.dat");
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	m_file<<"POINTS " << 2*n_points << " float\n";
	for(double i = 0; i!=n_points; i++){
		for(int k_z = 0; k_z!=2; k_z++){
			m_file<<setw(12)<<h+a*cos(2*3.14159*i/n_points);
			m_file<<" ";
			m_file<<setw(12)<<k+b*sin(2*3.14159*i/n_points);
			m_file<<" ";
			m_file<<setw(12)<<k_z*0.05;
			m_file<<"\n";
		}
	}
	m_file << "CELLS " << n_points << " " << 5*n_points << "\n";
	for(int i = 0; i!=n_points-1; i++){
			m_file << "4 " << 2*i << " "
					       << 2*i + 1 << " "
					       << 2*i + 3 << " "
					       << 2*i + 2 << "\n";
	}
	m_file << "4 " << 2*(n_points-1) << " "
				  << 2*(n_points-1) + 1 << " "
				  << "1" << " "
				  << "0" << "\n";
	m_file<<"CELL_TYPES "<< n_points << "\n";
	for(int i = 0; i!=n_points; i++){
		m_file <<"9\n";
	}
}