import sys
file1 = open(sys.argv[1],'r')
file2 = open(sys.argv[2],'r')
fileout = open(sys.argv[3],'w')
linesafterpressure = 0
incrementdiff = False
for line1 in file1:
	line2 = file2.readline()
	if "SCALARS" in line1:
		linesafterpressure = 0
		incrementdiff = False
	if linesafterpressure > 1:
		fileout.write(str(float(line2[:-1]) - float(line1[:-1])))
		fileout.write("\n")
	else:
		fileout.write(line1)
	if "pressure" in line1:
		incrementdiff = True
	if incrementdiff:
		linesafterpressure += 1
	# print(line1)
	# print(linesafterpressure)