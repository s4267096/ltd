#ifndef EMBSURF
#define EMBSURF
#include <cmath>
#include <fstream>
#include <iomanip>
#include <vector>
#include "geom.h"
#include "numerical.h"
#include "esp.h"
// #define N 100
class embsurf{
	public:
		virtual double phi(vec) = 0;
		virtual void generate_own_embsurfpoints() = 0;
		virtual int get_closestpoint_and_normal(vec, vec&, vec&) = 0;
		embsurfpoint *esps;
		int Npoints;
		void write_embsurf_to_vtk(string);
		void write_embpoints(string);
		void write_sdesign_input(string);
};
inline void embsurf::write_embsurf_to_vtk(string filename){
	int n_points = Npoints/2;
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	m_file<<"POINTS " << 4*n_points << " float\n";
	double x,y;
	for(int i = 0; i!=2*n_points; i++){
		for(int k_z = 0; k_z!=2; k_z++){
			x = esps[i].pos.x;
			y = esps[i].pos.y;
			m_file<<setw(12)<<x;
			m_file<<" ";
			m_file<<setw(12)<<y;
			m_file<<" ";
			m_file<<setw(12)<<k_z*0.05;
			m_file<<"\n";
		}
	}

	m_file << "CELLS " << 2*n_points << " " << 10*n_points << "\n";
	for(int i = 0; i!=2*n_points-1; i++){
			m_file << "4 " << 2*i << " "
					       << 2*i + 1 << " "
					       << 2*i + 3 << " "
					       << 2*i + 2 << "\n";
	}
	m_file << "4 " << 4*n_points -2 << " "
				  << 4*n_points - 1 << " "
				  << "1" << " "
				  << "0" << "\n";
	m_file<<"CELL_TYPES "<< 2*n_points << "\n";
	for(int i = 0; i!=2*n_points; i++){
		m_file <<"9\n";
	}
}
inline void embsurf::write_embpoints(string filename){
	int n_points = Npoints/2;
	ofstream m_file;
	m_file.open(filename, ios::out);
	double x,y;
	for(int i = 0; i!=2*n_points; i++){
			x = esps[i].pos.x;
			y = esps[i].pos.y;
			m_file<<setw(12)<<x;
			m_file<<" ";
			m_file<<setw(12)<<y;
			m_file<<" ";
			m_file<<setw(12)<<0.0;
			m_file<<"\n";
	}
}
inline void embsurf::write_sdesign_input(string filename){
	int n_points = Npoints/2;
	ofstream m_file;
	m_file.open(filename, ios::out);
	double x,y;
	m_file << "FENODES \n";
	for(int i = 0; i!=2*n_points; i++){
			x = esps[i].pos.x;
			y = esps[i].pos.y;
			m_file<<i+1<<" ";
			m_file<<setw(12)<<x;
			m_file<<" ";
			m_file<<setw(12)<<y;
			m_file<<" ";
			m_file<<setw(12)<<0.0;
			m_file<<"\n";
	}
	m_file << "END \n";
}
class embsurf_naca : public embsurf 
{
	public:
		embsurf_naca();
		double k;
		double t;
		double m;
		double phi(vec);
		void generate_own_embsurfpoints();
		int get_closestpoint_and_normal(vec, vec&, vec&);
	private:
		double cutoff_factor;
};
class bezier_airfoil : public embsurf
{
public: 
	bezier_airfoil();
	double phi(vec);
	void generate_own_embsurfpoints();
	int get_closestpoint_and_normal(vec, vec&, vec&);
private:
	int Nbezier;
	std::vector<vec> p; //bezier points
	std::vector<vec> dp; //bezier gradients
	int Nperbezier = 20;

};
class discrete_airfoil : public embsurf
{
public: 
	discrete_airfoil(int);
	double phi(vec);
	void generate_own_embsurfpoints();
	int get_closestpoint_and_normal(vec, vec&, vec&);
private:
	std::vector<vec> p; //bezier points
        int loop;
};

#endif
