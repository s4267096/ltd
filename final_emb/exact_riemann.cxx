#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include "thermo.h"
using namespace std;
flow_props getExactRiemannState(flow_props f, gas_model* gm_m){
	double g = gm_m->m_gamma;
	flow_props f_star;
	if (f.m_vel.x > 0){//shock
		double b = (g+1)/2*f.m_vel.x/gm_m->sound_speed(f);
		double M1 = (b + sqrt(b*b + 4))/2;
		double rho2_rho1 = (g+1)/2*M1*M1/(1+(g-1)/2*M1*M1);
		double p2_p1 = (2*g/(g-1)*M1*M1 - 1)/(g+1)*(g-1);
		vec velstar(0.0, f.m_vel.y, f.m_vel.z);
		f_star = flow_props_from_rho_p(rho2_rho1*f.m_rho, p2_p1*f.m_p, velstar, gm_m);
	} 
	else{//rarefaction
		double c1 = gm_m->sound_speed(f);
		double c2 = f.m_vel.x*(g-1)/2 + c1;
		double p2 = f.m_p*pow(c2/c1,2*g/(g-1));
		double rho2 = f.m_rho*pow(p2/f.m_p,1/g);
		vec velstar(0.0, f.m_vel.y, f.m_vel.z);
		f_star = flow_props_from_rho_p(rho2, p2, velstar, gm_m);
	}
	return f_star;
}
