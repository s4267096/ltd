#include <iostream>
#include <sstream> 
#include <fstream>
#include <iomanip> 
#include <cmath>
#include <map>
#include <omp.h>
#include "solver.h"
#include "thermo.h"
#include "cell.h"
#include "interpolate.h"
#include "post.h"
#include "force.h"
#include "embsurf.h"
#include "stegerwarming.h"
#include "roe.h"
//Set up overal parameters to govern solutioni
#define timesteppinglocal 0
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
#define fluxFuncName stegerwarming 
using fluxFunc = int (*)(flow_props, flow_props, flux&, gas_model*);
void reconstruct_and_compute_fluxes(cell_threeD*, flux*, gas_model&, int, int, int, int, std::map<string,int>&, int);
void run_auxiliary_embedded(int&, int*, int*, cell_threeD*, cell_threeD*, int, int, int, int, int, int, double, double, double&, flux*, gas_model&, double&, double&, double, std::map<string,int>&);
void read_input_file(string,std::map<string,int>&);
void read_inlet_file(string,std::map<string,double>&);
using namespace std;
//level set function here
//main
int main()
{
	double start_setup_time = omp_get_wtime();
	std::map<string,int> ioData;
	read_input_file("FluidFile",ioData);
	omp_set_num_threads(ioData["threads"]);
	int N = ioData["gridsize"];
	std::map<string,double> inletData;
	read_inlet_file("InletFile",inletData);
	//set up freestream conditions
	double p_inf = inletData["p"]; double T_inf = inletData["T"]; double M_inf = inletData["M"]; double delta = inletData["delta"]/180.0*3.14159;
 	gas_model air(1.4,0.02897);
 	gas_model air_copy(1.4,0.02897);
	flow_props flow_freestream_non_dim = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf, delta, &air);
	flow_props flow_freestream_copy = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf, delta, &air_copy);
	//flow_freestream_non_dim.non_dimensionalise(flow_freestream_copy);//modifies the gas model as well
	//setup solver
	cout << "about to make main_solver \n";
	solver main_solver(N, ioData, flow_freestream_non_dim);
	cout << "finished making main solver \n";
	//set up temporal discretisation and intervals at which to print/ write solution
	double initCFL = 0.4;
	double CFL = initCFL;
	int n_tsteps = ioData["ntsteps"];
	constexpr double resid_tol = 1e-9;
	int print_n_interval = ioData["interval"];//interval at which to print residual to terminal
	int print_resid_interval = ioData["interval"];//interval at which residual is written to text file
	double dt = 0.000000000001;//initial time step
	//fill entire domain with freestream conditions thereby setting intial and boundary conditions
	//Begin simulation by updating in time
	double t_elapsed=0;
	//initialize cell array which excludes ghost cells outside the boundary
	cell_threeD* cells_no_ghost = new cell_threeD[N*N];
	//set up text file for writing residual at various time steps
	ofstream m_file;
	string residualfilename;
	if (timesteppinglocal) residualfilename = "residuallocaldt.dat";
	else residualfilename = "residualglobaldt.dat";
	m_file.open(residualfilename, ios::out);
	m_file << "# iteration average_residual";
	double end_setup_time = omp_get_wtime();
    cout << "setup loop: " << end_setup_time - start_setup_time << endl;
	double rel_resid, rel_aux_resid, init_aux_resid, init_resid;
	double start_time_loop = omp_get_wtime();
	for(int i_t=0; i_t!=n_tsteps; ++i_t){	
		int debugFlag = 0;
		if (i_t % print_n_interval == 0){
			//assign correct flow properties to non-ghost cells for writing to text file
			main_solver.write_cells_no_ghost(cells_no_ghost);
			//write solution to *.vtk file for viewing in paraview
			stringstream vtkfilename;
			vtkfilename << "post/N"<<N<<"t00"<<i_t<<".vtk";
			write_to_vtk(N, N, 1, main_solver.L.x, main_solver.L.y, main_solver.L.z, vtkfilename.str());
			//write_cell_data(cells_no_ghost, flow_freestream_copy, N*N,vtkfilename.str());
			write_cell_data(cells_no_ghost, N*N,vtkfilename.str());
			//write_to_vtk(N+4, N+4, 1, main_solver.L.x, main_solver.L.y, main_solver.L.z, vtkfilename.str());
			//write_cell_data(main_solver.cells0, flow_freestream_copy, (N+4)*(N+4),vtkfilename.str());
		}
		if (i_t > 50000) debugFlag = 1;
		//do half step
		//fprintf(stderr,"i: %d, dt: %f \n", i_t, dt);
		main_solver.reconstruct_and_compute_fluxes(1);
		//fprintf(stdout,"b\n");
		main_solver.update_in_time(1,0.5*dt, CFL);//integrate fluxes half a time step using forward euler explicit
		//fprintf(stdout,"b\n");
		main_solver.run_auxiliary_embedded(1,0.5*dt, CFL);
		//fprintf(stdout,"b\n");
		//Do full step now
		main_solver.reconstruct_and_compute_fluxes(0);
		main_solver.update_in_time(0,0.5*dt,CFL);//integrate fluxes half a time step using forward euler explicit
		main_solver.run_auxiliary_embedded(0,dt, CFL);
		//fprintf(stdout,"c\n");
		if (CFL < 0.6) CFL *= 1.0002;
		if (CFL < 0.000001){
			fprintf(stderr, "CFL below 0.000001, exiting at n: %d \n", i_t);
			break;
		}
		if (i_t == 0) {
			if (ioData["restart"]){
				ifstream inFile("init_resids.dat");
				string val;
				getline(inFile,val);
				init_resid = stod(val);
				if(ioData["SI"] == 2){
					getline(inFile,val);
					init_aux_resid = stod(val);
				}
			}
			else{
				init_resid = main_solver.average_resid;
		 		init_aux_resid = main_solver.average_aux_resid;
			}
		 }
		rel_resid = main_solver.average_resid/init_resid;
		rel_aux_resid = main_solver.average_aux_resid/init_aux_resid;
		if (rel_resid < resid_tol) break;
		// assign dt for next time step and total elapsed time if using global time stepping
		if (!timesteppinglocal) {
					t_elapsed += dt;
					dt=CFL*main_solver.L.x/main_solver.max_a;
		}
		//write residual to file and print solution
		if (i_t % print_resid_interval == 0){
			vec F;
			ofstream m_file;
			m_file.open(residualfilename, ios::out | ios::app);
			m_file << setw(8) << i_t << " " << setw(8) << rel_resid << "\n";
			cout << "n: " << i_t << "; CFL: " << CFL << "; rel res: " << rel_resid << "; aux rel res: " << rel_aux_resid << "; F: " << F << "\n";
		}

	}
	double end_time_loop = omp_get_wtime();
    cout << "time loop: " << end_time_loop - start_time_loop << endl;
	//assign correct flow properties to non-ghost cells for writing to text file
	main_solver.write_cells_no_ghost(cells_no_ghost);
	//write soution at final time step to file
	vec F;
	if (ioData["forceorder"]==2) F = main_solver.calculate_force_hor("post/force.dat");
	else F = main_solver.calculate_force_zero();
	stringstream vtkfilename;
	vtkfilename << "post/N"<<N<<"t00"<<n_tsteps<<".vtk";
	write_to_vtk(N, N, 1, main_solver.L.x, main_solver.L.y, main_solver.L.z, vtkfilename.str());
	//write_cell_data(cells_no_ghost, flow_freestream_copy, N*N, vtkfilename.str());
	write_cell_data(cells_no_ghost, N*N, vtkfilename.str());
	save_cell_data(main_solver.cells0,(N+4)*(N+4),"celldata.rst");
	ofstream resid_file;
	resid_file.open("init_resids.dat");
	resid_file << init_resid << "\n" << init_aux_resid << "\n";
	resid_file.close();
	write_point_data_vs_height("camber.dat", cells_no_ghost[INDX(N/2,N/2,N-4)].flow.m_p,"p_vs_embsurfheight.dat");//print out center cell pressure
	write_point_data_vs_height("thickness.dat", F.y,"F_vs_embsurfheight.dat");//print out force on airfoil
	write_point_data_vs_height("camber.dat", main_solver.n_active,"n_active_vs_embsurfheight.dat");//print out number of active cells
	write_point_data_vs_height("camber.dat", rel_resid,"resid_vs_embsurfheight.dat");//print out rel resid
	write_point_data_vs_height("camber.dat", rel_aux_resid ,"aux_resid_vs_embsurfheight.dat");//print out rel resid
	write_vector(F,"force_output.dat");
}
void read_input_file(string filename, std::map<string,int> &ioData){
	cout << "----Reading parameters from input file ------- \n";
	ifstream inputFile;
	inputFile.open(filename);
	string field;
	string val;
	if (inputFile.is_open()){
		while(!inputFile.eof()){
			getline(inputFile,field,'=');
			getline(inputFile,val);
			if (field=="SI"){
				cout << "SI value read from file\n";
				ioData[field] = stoi(val);
			}
			else if (field=="order"){
				cout << "order scheme read from file\n";
				ioData[field] = stoi(val);
			}
			else if (field=="normal"){
				cout << "normal read from file \n";
				ioData[field] = (val=="structure") ? 1 : 0;
			}
			else if (field=="ntsteps"){
				cout << "ntsteps read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="higherorder"){
				cout << "higher order read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="forceorder"){
				cout << "force order read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="interval"){
				cout << "print interval read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="horswitch"){
				cout << "horswitch read from file \n";
				ioData[field] = stoi(val);
			}
			else if (field=="roe"){
				cout << "roe flux being used \n";
				ioData[field]=stoi(val);
			}
			else if (field=="sw"){
				cout << "steger warming flux being used \n";
				ioData[field]=stoi(val);
			}
			else if (field=="kstar"){
				cout << "kstar for hor2 read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="gridsize"){
				cout << "grid size read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="restart"){
				cout << "restart read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="threads"){
				cout << "restart read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="naca"){
				cout << "naca read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="bezier"){
				cout << "bezier read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="discrete"){
				cout << "discrete read from file \n";
				ioData[field]=stoi(val);
			}
			else if (field=="loop"){
				cout << "loop read from file \n";
				ioData[field]=stoi(val);
			}
		}
	}
	cout << "----Finished reading parameters from input file ------- \n";
}
void read_inlet_file(string filename, std::map<string,double> &inletData){
	cout << "----Reading parameters from inlet file ------- \n";
	ifstream inputFile;
	inputFile.open(filename);
	string field;
	string val;
	if (inputFile.is_open()){
		while(!inputFile.eof()){
			getline(inputFile,field,'=');
			getline(inputFile,val);
			if (field=="M"){
				cout << "M = " << stod(val) << " read from file\n";
				inletData[field] = stod(val);
			}
			else if (field=="p"){
				cout << "p = " << stod(val) << " read from file\n";
				inletData[field] = stod(val);
			}
			else if (field=="T"){
				cout << "T = " << stod(val) << " read from file\n";
				inletData[field] = stod(val);
			}
			else if (field=="delta"){
				cout << "delta = " << stod(val) << "read from file\n";
				inletData[field] = stod(val);
			}
		}
	}
	cout << "----Finished reading parameters from inlet file ------- \n";
}
