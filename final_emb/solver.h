#ifndef SOLVERCLASS
#define SOLVERCLASS
#include <iostream>
#include <sstream> 
#include <fstream>
#include <iomanip> 
#include <cmath>
#include <map>
#include "thermo.h"
#include "cell.h"
#include "interpolate.h"
#include "post.h"
#include "force.h"
#include "embsurf.h"
#include "stegerwarming.h"
#include "gmres/solution.h"
class solver{
	public:
		solver(int, std::map<string,int>, const flow_props&);
		void reconstruct_and_compute_fluxes(int);
		void update_in_time(int, double, double&);
		void run_auxiliary_embedded(int, double, double&);
		void write_cells_no_ghost(cell_threeD*);
		void compute_flux_vector(Solution&,int);
		void compute_flux_auxiliary(Solution&,int);
		void perturb_cells(Solution&, double);
		void update_cells(Solution&);
		vec calculate_force_hor(string);
		vec calculate_force_zero();
		cell_threeD* cells0 = NULL;
		gas_model air;
		double max_a;
		vec L;
		double average_aux_resid;
		double average_resid;
		int n_active;
		int n_cells_containingboundary;
		int debugFlag = 0;
		int N_nonghost;
	private:
		embsurf *esf;//information about the embedded surface
		//simulation parameters
		int N;
		int n_cells_x;
		int n_cells_y;
		int n_cells_z = 5;
		cell_threeD *cells_halfstep = NULL;
		flux *fluxes = NULL;
		//
		int sor;
		int hor;
		int SI;
		int restart;
		int normal;
		int n_tsteps;
		int *contains_boundary_idxs_x;
		int *contains_boundary_idxs_y;
		//interpolation function
		interpFunc interpolateUpwind;
};
#endif
