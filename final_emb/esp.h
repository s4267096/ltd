#ifndef ESP
#define ESP
#include "geom.h"
class embsurfpoint{
public:
	embsurfpoint();
	embsurfpoint(vec, vec, double);
	vec pos;
	vec dn;
	vec tangent;
	double dA;
};
#endif