//some functions to do some simple thermodynamic calculations
#include <iostream>
#include <cmath>
#include <fstream>
#include "thermo.h"
#include <iomanip>
using namespace std;
gas_model::gas_model()
	:m_gamma{1.4}, m_M{0.02897}, m_R{R/0.02897}
{m_c_v = m_R/(m_gamma - 1);}
gas_model::gas_model(double gamma, double M)
	:m_gamma{gamma}, m_M{M}, m_R{R/M}
{m_c_v = m_R/(gamma - 1);}
void gas_model::update_from_rho_e(flow_props &f){
	f.m_T = f.m_e/m_c_v;
	f.m_p = m_R*f.m_rho*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_vel.mag()*f.m_vel.mag() + f.m_p/f.m_rho;
}
void gas_model::update_from_rho_T(flow_props &f){
	f.m_e = m_c_v*f.m_T;
	f.m_p = m_R*f.m_rho*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_vel.mag()*f.m_vel.mag() + f.m_p/f.m_rho;
}
void gas_model::update_from_rho_p(flow_props &f){
	f.m_T = f.m_p/f.m_rho/m_R;
	f.m_e = m_c_v*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_vel.mag()*f.m_vel.mag() + f.m_p/f.m_rho;
}
void gas_model::update_from_p_T(flow_props &f){
	f.m_rho=f.m_p/m_R/f.m_T;
	f.m_e=m_c_v*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_vel.mag()*f.m_vel.mag() + f.m_p/f.m_rho;
}
void gas_model::update_from_p_c(flow_props &f, double c){
	f.m_rho = m_gamma*f.m_p/c/c;
	f.m_T = f.m_p/f.m_rho/m_R;
	f.m_e = m_c_v*f.m_T;
}
void gas_model::non_dimensionalise(){
	m_R = m_R/m_c_v;
	m_c_v = 1.0;
}
double gas_model::sound_speed(flow_props f){
	if (f.m_T < 0) {
		fprintf(stderr, "negative Temperature, T = %f detected whilst calculating sound speed \n",f.m_T);
		exit(-1);
	}
	return sqrt(m_gamma*m_R*f.m_T);
}
