#ifndef POST
#define POST
#include <iostream>
#include "thermo.h"
#include "cell.h"
#include "geom.h"
using namespace std;
void write_to_vtk(int, int, int, double, double, double, string);
void write_cell_data(cell_threeD*, const int, string);
void write_cell_data(cell_threeD*, const flow_props&, const int, string);
void save_cell_data(cell_threeD* cells, const int n_cells, string filename);
void fill_cells_from_saved_txt(string filename, gas_model* gm, const int, cell_threeD* cells);

void write_pressure_for_matlab(cell_threeD*, const int, const int, string);
void write_pos_x_for_matlab(cell_threeD*, const int, const int, string);
void write_pos_y_for_matlab(cell_threeD*, const int, const int, string);
void write_point_data_vs_height(const char*, const double, const char*);
void write_point_data_vs_iteration(const int, const double, const char*);
void write_vector(const vec, const char*);
#endif
