import numpy as np
import sys
from math import *
x = np.linspace(-0.1, 3.1, sys.argv[2])
angle = float(sys.argv[1])
y = tan(angle/180.0*pi)*x + 0.05 
dat = np.zeros((len(y),3))
dat[:,0] = x
dat[:,1] = y
np.savetxt('flatplate.txt',dat,fmt='%16.16f')
