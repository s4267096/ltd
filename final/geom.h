#ifndef GEOM
#define GEOM
#include <iostream>
using namespace std;
class vec{
	public:
		double x, y, z;
		double mag();
		vec();
		vec(double, double, double);
		void switch_y_x();
		void switch_z_x();
};
ostream& operator<<(ostream&, const vec);
#endif