#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "post.h"
void write_cells_to_file(cell_oneD* cells, const int n_cells, double L, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<setw(14)<<"#x"<<setw(14)<<"rho"<<setw(14)<<"p"
		  <<setw(14)<<"u"<<setw(14)<<"T"
		  <<setw(14)<<"e"<<"\n";
	m_file.close();
	double dx = 2*L/n_cells;
	for (int i = 0; i != n_cells; i++){
		m_file.open(filename, ios::out | ios::app);
		m_file<<setw(14)<<-L + (i+0.5)*dx;
		m_file.close();
		(cells+i)->flow.write_to_file(filename);
	}
}

void write_to_vtk(int n_x, int n_y, int n_z, double dx, double dy, double dz, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	int n_points = (n_x + 1)*(n_y + 1)*(n_z +1);
	m_file<<"POINTS " << n_points << " float\n";
	for(int k = 0; k!=n_z+1; k++){
		for(int j = 0; j!=n_y+1; j++){
			for(int i = 0; i!=n_x+1; i++){
				m_file<<setw(8)<<i*dx;
				m_file<<" ";
				m_file<<setw(8)<<j*dy;
				m_file<<" ";
				m_file<<setw(8)<<k*dz;
				m_file<<"\n";
			}
		}
	}
	int n_xy_one = (n_x + 1)*(n_y+1);
	int n_cells = n_x*n_y*n_z;
	m_file << "CELLS " << n_cells << " " << 9*n_cells << "\n";
	for(int k = 0; k!=n_z; k++){
		for(int j = 0; j!=n_y; j++){
			for(int i = 0; i!=n_x; i++){
				m_file << "8 " << i +j*(n_x+1) + k*n_xy_one << " "
						       << i +j*(n_x+1) + k*n_xy_one + 1 << " "
						       << i +j*(n_x+1) + k*n_xy_one + n_x + 2 << " "
						       << i +j*(n_x+1) + k*n_xy_one + n_x + 1 << " "
						       << i +j*(n_x+1) + k*n_xy_one + n_xy_one << " "
						       << i +j*(n_x+1) + k*n_xy_one + 1 + n_xy_one << " "
						       << i +j*(n_x+1) + k*n_xy_one + n_x + 2 + n_xy_one << " "
						       << i +j*(n_x+1) + k*n_xy_one + n_x + 1 + n_xy_one << "\n";
			}
		}
	}
	m_file<<"CELL_TYPES "<< n_cells << "\n";
	for(int i = 0; i!=n_cells; i++){
		m_file <<"12\n";
	}
	m_file<<"CELL_DATA "<< n_cells <<"\n";
	m_file<<"SCALARS pressure float 1\n";
	m_file<<"LOOKUP_TABLE default\n";
	for(int i = 0; i!=n_cells; i++){
		if(i < n_cells/2) m_file << "100\n";
		else m_file << "200\n";
	}
}
// void write_cell_data(cell_threeD* cells, const int n_cells, string filename){
// 		ofstream m_file;
// 		m_file.open(filename, ios::out | ios::app);
// 		m_file<<"CELL_DATA "<< n_cells <<"\n";
// 		m_file<<"SCALARS pressure float 1\n";
// 		m_file<<"LOOKUP_TABLE default\n";
// 		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_p<<"\n";
// 		m_file<<"SCALARS Temperature float 1\n";
// 		m_file<<"LOOKUP_TABLE default\n";
// 		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_T<<"\n";
// 		m_file<<"SCALARS rho float 1\n";
// 		m_file<<"LOOKUP_TABLE default\n";
// 		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_rho<<"\n";
// }