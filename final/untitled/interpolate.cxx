#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
using namespace std;

double interpolateSecondOrderUpwind(double fi, double fi_1, double fi_2){
	/*
	|------------|------------|-------------|
	|            |            |             |
	|            |            |             |
	|    fi_2    |   fi_1    *|     f_i     |
	|            |            |             |
	|            |            |             |
	|------------|------------|-------------|
    Interpolated value at the face denoted by *
	*/
	//Second Order Upwind for computing interface values (left or right of interface) depending on a given list of cell centres us
	double r = (fi_1 - fi_2)/(fi - fi_1 +1e-12);
	r = max(r,0.0);
	double phi = 2*r/(1+r);//van leer's limiter
	return fi_1 + 0.5*phi*(fi - fi_1);
}

double interpolateFirstOrderUpwind(double fi, double fi_1, double fi_2){
	/*
	|------------|------------|-------------|
	|            |            |             |
	|            |            |             |
	|    fi_2    |   fi_1    *|     f_i     |
	|            |            |             |
	|            |            |             |
	|------------|------------|-------------|
    Interpolated value at the face denoted by *
	*/
	//First Order Upwind for computing interface values (left or right of interface) depending on a given list of cell centres us
	return fi_1;
}