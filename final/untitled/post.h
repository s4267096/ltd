#ifndef POST
#define POST
#include <iostream>
#include "thermo.h"
#include "cell.h"
using namespace std;
void write_cells_to_file(cell_oneD*,const int, double, string);
void write_to_vtk(int, int, int, double, double, double, string);
#endif