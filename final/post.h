#ifndef POST
#define POST
#include <iostream>
#include "thermo.h"
#include "cell.h"
using namespace std;
void write_to_vtk(int, int, int, double, double, double, string);
void write_cell_data(cell_threeD*, const int, string);
void write_pressure_for_matlab(cell_threeD*, const int, const int, string);
void write_pos_x_for_matlab(cell_threeD*, const int, const int, string);
void write_pos_y_for_matlab(cell_threeD*, const int, const int, string);
#endif