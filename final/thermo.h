#ifndef THERMO
#define THERMO
#include <iostream>
#include "flow_threeD.h"
using namespace std;
constexpr double R = 8.314; //should find a better way to do this
class gas_model{
		double m_M, m_c_v;
		double m_R;
	public:
		double m_gamma;
		gas_model(double,double);//constructor accepting gamma, M_w
		void update_from_rho_e(flow_props&);
		void update_from_rho_T(flow_props&);
		void update_from_rho_p(flow_props&);
		void update_from_p_T(flow_props&);
		void update_from_p_c(flow_props&, double);
		double sound_speed(flow_props);
};
#endif