#include <cmath>
#include "geom.h"
using namespace std;
//a simple class for three dimensional vectors in x, y, z
double vec::mag(){
	return sqrt(x*x + y*y + z*z);
}
vec::vec()
	:x{0}, y{0}, z{0}
{
}

vec::vec(double xx, double yy, double zz)
	:x{xx}, y{yy}, z{zz}
{
}
void vec::switch_y_x(){
	double a=x;
	x=y;
	y=a;
}
void vec::switch_z_x(){
	double a=x;
	x=z;
	z=a;
}
ostream& operator<<(ostream& info, const vec c){
	info << "{" << c.x << ", " << c.y << ", " << c.z <<"}";
	return info;
}
