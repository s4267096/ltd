/*
	Class definition of 3x3 matrix and 1x3 vector and associated matrix operations
*/
#include <cmath>
#include <algorithm>
#include "matrix.h"
vector3::vector3(double xx, double yy, double zz){
	x[0] = xx; x[1] = yy; x[2] = zz;
}
vector3::vector3()
{
	x[0] = 0.0; x[1] = 0.0; x[2] = 0.0;
}

vector3 vector3::scalarMult(double k){
	vector3 newVec;
	for(int i = 0; i != 3; i++) newVec.x[i] = k*x[i];
	return newVec;
}

matrix3::matrix3(){
}
matrix3 matrix3::transpose(){
	matrix3 trans_mat;
	for(int i = 0; i != 3; i++){
		for(int j = 0; j != 3; j++){
			trans_mat.A[i][j] = A[j][i];
		}
	}
	return trans_mat;
}

void matrix3::print(){
	 for(int i = 0; i != 3; i++){
        for(int j = 0; j != 3; j++){
            cout << "A["<<i<<","<<j<<"]: " << A[i][j] << "\n";
        }
    }
}
matrix3 multiply(matrix3 matA, matrix3 matB){
	matrix3 matResult;
	for(int i = 0; i != 3; i++){
		for(int j = 0; j != 3; j++){
			matResult.A[i][j] = 0;
			for(int k = 0; k != 3; k++){
			matResult.A[i][j] += matA.A[i][k]*matB.A[k][j];
			}
		}
	}
	return matResult;
}

vector3 multiply(matrix3 mat, vector3 vec){
	vector3 vecResult;
	for(int i = 0; i != 3; i++){
		for(int j = 0; j != 3; j++){
			vecResult.x[i] += mat.A[i][j]*vec.x[j];
		}
	}
	return vecResult;
}

vector3 add(vector3 a, vector3 b){
	vector3 vecResult;
	for(int i = 0; i != 3; i ++) vecResult.x[i] = a.x[i] + b.x[i];
	return vecResult;
}

vector3 subtract(vector3 a, vector3 b){
	vector3 vecResult;
	for(int i = 0; i != 3; i ++) vecResult.x[i] = a.x[i] - b.x[i];
	return vecResult;
}
double dot(vector3 a, vector3 b){
	double result = 0;
	for(int i = 0; i != 3; i ++) result += a.x[i]*b.x[i];
	return result;
}

void vector3::print(){
    cout << "["<<x[0]<<","<<x[1]<<","<<x[2]<<"]\n"; 
}
