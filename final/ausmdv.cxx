/** \file ausmdv.cxx
 * \ingroup eilmer3
 * \brief Wada and Liou's flux calculator.
 * 
 * Implemented from details in their AIAA paper 
 * with hints from Ian Johnston.
 * \verbatim
 * Y. Wada and M. -S. Liou (1994)
 * A flux splitting scheme with high-resolution and 
 * robustness for discontinuities.
 * AIAA-94-0083.
 * \endverbatim
 *
 * \author P. A. Jacobs
 *     Department of Mechanical Engineering
 *     The University of Queensland
 *
 *
 * \version
 * \verbatim
 * 26-Jun-97: Initial coding.
 * 08-Jul-97: Fix entropy fix (ironic, eh?)
 *            And finally debugged by Ian J.
 * 12-Oct-97: data structure for fluxes, array data
 * 15-Oct-97: vectorise for Cray
 * \endverbatim
 * 18-Dec-16: adapted for JHO's 1D code
 * 
 * \todo Really should get rid of the vector loop some time.
 */

#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include "thermo.h"
using namespace std;
/*------------------------------------------------------------*/

constexpr double K_SWITCH = 10.0;//how do we get constexpr to work
constexpr double C_EFIX = 0.125;

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int ausmdv(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel){
    double rL, rR;
    double pL, pR;
    double uL, uR;
    double aL, aR;
    double vL, vR;
    double wL, wR;
    double HL, HR;
    double pLrL, pRrR;
    double ML, MR;
    double eL, eR;
    double keL, keR;
    double alphaL, alphaR, am;
    double pLplus, pRminus;
    double uLplus, uRminus;
    double duL, duR;

    double p_half, ru_half, ru2_half;
    double dp, s, ru2_AUSMV, ru2_AUSMD;

    int caseA, caseB;
    double d_ua;

    gas_model m_gas_model = *gmodel;
    // int nsp = gmodel->get_number_of_species();
    // int nmodes = gmodel->get_number_of_modes();
    
    /*
     * Unpack the flow-state vectors for either side of the interface.
     * Store in work vectors, those quantities that will be neede later.
     */
    rL = f_L.m_rho;
    pL = f_L.m_p;
    pLrL = pL / rL;
    uL = f_L.m_vel.x;
    vL = f_L.m_vel.y;
    wL = f_L.m_vel.z;
    eL = f_L.m_e;
    aL = m_gas_model.sound_speed(f_L);
    keL = 0.5 * (uL * uL + vL * vL + wL * wL);
    HL = eL + pLrL + keL;

    rR = f_R.m_rho;
    pR = f_R.m_p;
    pRrR = pR / rR;
    uR = f_R.m_vel.x;
    vR = f_R.m_vel.y;
    wR = f_R.m_vel.z;
    eR = f_R.m_e;
    aR = m_gas_model.sound_speed(f_R);
    keR = 0.5 * (uR * uR + vR * vR + wR * wR);
    HR = eR + pRrR + keR;

    /*
     * This is the main part of the flux calculator.
     */
    /*
     * Weighting parameters (eqn 32) for velocity splitting.
     */
    alphaL = 2.0 * pLrL / (pLrL + pRrR);
    alphaR = 2.0 * pRrR / (pLrL + pRrR);
    /*
     * Common sound speed (eqn 33) and Mach numbers.
     */
    am = max(aL, aR);
    ML = uL / am;
    MR = uR / am;
    /*
     * Left state: 
     * pressure splitting (eqn 34) 
     * and velocity splitting (eqn 30)
     */
    duL = 0.5 * (uL + fabs(uL));
    if (fabs(ML) <= 1.0) {
	pLplus = pL * (ML + 1.0) * (ML + 1.0) * (2.0 - ML) * 0.25;
	uLplus =
	    alphaL * ((uL + am) * (uL + am) / (4.0 * am) - duL) +
	    duL;
    } else {
	pLplus = pL * duL / uL;
	uLplus = duL;
    }
    /*
     * Right state: 
     * pressure splitting (eqn 34) 
     * and velocity splitting (eqn 31)
     */
    duR = 0.5 * (uR - fabs(uR));
    if (fabs(MR) <= 1.0) {
	pRminus = pR * (MR - 1.0) * (MR - 1.0) * (2.0 + MR) * 0.25;
	uRminus =
	    alphaR * (-(uR - am) * (uR - am) / (4.0 * am) - duR) +
	    duR;
    } else {
	pRminus = pR * duR / uR;
	uRminus = duR;
    }
    /*
     * Mass Flux (eqn 29)
     */
    // The mass flux is relative to the moving interface.
    ru_half = uLplus * rL + uRminus * rR;
    /*
     * Pressure flux (eqn 34)
     */
    p_half = pLplus + pRminus;
    /*
     * Momentum flux: normal direction
     *
     * Compute blending parameter s (eqn 37),
     * the momentum flux for AUSMV (eqn 21) and AUSMD (eqn 21)
     * and blend (eqn 36).
     */
    dp = pL - pR;
    dp = K_SWITCH * fabs(dp) / min(pL, pR);
    s = 0.5 * min(1.0, dp);

    ru2_AUSMV = uLplus * rL * uL + uRminus * rR * uR;
    ru2_AUSMD = 0.5 * (ru_half * (uL + uR) -
		       fabs(ru_half) * (uR - uL));

    ru2_half = (0.5 + s) * ru2_AUSMV + (0.5 - s) * ru2_AUSMD;

    /*
     * Assemble components of the flux vector.
     */
    face.mass = ru_half;
    face.momentum.x = ru2_half + p_half;
    if (ru_half >= 0.0) {
    face.momentum.y = ru_half * vL;
    face.momentum.z = ru_half * wL;
	face.total_energy = ru_half * HL;
	face.tke = ru_half * f_L.get_tke();
	// face.omega = ru_half * f_L.get_omega();
    } else {
	/* Wind is blowing from the right */
    face.momentum.y = ru_half * vR;
    face.momentum.z = ru_half * wR;
    face.total_energy = ru_half * HR;
	face.tke = ru_half * f_R.get_tke();
	// face.omega = ru_half * f_R.get_omega();
    }
    /*
     * Apply entropy fix (section 3.5 in Wada and Liou's paper)
     */
    caseA = ((uL - aL) < 0.0) && ((uR - aR) > 0.0);
    caseB = ((uL + aL) < 0.0) && ((uR + aR) > 0.0);

    d_ua = 0.0;
    if (caseA && !caseB)
	d_ua = C_EFIX * ((uR - aR) - (uL - aL));
    if (caseB && !caseA)
	d_ua = C_EFIX * ((uR + aR) - (uL + aL));

    if (d_ua != 0.0) {
	face.mass -= d_ua * (rR - rL);
	face.momentum.x-= d_ua * (rR * uR - rL * uL);
    face.momentum.y -= d_ua * (rR * vR - rL * vL);
    face.momentum.z -= d_ua * (rR * wR - rL * wL);
	face.total_energy -= d_ua * (rR * HR - rL * HL);
	face.tke -= d_ua * (rR * f_R.get_tke() - rL * f_L.get_tke());
	// face.omega -= d_ua * (rR * f_R.get_omega() - rL * f_L.get_omega());
    }   /* end of entropy fix (d_ua != 0) */

    return 1;
}   /* end of ausmdv() */

/*--------------------------------------------------------------*/
