#include <iostream>
#include <cmath>
#include <fstream>
#include "thermo.h"
#include <iomanip>
using namespace std;
flow_props::flow_props()
	:m_rho{0}, m_vel{vec(0,0,0)},m_T{0}
{
}
flow_props flow_props_from_rho_T(double rho, double T, vec vel, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_vel = vel; f.m_T=T; f.gm=gm_m;
		f.gm->update_from_rho_T(f);
		return f;
}
flow_props flow_props_from_p_T_M_alpha(double p, double T, double M, double alpha, gas_model* gm_m)
{
		//alpha is already in radians
		flow_props f;
		f.m_p = p; f.m_T=T; f.gm=gm_m;
		double c = f.gm->sound_speed(f);
		f.m_vel = vec(M*c*cos(alpha), M*c*sin(alpha), 0.0);
		f.gm->update_from_p_T(f);
		return f;
}
flow_props flow_props_from_rho_p(double rho, double p, vec vel, gas_model* gm_m)
{
		flow_props f;
		f.m_rho = rho; f.m_p = p; f.m_vel = vel; f.gm=gm_m;
		f.gm->update_from_rho_p(f);
		return f;
}
flow_props mirror_flow_along_y(flow_props f){
	f.m_vel = vec(f.m_vel.x, -f.m_vel.y, f.m_vel.z);
	return f;
}
double flow_props::get_tke(){
	return 0.5*m_rho*m_vel.mag()*m_vel.mag();
}
double flow_props::get_total_energy(){
	return m_rho*m_e + 0.5*m_rho*m_vel.mag()*m_vel.mag();
}
vec flow_props::get_momentum(){
	return vec(m_rho*m_vel.x, m_rho*m_vel.y, m_rho*m_vel.z);
}
void flow_props::write_to_file(string filename){
	ofstream m_file;
	m_file.open(filename, ios::out | ios::app );
	m_file<<setw(12)<<m_rho<<setw(12)<<m_p
		  <<setw(12)<<m_vel.x<<setw(12)<<m_vel.y
		  <<setw(12)<<m_vel.z<<setw(12)<<m_T
		  <<setw(12)<<m_e<<"\n";
	m_file.close();
}
flux::flux()
	:mass{0}, momentum{vec(0,0,0)}, total_energy{0}, tke{0}, omega{0}
{
}
ostream& operator<<(ostream& info, const flow_props& c){
	info << "p: " << c.m_p << " T: " << c.m_T << " rho: "
	<< c.m_rho << " e: " << c.m_e << " vel: " << c.m_vel
	<<"\n";
	return info;
}
ostream& operator<<(ostream& info, const flux& c){
	info << "mass: " << c.mass << " mom: " << c.momentum 
	<<" TE: " << c.total_energy << "\n";
	return info;
}
