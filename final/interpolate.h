#ifndef INTERPOLATE
#define INTERPOLATE
double interpolateSecondOrderUpwind(double, double, double);
double interpolateFirstOrderUpwind(double, double, double);
#endif