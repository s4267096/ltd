#include <iostream>
#include <sstream> 
#include <fstream>
#include <iomanip> 
#include <cmath>
#include "thermo.h"
#include "cell.h"
#include "interpolate.h"
#include "post.h"
//Set up overal parameters to govern solutioni
#define N 150 //size of grid is N x N
#define timesteppinglocal 1
#define CFL 0.4
#define fluxFuncName roe
#define strfluxFuncName "roe"
#define orderinterp "secondorder"
#define interpFuncname interpolateSecondOrderUpwind
#define INDX(x,y,n_x) (((n_x)*(x) + (y))) //used to index a 2-dimensional array which is stored as a single dimension
using fluxFunc = int (*)(flow_props, flow_props, flux&, gas_model*);
int roe(flow_props, flow_props, flux&, gas_model*,char);
int ausmdv(flow_props, flow_props, flux&, gas_model*);
// #include "post.h"
using namespace std;


int main()
{
	//set up temporal discretisation and intervals at which to print/ write solution
	constexpr int n_tsteps = 10000;
	int print_n_interval = 1000;//interval at which to print residual to terminal
	int print_resid_interval = n_tsteps/200;//interval at which residual is written to text file
	double dt = 0.00001;//initial time step
	//set up spacial discretistation
	constexpr int n_cells_x = N+4;
	constexpr int n_cells_y = N+4;
	constexpr int n_cells_z = 5;
	double dL = 3.0/(N - 4);
	const vec L(dL, dL, dL);
	const double min_L = min(min(L.x, L.y), L.z);	
	//initialise cell objects in an array and fill with geometric information
	cell_threeD *cells = new cell_threeD[n_cells_x*n_cells_y];
	constexpr int nfluxes = 2*n_cells_x*n_cells_y  - 7*(n_cells_x + n_cells_y) + 24;
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
				vec pos(L.x*(i-2+0.5), L.y*(j-2+0.5),0);
				cells[INDX(i,j,n_cells_x)]=cell_threeD(L, pos);
		}
	}
	//Initialise flux objects in an array and link to relevant cells (left and right) by assigning poitners 
	flux *fluxes = new flux[nfluxes];
	int i_flux = 0;
	for(int i=2; i!=n_cells_x-1; i++){
		for(int j=2; j!=n_cells_y-2; j++){
				cells[INDX(i,j,n_cells_x)].fluxL_x=&fluxes[i_flux];
				cells[INDX(i-1,j,n_cells_x)].fluxR_x=&fluxes[i_flux];
				i_flux++;
		}
	}
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-1; j++){
				cells[INDX(i,j,n_cells_x)].fluxL_y=&fluxes[i_flux];
				cells[INDX(i,j-1,n_cells_x)].fluxR_y=&fluxes[i_flux];
				i_flux++;
		}
	}
	//Set up flow state object to represent free stream conditions
	double p_inf = 100000; double T_inf = 300; double M_inf = 2; double delta = -10/180.0*3.14159;
	gas_model air(1.4,0.02897);
	flow_props flow_freestream = flow_props_from_p_T_M_alpha(p_inf, T_inf, M_inf, delta, &air);
	cout << "freestream flow \n" << flow_freestream;
	//fill entire domain with freestream conditions thereby setting intial and boundary conditions
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
			cells[INDX(i,j,n_cells_x)].flow=flow_freestream;//left
		}
	}
	//Begin simulation by updating in time
	double max_a=0;//max wave speed over the domain
	double t_elapsed=0;
	//initialize cell array which excludes ghost cells outside the boundary
	cell_threeD* cells_no_ghost = new cell_threeD[(n_cells_x - 4)*(n_cells_y - 4)];
	//set up text file for writing residual at various time steps
	ofstream m_file;
	string residualfilename;
	if (timesteppinglocal) residualfilename = "residuallocaldt.dat";
	else residualfilename = "residualglobaldt.dat";
	m_file.open(residualfilename, ios::out);
	m_file << "# iteration average_resididual";
	for(int i_t=0; i_t!=n_tsteps; ++i_t){	
		//assign correct flow properties to non-ghost cells for writing to text file
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				cells_no_ghost[INDX(i-2,j-2,n_cells_x-4)]=cells[INDX(i,j,n_cells_x)];
				}
			}
		//write solution to *.vtk file for viewing in paraview
		if (i_t % print_n_interval == 0){
			stringstream vtkfilename;
			vtkfilename << "post/N"<<N<<"t00"<<i_t<<".vtk";
			write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
			write_cell_data(&cells_no_ghost[0],(n_cells_x-4)*(n_cells_y-4)*(n_cells_z-4),vtkfilename.str());
		}
		//set wall boundary conditions for bottom boundary by mirroring flow
		for(int i = 0; i != n_cells_x; i++){
			for(int j = 0; j !=2; j++){
				cells[INDX(i, j, n_cells_x)].flow = mirror_flow_along_y(cells[INDX(i, 3 - j, n_cells_x)].flow);
			}
		}
		//reconstruct flow state at cell interfaces in x,y direction for right and left
		for(int i=1; i!=n_cells_x-1; i++){
			for(int j=1; j!=n_cells_y-1; j++){
  					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_x,&interpFuncname,&cells[INDX(i-1,j,n_cells_x)],&cells[INDX(i+1,j,n_cells_x)]);
					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_x,&interpFuncname,&cells[INDX(i+1,j,n_cells_x)],&cells[INDX(i-1,j,n_cells_x)]);
					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfR_y,&interpFuncname,&cells[INDX(i,j-1,n_cells_x)],&cells[INDX(i,j+1,n_cells_x)]);
					cells[INDX(i,j,n_cells_x)].interpolate(cells[INDX(i,j,n_cells_x)].interfL_y,&interpFuncname,&cells[INDX(i,j+1,n_cells_x)],&cells[INDX(i,j-1,n_cells_x)]);
			}
		}
		//calculate fluxes based on these interface values, accounting for boundary conditions
		char BC;
		int i_flux = 0;
		//x-direction fluxes
		for(int i=2; i!=n_cells_x-1; i++){
			for(int j=2; j!=n_cells_y-2; j++){
					if (i == 2) BC = 'L'; //left boundary
					else if (i == n_cells_x - 3) BC = 'R';//right boundary
					else BC = 'N';
					fluxFuncName(cells[INDX(i-1,j,n_cells_x)].interfR_x, cells[INDX(i,j,n_cells_x)].interfL_x, fluxes[i_flux], &air, BC);
					i_flux++;
			}
		}
		//y-direction fluxes
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-1; j++){
					if (j == n_cells_y-3) BC = 'A'; //top boundary
					else BC = 'N';
					cells[INDX(i,j-1,n_cells_x)].interfR_y.m_vel.switch_y_x();
					cells[INDX(i,j,n_cells_x)].interfL_y.m_vel.switch_y_x();
					fluxFuncName(cells[INDX(i,j-1,n_cells_x)].interfR_y, cells[INDX(i,j,n_cells_x)].interfL_y, fluxes[i_flux], &air, BC);
					fluxes[i_flux].momentum.switch_y_x();
					i_flux++;
			}
		}
		//integrate fluxes in time using forward euler explicit
		double average_resid = 0.0; 
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				//calculate maximum wave speed for each cell and assign dt if local time stepping
				double abs_vel_x = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.x);		
				double abs_vel_y = fabs(cells[INDX(i,j,n_cells_x)].flow.m_vel.y);
				double max_abs_v = max(abs_vel_x, abs_vel_y);
				double c = air.sound_speed(cells[INDX(i,j,n_cells_x)].flow);
				if (timesteppinglocal)	dt=CFL*min_L/(max_abs_v + c);
				else max_a=max(max_a, max_abs_v + c);
				cells[INDX(i,j,n_cells_x)].update_in_time(dt);
				average_resid += cells[INDX(i,j,n_cells_x)].max_resid;
			}
		}
		average_resid /= (N - 4)*(N - 4);
		// assign dt for next time step and total elapsed time if using global time stepping
		if (!timesteppinglocal) {
					t_elapsed += dt;
					dt=CFL*min_L/max_a;
		}
		// write residual to file and print solution
		if (i_t % print_resid_interval == 0){
			ofstream m_file;
			m_file.open(residualfilename, ios::out | ios::app);
			m_file << setw(8) << i_t << " " << setw(8) << average_resid << "\n";
			if (timesteppinglocal)	cout << "n: " << i_t << "; average residual: " << average_resid << "\n";
			else cout << "n: " << i_t <<  "; t: " << t_elapsed << "; dt: " << dt << "; average residual: " << average_resid << "\n";//LOCAL TIME 
		}
	}
	//write soution at final time step to file
	stringstream vtkfilename;
	vtkfilename << "post/N"<<N<<"t00"<<n_tsteps<<".vtk";
	write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
	write_cell_data(&cells_no_ghost[0],(n_cells_x-4)*(n_cells_y-4),vtkfilename.str());
	//write solution to text file for MATLAB post processing
	stringstream p_final_filename;
	stringstream x_final_filename;
	stringstream y_final_filename;
	p_final_filename << "post_N/N" << N << "p.dat";
	x_final_filename << "post_N/N" << N << "x.dat";
	y_final_filename << "post_N/N" << N << "y.dat";
	write_pressure_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, p_final_filename.str());
	write_pos_x_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, x_final_filename.str());
	write_pos_y_for_matlab(&cells_no_ghost[0], n_cells_x-4, n_cells_y-4, y_final_filename.str());
	delete cells;
	delete cells_no_ghost;
	delete fluxes;
}