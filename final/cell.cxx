#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "thermo.h"
#include "cell.h"
using namespace std;
cell_threeD::cell_threeD(vec L_in, vec pos_in)
	:L{vec(L_in.x, L_in.y, L_in.z)}, pos{vec(pos_in.x, pos_in.y, pos_in.z)}
{
	get_areas();
}
cell_threeD::cell_threeD()
	:L{vec(0,0,0)}
{
}
ostream& operator<<(ostream& info, const cell_threeD& c){
	info << "Cell at: " << c.pos;
	return info;
}
double cell_threeD::get_volume(){
	return L.x*L.y*L.z;
}
void cell_threeD::interpolate(flow_props &interf, interpFunc f, cell_threeD* c_1, cell_threeD* c_2){
	interf.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interf.m_p = f(c_2->flow.m_p, flow.m_p, c_1->flow.m_p);
	interf.m_vel.x = f(c_2->flow.m_vel.x, flow.m_vel.x, c_1->flow.m_vel.x);
	interf.m_vel.y = f(c_2->flow.m_vel.y, flow.m_vel.y, c_1->flow.m_vel.y);
	interf.m_vel.z = f(c_2->flow.m_vel.z, flow.m_vel.z, c_1->flow.m_vel.z);
	flow.gm->update_from_rho_p(interf);
}
void cell_threeD::update_in_time(double dt){
	//calculate sum of the fluxes for conserved variables
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y);
	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y);
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//calculate maximum of residuals normalised by flow quantities
	double rho_resid = rhodash*dt/flow.m_rho; double velx_resid = momentumdash_x*dt/oldmomentum.x;
	double vely_resid = momentumdash_y*dt/oldmomentum.y; double e_resid = totalEdash*dt/(oldenergy - flow.get_tke());
	max_resid = max({rho_resid, velx_resid, vely_resid, e_resid});
	//integrate in time
	flow.m_rho += rhodash*dt;
	flow.m_vel.x = (oldmomentum.x + momentumdash_x*dt)/flow.m_rho;
	flow.m_vel.y = (oldmomentum.y + momentumdash_y*dt)/flow.m_rho;
	flow.m_vel.z = (oldmomentum.z + momentumdash_z*dt)/flow.m_rho;
	flow.m_e = (oldenergy + totalEdash*dt- flow.get_tke())/flow.m_rho ;
	flow.gm->update_from_rho_e(flow);
}
void cell_threeD::get_areas(){
	area.x = L.y*L.z;
	area.y = L.x*L.z;
	area.z = 0;//L.x*L.y; 2D simulation
}
cell_threeD cell_threeD::update_in_time2(double dt){
	//calculate sum of the fluxes for conserved variables
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y);
	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y);
	//get current conserved variables
	vec oldmomentum = flow.get_momentum();
	double oldenergy = flow.get_total_energy();
	//integrate in time and return a new cell
	cell_threeD newcell = cell_threeD(pos,L);
	newcell.flow.gm = flow.gm;
	newcell.flow.m_rho += flow.m_rho + rhodash*dt;
	newcell.flow.m_vel.x = (oldmomentum.x + momentumdash_x*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.y = (oldmomentum.y + momentumdash_y*dt)/newcell.flow.m_rho;
	newcell.flow.m_vel.z = (oldmomentum.z + momentumdash_z*dt)/newcell.flow.m_rho;
	newcell.flow.m_e = (oldenergy + totalEdash*dt- newcell.flow.get_tke())/newcell.flow.m_rho ;
	newcell.flow.gm->update_from_rho_e(newcell.flow);
	return newcell;
}
