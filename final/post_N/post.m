%% some shit
clear all; close all;
Xexact = importdata('N400x.dat').';
Yexact = importdata('N400y.dat').';
Pexact = importdata('N400p.dat').';
ns = [10,20, 50, 80,100,150,200,250];
errors = [];
i = 1;
for n = ns
    xname = ['N',num2str(n),'x.dat'];
    yname = ['N',num2str(n),'y.dat'];
    pname = ['N',num2str(n),'p.dat'];
    X1 = importdata(xname).';
    Y1 = importdata(yname).';
    P1 = importdata(pname).';
    P1_intermediate = zeros(length(Xexact), length(X1));
    P1_final = P1;
    for i = 1:length(Xexact)
        P1_intermediate(i,:) = interp1(Xexact(i,:), Pexact(i,:), X1(1,:),'linear','extrap');
    end
    for j = 1:length(X1)
        P1_final(:,j) = interp1(Yexact(:,j),P1_intermediate(:,j),Y1(:,1),'linear','extrap');
    end
    errors = [ errors abs(mean(mean((P1_final - P1)./P1)))];
    i = i + 1;
end

figure(1);
plot(ns, errors,'x');
xlabel('N - number of grid points')
ylabel('||e||_\infty')
hold on;
plot([0,300],[0,0],'k--')
% axis([0,300,-0.001,0.02])

saveas(1,'error.png')
