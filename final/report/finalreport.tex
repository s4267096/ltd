\documentclass[letterpaper,12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage[margin=0.75in]{geometry}
\usepackage{IEEEtrantools}
\usepackage{subcaption}
\usepackage{epstopdf}
\usepackage{listings}
\newcommand{\ieq}{\begin{IEEEeqnarray*}{rCl}}
	\newcommand{\ieeq}{\end{IEEEeqnarray*}}
\newcommand{\jeq}{&=&}
\newcommand{\degrees}{^\circ}
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
	basicstyle=\footnotesize\ttfamily,
	breaklines=true,
	language=MATLAB,
	commentstyle=\color{mygreen},
	keywordstyle=\color{blue},
	stringstyle=\color{mymauve}
}
\lstset{
	basicstyle=\footnotesize\ttfamily,
	breaklines=true,
	language=C++,
	commentstyle=\color{mygreen},
	keywordstyle=\color{blue},
	stringstyle=\color{mymauve}
}

\title{AA214B - Final Exam}
\author{Jonathan Ho}
\date{March 20 2017}

\begin{document}
	\maketitle
	\section*{Problem 2: Steady Flow Past a Wedge}
	\subsection*{1. Introduction to Flow Solver}
	A finite volume, cell-centered 2D compressible solver that uses a ghost cell approach for boundary conditions was used to analyse this problem. The code was adapted from a 3D code and some remnants of the 3D architecture may remain but were set to an appropriate value to make the simulation quasi 2D. In particular, fluxes and velocities in the $z$-direction were set to zero. 
	\subsubsection*{Flow Solver Architecture}
	Some existing code of the author in \texttt{C++} was exploited in solving this problem. It is set up for the possibility of doing more complex problems and as such, has multiple objects and modules defined. 
	
	\noindent \texttt{cell.h} contains a cell class which contains:
		\begin{itemize}
			\itemsep0em 
			\item a flow state object at the cell;
			\item a flow state object for the reconstructed left and right faces of the cell in the $x$ and $y$ direction;			
			\item pointers to a flux objects for left and right interfaces in the $x$ and $y$ direction;
			\item geometric information including the position and size of the cell
			\item a method for reconstruction interface values at left right given a certain interpolation method;
			\item an integration method to update the flow state based on fluxes.
			\item a residual that measures the convergence of the solution (this is defined later).
		\end{itemize}
		\texttt{flow\_oneD.h} is a flow state class which contains:
		\begin{itemize}
			\itemsep0em 
			\item flow state variables $\rho, \vec{v}, e, p, T, h$ where $e$ is specific internal energy and $h$ is specific stagnation enthalpy;
			\item a pointer to a gas model object;
			\item methods for computing total energy, momentum;
			\item	\text{flux} class contains mass, momentum (as a vector) and energy fluxes.
		\end{itemize}
		
		\noindent \texttt{thermo.h} is a gas model class for a thermally perfect ideal gas which updates a flow state based on two other thermodynamic variables such as $(\rho, p)$ or $p, c$. The header files are shown in Listing \ref{l1}--\ref{l4}.
		\texttt{Interpolate.h} contains a variety of first order and second order reconstruction methods at the scalar level.
	\lstinputlisting[label={l1},caption={Cell class header file}]{../cell.h}
	\lstinputlisting[label={l2},caption={Flow state class header file}]{../flow_threeD.h}
	\lstinputlisting[label={l3},caption={Thermodynamic gas model header file}]{../thermo.h}
	\lstinputlisting[label={l4},caption={Reconstruction methods header file}]{../interpolate.h}	
	\subsubsection*{Solution Procedure}
		 The following procedure was used to set up the simulation and is implemented in  \texttt{main.cxx} in Listing \ref{main}.
		\begin{enumerate}
			\item Define overall parameters for the simulation including:
			\subitem -- level of discretisation for a $N \times N$ grid
			\subitem -- initial time step $dt$
			\subitem -- time stepping scheme
			\subitem -- CFL
			\item Initiate array of cell objects of length $(N+4) \times (N+4)$ to account for two rows of ghost cells on either boundary;
			\item Initiate array of flux objects of appropriate length for a $N \times N$ grid and link these fluxes to the appropriate cells by assigning flux pointers in the cell object;
			\item Initiate the gas model using $\gamma = 1.4$ and $M_w = 0.02897$;
			\item Initiate a flow state object for the free-stream conditions at $p_\infty = 100,000$ Pa, $T_\infty = 300$ K, $M_\infty = 2.0$ Pa and $\alpha = -10^\circ$ where $\alpha$ is the angle of attack of the incoming flow. The velocities are calculated in the function \texttt{flow\_props\_from\_p\_T\_M\_alpha} in Listing \ref{flow} (see Appendix) using the equations:
			\subitem $v_x = M_\infty \sqrt{\gamma R T_\infty} \cos \alpha$
			\subitem $v_y = M_\infty \sqrt{\gamma R T_\infty} \sin \alpha$
			\item Fill all cells including ghost cells with flow state objects according to the free-stream flow state.
		\end{enumerate}
		The simulation was executed in a time loop. At each time loop:
		\begin{enumerate}
			\item The solution in the cells excluding the ghost cells is written to a \texttt{*.vtk} file for viewing in paraview,
			\item On the bottom boundary, the wall boundary condition is implemented by mirroring flow properties (more details in subsequent sections)
			\item At the left and right interfaces for each cell for both the $x$ and $y$ directions, reconstruct the flow state based on either a first or second order method that may take flow states from adjacent cells. 
			\item At each interface in the $x$ direction calculate the flux by using Roe's approximate solver in 1D and taking the velocity to be $v_x$;
			\item At each interface in the $y$ direction, first switch the $x$ and $y$ velocities, then calculate the flux using Roe's approximate solver in 1D and taking the velocity to be $v_x$ (which is really $v_y$). Switch the calculated momentum fluxes in the $x$ and $y$ direction (more details in subsequent sections) 
			\item Update the flow state in each cell using time integration using a particular value of $dt$ depending on the scheme. The value of $dt$ used is explained later.
		\end{enumerate}
		\lstinputlisting[caption={Main routine},label={main}]{../main.cxx}
		\subsubsection*{Wall Boundary Conditions}
		Wall boundary conditions are implemented by instantiating a copy of the flow state at the boundary, but reversing the sign of the relevant vector components (namely $v_y$ for the case of a boundary in the $x$ direction). As, the domain was set up with $j = 0$ corresponding to the bottom wall (but really the ghost cells), then this involved:
		\begin{itemize}
			\item Mirroring flow state from $j = 2$ (real cell closest to wall) to $j = 1$ (first ghost cell);
			\item Mirroring flow state from $j = 3$ (real cell one in from the wall) to $j = 0$ (second ghost cell);
		\end{itemize}
		This is implemented in the sections of code shown in Listing \ref{wall}.
		\lstinputlisting[firstline=103,lastline=108,caption = {Wall Boundary Condition}, label={wall}]{../main.cxx}
		The \texttt{mirror\_flow\_along\_y} function is very simple and can be found in Listing \ref{flow}.
		
		\subsubsection*{Far field Boundary Conditions}
		For the far field boundary conditions, the ghost cells on the top, left and right boundaries are set at free stream conditions at the start. As these are not modified during the simulation, they remain at these conditions. A slight modification to Roe's solver at the boundary conditions was used to account for the boundary condition. According to Farhat's notes in Lecture 8, a far field boundary condition (adapted to the case with the far-field on the left) can be implemented in a Flux-Splitting Scheme as:
		\[
		\mathcal{F}_{j\infty} = A^+(W_j) W_\infty + A^- (W_j) W_j
		\]
		instead of 
			\[
			\mathcal{F}_{j\infty} = A^+(W_\infty) W_\infty + A^- (W_j) W_j
			\]
		Qualitatively, this just suggests that the upwinding and wave speeds is determined entirely by $W_j$ rather than a combination of $W_j$ and $W_\infty$. Currently, Roe's method is implemented using the method described below which is taken directly from Homework 3:
		\begin{IEEEeqnarray*}{rCll}
			W \jeq W_L &\qquad x/t < \lambda_3 < \lambda_2 < \lambda_1\\
			W \jeq W_L + l_3(W_R - W_L)r_3 &\qquad  \lambda_3 < x/t < \lambda_2 < \lambda_1\\
			W \jeq W_R - l_1(W_R - W_L)r_1&\qquad  \lambda_3 < \lambda_2 < x/t < \lambda_1\\
			W \jeq W_R &\qquad  \lambda_3 < \lambda_2 < \lambda_1 < x/t\\
		\end{IEEEeqnarray*} 
		
		Essentially, with the far-field boundary conditions, we need to determine which characteristics are appropriate to convect from the far field, but based only on the wave speeds from inside the domain. Hence, we should replace the if condition on the right with $\lambda_i$ calculated from $W_j$ rather than Roe's averages $W_{RL}$. The wave speeds and eigenvectors $\lambda_i, r_i, l_i$ used for the actual computation are still generated from Roe's averages in the spirit of Roe's scheme. While this is not entirely physical it still yields relatively good results. This was implemented in the code in Listing \ref{roe} in an if condition that sets the $u$ (velocity) and $c$ (sound speed) used for deciding which version of the flux to compute based on a boundary condition flag:
		\begin{itemize}
			\item \texttt{L} for a left boundary condition so $u, c$ are calculated from inside the domain on the right;
			\item \texttt{R} for a right boundary condition so $u, c$ are calculated from inside the domain on the left;
			\item \texttt{N} for no boundary condition so $u, c$ are calculated from the Roe averaged states.
		\end{itemize}
		\lstinputlisting[numbers=left,caption={Roe's Solver},label={roe}]{../roe.cxx}
		\subsubsection*{Roe's Solver applied to uniform cartesian 2D grids}
		Method \#2 from the notes was used. This involves decomposing the velocity into a normal and tangential component. As the grid was uniform and cartesian, this was largely trivial. In the case of an interface with flow in the $x$ direction:
		\[v_n = v_x \quad ; \quad v_t = v_y\]
		In the case of an interface with flow in the $y$ direction:
		\[v_n = v_y \quad ; \quad v_t = v_x\]
		By default, Roe's solver was set up to use $v_x$ as the normal velocity so that little modification as required for interfaces in the $x$ direction. For the interfaces with flow in the $y$ direction, this was implemented by switching $x$ and $y$ velocities before input into Roe's calculator, and then switching the calculated $x$ and $y$ momentum fluxes. Hence, in the frame of reference of the Roe solver, $x$ is the normal direction and $y$ is the tangential direction. The mass, $x$ (normal) momentum and energy fluxes were calculated as usual. The tangential $y$ momentum is defined as $\mathcal{F} = \rho_\text{RIEMANN} v_{x\text{RIEMANN}} v_y$ where $\rho_\text{RIEMANN},  v_{x\text{RIEMANN}} $ can be inferred from the mass flux from the Riemann solver. However, it is not clear from the notes where $v_y$ should be taken from the left, right or as a Roe average. In the spirit of upwinding for stability, $v_y$ was taken from the left if mass flux was positive (going to the right), and $v_y$ was taken from the right if mass flux was negative (going to the left). This is shown previously in lines 86--99 of Listing \ref{roe}.
		\subsubsection*{Monitoring Convergence using a Residual}
		For the steady solution $\partial W/ \partial t = 0$. One way of monitoring the solution is to see how close each component of $\partial W/ \partial t$ is to zero. However, each component of $W$ has different units, so it makes more sense to non-dimensionalise it. In this spirit, a residual for each cell was defined by dividing the change in conserved quantity at a particular time step by the original value of the conserved quantity:
		\[ e = \text{max}_i \left(\frac{\frac{\partial W_i}{\partial t} \Delta t}{W_i}\right)\]
		where $\frac{\partial W_i}{\partial t} $ was the estimate based on the finite volume approximation of the sum of the fluxes:
		\[\frac{\partial W_i}{\partial t} \approx \frac{1}{V}\sum \mathcal{F}\]
		This was implemented as part of the \texttt{update\_in\_time} method in the \texttt{cell} class. An excerpt of the code is shown in Listing \ref{updateintime}.
		\lstinputlisting[firstline=34,lastline=60,label={updateintime},caption={Time Integration}]{../cell.cxx}
		The average of $e$ across all cells was then taken as a measure of the convergence of the solution at a particular time step. A first, the maximum of $e$ across all cells was taken, but this was found to misrepresent the convergence of the solution as there were some localised instabilities in the flow field. An example of this instability is shown in Figure \ref{instability}.
		\begin{figure}[hbpt!]
			\centering
			\caption{Error at something}
			\label{instability}
		\end{figure}
		The residual was written to a text file at regular time steps for post analysis.
	\subsection*{2. Level of Discretisation}
	In order to determine an appropriate level of discretisation, the solution was run to steady state for a variety of grid refinements. The arithmetic mean of the error over the entire domain was computed by comparing each solution to a ``reference solution'' which was simply a solution at a suitably higher level of discretistation. An appropriate level of discretisation was chosen as one where further refinement would yield minimal decreases in error, and computational expense was not prohibitive. In order to compare the error at different discretisations, linear interpolation between data at cell centers was used. This was implemented in a post processing routine in MATLAB shown in Listing \ref{postM}. 
	\lstinputlisting[language=MATLAB,caption={Post processing to compare error}, label={postM}]{../post_N/post.m}
	\begin{figure}
		\centering
		\includegraphics[scale=0.8]{../post_N/error.png}
		\caption{Mean error in domain compared to reference solution}
		\label{Nerror}
	\end{figure}
	The results of running several simulations at different values of $N$ and comparing the error is shown in Figure \ref{Nerror}. In this case, $N = 150$ was deemed to give sufficient accuracy with an average error of 0.002 and taking only 30 seconds to run on a laptop. From this point forwards in the report, all simulations were run on a $150 \times 150$ domain.
	\clearpage
	\subsection*{3. Time Stepping}
	The time step that was fed into the \texttt{update\_in\_time} function was calculated differently for different time stepping.
	\paragraph{Part a) Global Time Stepping}Under global time stepping, the maximum wave speed in the domain was computed as $\lambda_\text{max} = \text{max}_D(|v_x| + c, |v_y| + c)$ and the time step was computed as $\Delta t = \text{CFL} \Delta x_\text{min} / \lambda_\text{max} $ where $D$ is the domain representing all cells. Under this scheme, all cells were fed the one $\Delta t$ per time step. Note that the maximum wave speed was calculated in the previous time step during the time integration loop.
	
	\paragraph{Part b) Local Time Stepping} Under local time stepping, the maximum wave speed in each cell was computed as $\lambda_i = \text{max}(|v_x| + c, |v_y| + c)$ and the time step was computed as $\Delta t_i = \text{CFL} \Delta x_{i} / \lambda_\text{max} $. Under this scheme, each cell was fed its own unique $\lambda_i$. In a true local time stepping scheme, each cell would also have a unique $\Delta x_{i}$ but the grid was uniform in this case so this was not required. An excerpt of \texttt{main.cxx} is shown in Listing \ref{dt} showing how $\Delta t$ is calculated for each method based on a \texttt{timesteppinglocal} flag set at the start of the routine.
	\lstinputlisting[firstline=159,lastline=170,label={dt},caption={Time stepping}]{../main.cxx}
	
	\paragraph{Part c) Comparison} In order to compare how quickly the two methods converged, the average residual was plotted vs. number of iterations for both methods holding CFL constant at 0.7. The results are shown in Figure \ref{residcompare}.
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.8]{../resid.png}
		\caption{Comparison of Residual Convergence under different time stepping}
		\label{residcompare}
	\end{figure}
	For the given grid discretisation and CFL, the local time stepping appears superior in several ways:
	\begin{itemize}
		\item It achieves a marginally higher rate of convergence as the slope of the curve in Figure \ref{residcompare} is slightly steeper;
		\item It begins converging and finishes converging at slightly lower number of iterations.
	\end{itemize}
	As expected, local time stepping yields some improvement over global time stepping as it can take advantage of the difference in maximum characteristic wave speed across the domain. However, the grid is uniform in this case, which means there is no advantage with regards to larger cells being able to have larger time steps. Hence, it is likely that local time stepping is significantly more beneficial for non-uniform grids. As local time stepping was marginally faster it is used for the remained of this report.
	\subsection*{4.a Results -- First Order Reconstruction}
	All simulations were run on a $150 \times 150$ grid using local time stepping using CFL $ = 0.7$. The simulation was run with local time stepping until the residual appeared to approach an asymptotic limit as shown in Figure \ref{residfirstorder}. The results are then shown in Figures \ref{pfirstorder}--\ref{Mfirstorder}.
	\begin{figure}
		\centering
		\includegraphics[scale=0.8]{../resid_firstorder}
		\caption{Residual for first order simulation}
		\label{residfirstorder}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{../pressure_firstorder}
		\caption{Pressure over the domain}
		\label{pfirstorder}
	\end{figure}
	\begin{figure}
			\centering
		\includegraphics[scale=0.4]{../density_firstorder}
			\caption{Density over the domain}
			\label{rhofirstorder}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.4]{../mach_firstorder}
		\caption{Mach number over the domain}
		\label{Mfirstorder}
	\end{figure}
	\subsection*{5.a Comparison to Analytical Results First Order}
	There exists an analytical solution to the oblique shock necessary to deflect the flow by $10^\circ$. This solution was found using the Virginia Tech Compressible Flow Calculator and for $M_1 = 2.0$ is:
	\[\frac{p_2}{p_1} = 1.7065786 \quad ; \quad M_2 = 1.6405222, \quad ; \quad \frac{\rho_2}{\rho_1} = 1.4584256\quad; \quad \beta = 39.3139^\circ\]
	The angle of the shock was found by locating the position of the shock at the end of the domain. Figure \ref{pressure_endshock} shows a plot of the pressure along the right side of the domain where we can estimate the shock location as approximately 1.748m from the bottom boundary. This implies an angle of shock of $\tan^{-1}(1.736/3) = 30.05$ which is 40.05$^\circ$ with reference to the original flow direction. The shock property ratios from the solution was:
	\[\frac{p_2}{p_1} = 1.70646\quad ; \quad M_2 = 1.6395, \quad ;\quad \frac{\rho_2}{\rho_1} = 1.4583 \quad \beta = 40.05^\circ\]
	
	This represents a percentage error of:
	\[e_p = \quad ; \quad e_{M_2} =-0.062\% ; \quad e_\rho = -0.0086\%\quad  \quad ; \quad e_\beta = 1.87\%\]
		\begin{figure}
			\centering
        	\includegraphics[scale=0.6]{../pressure_endshock}
			\caption{Pressure plot at right boundary}
			\label{pressure_endshock}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[scale=0.6]{../rho_endshock}
			\caption{Density plot at right boundary}
			\label{density_endshock}
		\end{figure}
	These relatively low errors suggest a relatively accurate solution
	\clearpage
	\subsection*{4.b -- Second Order Reconstruction, First Order Time Integration}
	All solutions in this section were run on a $150 \times 150$ grid with CFL $ = 0.4$. The second order reconstruction implemented in Homework 6 using Van leer's limiter was used to do a second order simulation with first order forward euler time integration (second order integration in section 4c). The ``second order method'' was slightly more complicated and incorporated the Van Leer's limiter as defined on slide 34--36 of Farhat's notes ``AA214B: The Finite Volume Method''. For the cell arrangement depicted in the comments of Listing \ref{interpolate} where we are reconstructing some flow variable on the right interface (denoted by *) of cell $i-1$:
	\begin{enumerate}
		\item Compute $r = \frac{f_i - f_{i-1}}{f_{i-1} - f_{i-2} + \epsilon}$ where $\epsilon = 10^{-12}$ was chosen to prevent dividing by zero
		\item If $r > 0$ keep the value of $r$, if it is negative set it to $r = 0$.
		\item Compute Van Leer's limiter function $\phi = \frac{2r}{1+r}$;
		\item Calculate the interface value as $f_{i-1/2} = f_{i-1} + \frac{1}{2}\phi(f_{i-1} - f_{i-2})$.
	\end{enumerate}
	This method is shown in \texttt{inerpolateSecondOrderUpwind} in LIsting \ref{interpolate}. The method is easily reversed for calculating left interface values. In this current implementation, the primitive variables $\rho, p, u$ were reconstructed (see the \texttt{interpolateL} and \texttt{interpolateR} functions in \texttt{cell.cxx} in Listing \ref{cell} in the appendix), and the remaining thermodynamic variables were updated using the gas model class. Special care was taken when reconstructing in the $x$ and $y$ directions to take the adjacent cells in the correct direction. This is shown in the function \texttt{cell\_threeD::interpolate} in Listing \ref{cell}. The primitive variables $\rho, \vec{v}, p$ were reconstructed and then the conserved states was calculated from the primitive variables. 
			
	\lstinputlisting[caption={Reconstruction Methods},label={interpolate}]{../interpolate.cxx} 
	The simulation was run with local time stepping until the residual appeared to approach an asymptotic limit as shown in Figure \ref{residsecondorder}. In the second order case with limiter, the residual was not able to converge to a low value. Instead, it was dominated by oscillations which can be seen close to the right boundary in Figure \ref{psecondorder}. It appears that the second order method may have introduced some oscillations as shown in Figure \ref{pressure_2endshock} that the limiter has not been able to remove. Additionally, we may be limited by the first order time integration method. In this sense, the solution has not reached a true steady state as it is still fluctuating, but it has reached some sort of periodic steady state. Whilst not optimal, this steady state can still be analysed if we ignore the spurious oscillations close to the shock. The location and flow properties of the shock are still valid, and the sharpness of the shock is now better captured. It is likely that the spurious oscillations (which are located close to the boundary) are caused partially by the shock not being aligned with the grid. A possible recommendation is to introduce non-uniform grids to account for this. The results for the second order method are then shown in Figures \ref{psecondorder}--\ref{Msecondorder}.
	\begin{figure}
		\centering
		\includegraphics[scale=0.6]{../resid_secondorder}
		\caption{Pressure over the domain}
		\label{residsecondorder}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.5]{../pressure_secondorder}
		\caption{Pressure over the domain}
		\label{psecondorder}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.5]{../density_secondorder}
		\caption{Density over the domain}
		\label{rhosecondorder}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.5]{../mach_secondorder}
		\caption{Mach number over the domain}
		\label{Msecondorder}
	\end{figure}
	\subsection*{5.b Comparison to Analytical Results}
	There exists an analytical solution to the oblique shock necessary to deflect the flow by $10^\circ$. This solution was found using the Virginia Tech Compressible Flow Calculator and for $M_1 = 2.0$ is:
	\[\frac{p_2}{p_1} = 1.7065786 \quad ; \quad M_2 = 1.6405222, \quad ; \quad \frac{\rho_2}{\rho_1} = 1.4584256\quad; \quad \beta = 39.3139^\circ\]
	The angle of the shock was found by locating the position of the shock at the end of the domain. Figure \ref{pressure_endshock} shows a plot of the pressure along the right side of the domain where we can estimate the shock location as approximately 1.748m from the bottom domain. This implies an angle of shock of $\tan^{-1}(1.726/3) = 29.9$ which is 39.9$^\circ$ with reference to the original domain. The shock property ratios from the solution was:
\[\frac{p_2}{p_1} = 1.7068\quad ; \quad M_2 = 1.6405, \quad ;\quad \frac{\rho_2}{\rho_1} = 1.4586 \quad \beta = 39.9^\circ\]
	This represents a percentage error of:
	\[e_p = 0.013\%\quad ; \quad e_\rho = 0.012\%\quad ; \quad e_{M_2} =-0.001\% , \quad ; \quad e_\beta = 1.5\%\]
	\begin{figure}
		\centering
		\includegraphics[scale=0.6]{../pressure_2endshock}
		\caption{Pressure plot at right boundary}
		\label{pressure_2endshock}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[scale=0.6]{../rho_2endshock}
		\caption{Density plot at right boundary}
		\label{density_2endshock}
	\end{figure}
	These relatively low errors suggest a relatively accurate solution. Note that the second order method did not really improve on the estimate of the correct flow properties behind the shock but it is able to capture the sharpness of the shock much more effectively. However, due to limitations the first order time integration, some oscillations have been introduced as seen in figures \ref{pressure_2endshock} \& \ref{density_2endshock}.
	\clearpage
	
		\subsection*{4.c -- Second Order Reconstruction, Second Order Time Integration}
		All solutions in this section were run on a $150 \times 150$ grid with CFL $ = 0.4$. The method in in 4.b) was augmented with a second order time integration method. This was implemented using a separate routine \texttt{main2.cxx} which is shown in listing \ref{main2}. The second order scheme that was used involved taking a half time step to estimate a new flow state, and then re-computing the flux at this half time step to update the original flow. Mathematically speaking:
		\ieq
		W_i^{n+1/2} \jeq W_i^n + \frac{1}{2}\frac{\Delta t}{\Delta x} (\mathcal{F}_{i+1/2}(W^n) - \mathcal{F}_{i-1/2}(W^n))\\
		W_i^{n+1} \jeq W_i^n + \frac{\Delta t}{\Delta x} (\mathcal{F}_{i+1/2}(W^{n+1/2}) - \mathcal{F}_{i-1/2}(W^{n+1/2}))\\
		\ieeq
		\lstinputlisting[caption={Main Routine for second order interpolation},label={main2}]{../main2.cxx} 
		The simulation was run with local time stepping until the residual appeared to approach some kind of asymptotic limit as shown in Figure \ref{residsecond2order}. In the second order case with limiter and second order time integration, the residual was not able to converge to a low value like in 4b). However, with the second order time integration we can see that the residual has been reduced by at least 2 orders of magnitude compared to Figure \ref{residsecondorder}. Again the residual is caused by oscillations which can be seen in a plot of the residual over the flow domain in Figure \ref{residsecond2order}. It appears that the second order spacial reconstruction may have introduced some oscillations as shown in Figure \ref{pressure_22endshock}. In this sense, the solution has not reached a true steady state as it is still fluctuating, but it has reached some sort of periodic steady state. Whilst not optimal, this steady state can still be analysed if we ignore the spurious oscillations close to the shock. Notably though, the spurious oscillations are much smaller in magnitude with second order time integration compared to first order time integration which is discussed later.
		\begin{figure}
			\centering
			\includegraphics[scale=0.6]{../resid_secondorder2}
			\caption{Residual vs time step}
			\label{residsecond2order}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[scale=0.4]{../resid_second2order}
			\caption{Residual over the domain}
			\label{resid_second2order}
		\end{figure}
		
		\begin{figure}
			\centering
			\includegraphics[scale=0.5]{../pressure_second2order}
			\caption{Pressure over the domain}
			\label{psecond2order}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[scale=0.5]{../density_second2order}
			\caption{Density over the domain}
			\label{rhosecond2order}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[scale=0.5]{../mach_second2order}
			\caption{Mach number over the domain}
			\label{Msecond2order}
		\end{figure}
		\subsection*{5.c Comparison to Analytical Results}
		There exists an analytical solution to the oblique shock necessary to deflect the flow by $10^\circ$. This solution was found using the Virginia Tech Compressible Flow Calculator and for $M_1 = 2.0$ is:
		\[\frac{p_2}{p_1} = 1.7065786 \quad ; \quad M_2 = 1.6405222, \quad ; \quad \frac{\rho_2}{\rho_1} = 1.4584256\quad; \quad \beta = 39.3139^\circ\]
		The angle of the shock was found by locating the position of the shock at the end of the domain. Figure \ref{pressure_22endshock} shows a plot of the pressure along the right side of the domain where we can estimate the shock location as approximately 1.733m from the bottom domain. This implies an angle of shock of $\tan^{-1}(1.733/3) = 30.01$ which is 40.01$^\circ$ with reference to the original domain. The shock property ratios from the solution was:
		\[\frac{p_2}{p_1} = 1.7065\quad ; \quad M_2 = 1.6405, \quad ;\quad \frac{\rho_2}{\rho_1} = 1.45833 \quad \beta = 40.01^\circ\]
		This represents a percentage error of:
		\[e_p = -0.0046\%\quad ; \quad e_\rho = -0.0063\%\quad ; \quad e_{M_2} =-0.001\% , \quad ; \quad e_\beta = 1.8\%\]
		\begin{figure}
			\centering
			\includegraphics[scale=0.6]{../pressure_22endshock}
			\caption{Pressure plot at right boundary}
			\label{pressure_22endshock}
		\end{figure}
		\begin{figure}
			\centering
			\includegraphics[scale=0.6]{../rho_22endshock}
			\caption{Density plot at right boundary}
			\label{density_22endshock}
		\end{figure}
		These relatively low errors suggest a relatively accurate solution. Note that it is not easy to conclude if this fully second order method is more accurate than the other methods at capturing the flow state before and after the shock. However, it is clearly superior to all methods presented previously in terms of its ability to capture the sharpness of the shock whilst minimising spurious oscillations as shown in Figures \ref{pressure_22endshock} \& \ref{density_22endshock}.
		\clearpage
	\subsection*{Appendix -- Auxiliary Code Listings}
	\lstinputlisting[label={flow},caption={\texttt{flow\_threeD.cxx}}]{../flow_threeD.cxx}
	\lstinputlisting[label={thermo},caption={\texttt{thermo.cxx}}]{../thermo.cxx}
	\lstinputlisting[label={matrix},caption={\texttt{matrix.cxx}}]{../matrix.cxx}
	\lstinputlisting[label={cell},caption={\texttt{cell.cxx}}]{../cell.cxx}
\end{document}