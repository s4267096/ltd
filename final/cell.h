#ifndef CELL
#define CELL
#include <iostream>
#include "geom.h"
using namespace std;
using interpFunc = double (*)(double, double, double);
class cell_threeD{
	public:
		flow_props flow;//flow properties at center
		//reconstructed flow interfaces
		flow_props interfR_x;
		flow_props interfL_x;
		flow_props interfR_y;
		flow_props interfL_y;
		flow_props interfR_z;
		flow_props interfL_z;
		//pointers to relevant luxes
		flux* fluxL_x;
		flux* fluxR_x;
		flux* fluxL_y;
		flux* fluxR_y;
		flux* fluxL_z;
		flux* fluxR_z;
		//geometric information about the cell
		vec pos;
		vec area;
		vec L;
		double max_resid;//residual to measure steady convergence
		double get_volume();
		cell_threeD();		
		cell_threeD(vec, vec);
		//generic second order reconstruction function accepting an interpolation function
		void interpolate(flow_props&, interpFunc, cell_threeD*, cell_threeD*);
		void update_in_time(double);
		cell_threeD update_in_time2(double);//returns a copy of the cell for second order RK
	private:
		void get_areas();
};
ostream& operator<<(ostream& info,const cell_threeD&);
#endif