#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "post.h"
#define INDX(x,y,n_x) (((n_x)*(x) + (y)))
void write_to_vtk(int n_x, int n_y, int n_z, double dx, double dy, double dz, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	int n_points = (n_x + 1)*(n_y + 1)*(n_z +1);
	m_file<<"POINTS " << n_points << " float\n";
	for(int i = 0; i!=n_x+1; i++){
		for(int j = 0; j!=n_y+1; j++){
			for(int k = 0; k!=n_z+1; k++){
				m_file<<setw(8)<<i*dx;
				m_file<<" ";
				m_file<<setw(8)<<j*dy;
				m_file<<" ";
				m_file<<setw(8)<<k*dz;
				m_file<<"\n";
			}
		}
	}
	int n_zy_one = (n_z + 1)*(n_y+1);
	int n_cells = n_x*n_y*n_z;
	m_file << "CELLS " << n_cells << " " << 9*n_cells << "\n";
	for(int i = 0; i!=n_x; i++){
		for(int j = 0; j!=n_y; j++){
			for(int k = 0; k!=n_z; k++){
				m_file << "8 " << k +j*(n_z+1) + i*n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 + n_zy_one << "\n";
			}
		}
	}
	m_file<<"CELL_TYPES "<< n_cells << "\n";
	for(int i = 0; i!=n_cells; i++){
		m_file <<"12\n";
	}
}
void write_cell_data(cell_threeD* cells, const int n_cells, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out | ios::app);
		m_file<<"CELL_DATA "<< n_cells <<"\n";
		m_file<<"SCALARS pressure float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_p<<"\n";
		m_file<<"SCALARS Temperature float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_T<<"\n";
		m_file<<"SCALARS rho float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_rho<<"\n";
		m_file<<"VECTORS vel float\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_vel.x<<" "<<(cells+i)->flow.m_vel.y<<" "<<(cells+i)->flow.m_vel.z<<"\n";
		m_file<<"SCALARS M float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) {
			double M  = (cells+i)->flow.m_vel.mag()/(cells+i)->flow.gm->sound_speed((cells+i)->flow);
			m_file<<M<<"\n";
		}
		m_file<<"SCALARS residual float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->max_resid<<"\n";

}
void write_pressure_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].flow.m_p<<" ";		
			}
			m_file << "\n";
		} 
}
void write_pos_x_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].pos.x<<" ";		
			}
			m_file << "\n";
		} 
}
void write_pos_y_for_matlab(cell_threeD* cells, const int n_cells_x, const int n_cells_y, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out);
		for(int i = 0; i!=n_cells_x; i++){
			for(int j = 0; j!=n_cells_y; j++){
				m_file<<cells[INDX(i,j,n_cells_x)].pos.y<<" ";		
			}
			m_file << "\n";
		} 
}
