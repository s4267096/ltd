# README #

A second-order finite-volume scheme computational fluid dynamics (CFD) code using the embedded boundary method.
Uses a structured mesh (which represents the fluid domain) into which aerodynamic obstacles are embedded. 
Capable of simulating, linear or quadratic surfaces as well as 4-digit NACA series airfoils.
### What is this repository for? ###

* Designed for rapid development of new algorithms for enforcing the boundary condition under the FIVER approach


### Who do I talk to? ###

* Jonathan Ho <jbho.at.stanford.edu>

Test equation

<img src="https://render.githubusercontent.com/render/math?math=e^{i \pi} = -1">
