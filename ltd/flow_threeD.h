//some classes to represent 1D flow
#ifndef FLOW
#define FLOW
#include <iostream>
#include "geom.h"
using namespace std;
class gas_model;
class flow_props{
	public:
		flow_props();
		flow_props(double, double, vec, gas_model*);
		vec m_vel; 
		double m_rho, m_e, m_p, m_T;
		gas_model* gm;
		double get_tke();
		double get_total_energy();
		vec get_momentum();
		void write_to_file(string);
		// double get_omega();
};	
class flux{
		public:
		double mass;
		vec momentum;
		double total_energy;
		double tke;
		double omega;
		flux();
};
flow_props flow_props_from_rho_T(double, double, vec, gas_model*);
flow_props flow_props_from_rho_p(double, double, vec, gas_model*);
ostream& operator<<(ostream&, const flow_props&);
ostream& operator<<(ostream&, const flux&);
#endif