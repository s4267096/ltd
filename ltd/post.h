#ifndef POST
#define POST
#include <iostream>
#include "thermo.h"
#include "cell.h"
using namespace std;
// void write_cells_to_file(cell_oneD*,const int, string);
void write_to_vtk(int, int, int, double, double, double, string);
void write_cell_data(cell_threeD*, const int, string);
#endif