#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "post.h"
void write_to_vtk(int n_x, int n_y, int n_z, double dx, double dy, double dz){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	int n_points = (n_x + 1)*(n_y + 1)*(n_z +1);
	m_file<<"POINTS " << n_points << "float\n";
	for(int i = 0; i!=n_x+1; i++){
		for(int j = 0; j!=n_y+1; j++){
			for(int k = 0; k!=n_z+1; k++){
				m_file<<setw(8)<<i*dx;
				m_file<<" ";
				m_file<<setw(8)<<j*dy;
				m_file<<" ";
				m_file<<setw(8)<<k*dz;
				m_file<<"\n";
			}
		}
	}
}