#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "thermo.h"
#include "cell.h"
using namespace std;
cell_threeD::cell_threeD(vec L_in, vec pos_in)
	:L{vec(L_in.x, L_in.y, L_in.z)}, pos{vec(pos_in.x, pos_in.y, pos_in.z)}
{
	get_areas();
}
cell_threeD::cell_threeD()
	:L{vec(0,0,0)}
{
}
ostream& operator<<(ostream& info, const cell_threeD& c){
	info << "Cell at: " << c.pos;
	return info;
}
double cell_threeD::get_volume(){
	return L.x*L.y*L.z;
}
void cell_threeD::interpolate(flow_props &interf, interpFunc f, cell_threeD* c_1, cell_threeD* c_2){
	interf.m_rho = f(c_2->flow.m_rho, flow.m_rho, c_1->flow.m_rho);
	interf.m_e = f(c_2->flow.m_e, flow.m_e, c_1->flow.m_e);
	interf.m_vel.x = f(c_2->flow.m_vel.x, flow.m_vel.x, c_1->flow.m_vel.x);
	interf.m_vel.y = f(c_2->flow.m_vel.y, flow.m_vel.y, c_1->flow.m_vel.y);
	interf.m_vel.z = f(c_2->flow.m_vel.z, flow.m_vel.z, c_1->flow.m_vel.z);
	flow.gm->update_from_rho_e(interf);
}
void cell_threeD::update_in_time(double dt){
	double rhodash = 1.0/get_volume()*(fluxL_x->mass*area.x - fluxR_x->mass*area.x
					  +fluxL_y->mass*area.y - fluxR_y->mass*area.y
					  +fluxL_z->mass*area.z - fluxR_z->mass*area.z);
	double momentumdash_x = 1.0/get_volume()*(fluxL_x->momentum.x*area.x - fluxR_x->momentum.x*area.x
					  +fluxL_y->momentum.x*area.y - fluxR_y->momentum.x*area.y
					  +fluxL_z->momentum.x*area.z - fluxR_z->momentum.x*area.z);
	double momentumdash_y = 1.0/get_volume()*(fluxL_x->momentum.y*area.x - fluxR_x->momentum.y*area.x
					  +fluxL_y->momentum.y*area.y - fluxR_y->momentum.y*area.y
					  +fluxL_z->momentum.y*area.z - fluxR_z->momentum.y*area.z);

	double momentumdash_z = 1.0/get_volume()*(fluxL_x->momentum.z*area.x - fluxR_x->momentum.z*area.x
					  +fluxL_y->momentum.z*area.y - fluxR_y->momentum.z*area.y
					  +fluxL_z->momentum.z*area.z - fluxR_z->momentum.z*area.z);
	double totalEdash = 1.0/get_volume()*(fluxL_x->total_energy*area.x - fluxR_x->total_energy*area.x
					  +fluxL_y->total_energy*area.y - fluxR_y->total_energy*area.y
					  +fluxL_z->total_energy*area.z - fluxR_z->total_energy*area.z);
	// cout << "rhodash: " << rhodash;
	// cout << " momentumdash_x: " << momentumdash_x << "\n";
	flow.m_rho += rhodash*dt;
	double oldenergy = flow.get_total_energy();
	flow.m_vel.x = (flow.get_momentum().x + momentumdash_x*dt)/flow.m_rho;
	flow.m_vel.y = (flow.get_momentum().y + momentumdash_y*dt)/flow.m_rho;
	flow.m_vel.z = (flow.get_momentum().z + momentumdash_z*dt)/flow.m_rho;
	flow.m_e = (oldenergy + totalEdash*dt- flow.get_tke())/flow.m_rho ;
	// cout << "rho_new: " << flow.m_rho;
	// cout << " vel_xnew: " << flow.m_vel.x;
	// cout << " totalEdash: " << totalEdash;
	// cout << " e_new: " << flow.m_e << "\n";
	flow.gm->update_from_rho_e(flow);
}
void cell_threeD::get_areas(){
	area.x = 0;//L.y*L.z;
	area.y = 0;//L.x*L.z;
	area.z = L.x*L.y;
}
