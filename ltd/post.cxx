#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "post.h"
// void write_cells_to_file(cell_oneD* cells, const int n_cells, string filename){
// 	ofstream m_file;
// 	m_fisle.open(filename, ios::out);
// 	m_file<<setw(12)<<"#x"<<setw(12)<<"rho"<<setw(12)<<"p"
// 		  <<setw(12)<<"u"<<setw(12)<<"T"
// 		  <<setw(12)<<"e"<<"\n";
// 	m_file.close();
// 	for (int i = 0; i != n_cells; i++){
// 		m_file.open(filename, ios::out | ios::app);
// 		m_file<<setw(12)<<i*0.01;
// 		m_file.close();
// 		(cells+i)->flow.write_to_file(filename);
// 	}
// }
void write_to_vtk(int n_x, int n_y, int n_z, double dx, double dy, double dz, string filename){
	ofstream m_file;
	m_file.open(filename, ios::out);
	m_file<<"# vtk DataFile Version 2.0\n";
	m_file<<"MyGrid \n"<<"ASCII \n"<<"DATASET UNSTRUCTURED_GRID \n";
	int n_points = (n_x + 1)*(n_y + 1)*(n_z +1);
	m_file<<"POINTS " << n_points << " float\n";
	for(int i = 0; i!=n_x+1; i++){
		for(int j = 0; j!=n_y+1; j++){
			for(int k = 0; k!=n_z+1; k++){
				m_file<<setw(8)<<i*dx;
				m_file<<" ";
				m_file<<setw(8)<<j*dy;
				m_file<<" ";
				m_file<<setw(8)<<k*dz;
				m_file<<"\n";
			}
		}
	}
	int n_zy_one = (n_z + 1)*(n_y+1);
	int n_cells = n_x*n_y*n_z;
	m_file << "CELLS " << n_cells << " " << 9*n_cells << "\n";
	for(int i = 0; i!=n_x; i++){
		for(int j = 0; j!=n_y; j++){
			for(int k = 0; k!=n_z; k++){
				m_file << "8 " << k +j*(n_z+1) + i*n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + 1 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 2 + n_zy_one << " "
						       << k +j*(n_z+1) + i*n_zy_one + n_z + 1 + n_zy_one << "\n";
			}
		}
	}
	m_file<<"CELL_TYPES "<< n_cells << "\n";
	for(int i = 0; i!=n_cells; i++){
		m_file <<"12\n";
	}
}
void write_cell_data(cell_threeD* cells, const int n_cells, string filename){
		ofstream m_file;
		m_file.open(filename, ios::out | ios::app);
		m_file<<"CELL_DATA "<< n_cells <<"\n";
		m_file<<"SCALARS pressure float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_p<<"\n";
		m_file<<"SCALARS Temperature float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_T<<"\n";
		m_file<<"SCALARS rho float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_rho<<"\n";
		m_file<<"SCALARS vel.x float 1\n";
		m_file<<"LOOKUP_TABLE default\n";
		for(int i = 0; i!=n_cells; i++) m_file<<(cells+i)->flow.m_vel.x<<"\n";
}