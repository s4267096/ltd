#ifndef CELL
#define CELL
#include <iostream>
#include "geom.h"
using namespace std;
using interpFunc = double (*)(double, double, double);
class cell_threeD{
	public:
		flow_props flow;
		flow_props interfR_x;
		flow_props interfL_x;
		flow_props interfR_y;
		flow_props interfL_y;
		flow_props interfR_z;
		flow_props interfL_z;
		flux* fluxL_x;
		flux* fluxR_x;
		flux* fluxL_y;
		flux* fluxR_y;
		flux* fluxL_z;
		flux* fluxR_z;
		vec pos;
		vec area;
		vec L;
		double get_volume();
		cell_threeD();		
		cell_threeD(vec, vec);
		void interpolate(flow_props&, interpFunc, cell_threeD*, cell_threeD*);
		void update_in_time(double);
	private:
		void get_areas();
};
ostream& operator<<(ostream& info,const cell_threeD&);
#endif