//a slightly not so silly program
#include <iostream>
#include <sstream>  
#include "thermo.h"
#include "cell.h"
#include "../lxd/interpolate.h"
#include "../lxd/ausmdv.h"
#include "post.h"
// #include "post.h"
using namespace std;
int main()
{
	constexpr int n_cells_x = 5;
	constexpr int n_cells_y = 5;
	constexpr int n_cells_z = 108;
	const vec L(0.05, 0.05, 0.05);
	const double min_L = min(min(L.x, L.y), L.z);	
	constexpr int n_tsteps = 200;
	double dt = 0.0000001;
	cell_threeD cells[n_cells_x][n_cells_y][n_cells_z];
	constexpr int nfluxes = 3*n_cells_x*n_cells_y*n_cells_z + n_cells_x*n_cells_y + n_cells_y*n_cells_z + n_cells_x*n_cells_z;
	flux fluxes[nfluxes];
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
			for(int k=0; k!=n_cells_z; k++){
				vec pos(L.x*(i+0.5), L.y*(j+0.5),L.z*(k+0.5));
				cells[i][j][k]=cell_threeD(L, pos);
			}
		}
	}
	//UP TO HERE
	//set up flux address linking
	int i_flux = 0;
	for(int i=2; i!=n_cells_x-1; i++){
		for(int j=2; j!=n_cells_y-2; j++){
			for(int k=2; k!=n_cells_z-2; k++){
				cells[i][j][k].fluxL_x=&fluxes[i_flux];
				cells[i-1][j][k].fluxR_x=&fluxes[i_flux];
				i_flux++;
			}
		}
	}
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-1; j++){
			for(int k=2; k!=n_cells_z-2; k++){
				cells[i][j][k].fluxL_y=&fluxes[i_flux];
				cells[i][j-1][k].fluxR_y=&fluxes[i_flux];
				i_flux++;
			}
		}
	}
	for(int i=2; i!=n_cells_x-2; i++){
		for(int j=2; j!=n_cells_y-2; j++){
			for(int k=2; k!=n_cells_z-1; k++){
				cells[i][j][k].fluxL_z=&fluxes[i_flux];
				cells[i][j][k-1].fluxR_z=&fluxes[i_flux];
				i_flux++;
			}
		}
	}
	//initial conditions
	double rho_L=1; vec vel_L(0.0, 0.0, 0.0); double p_L = 100000;
	double rho_R=0.125; vec vel_R(0.0, 0.0, 0.0); double p_R = 10000;
	gas_model air(1.4,0.02897,720);
	for(int i=0; i!=n_cells_x; i++){
		for(int j=0; j!=n_cells_y; j++){
			for(int k=0; k!=n_cells_z; k++){
				if (k < n_cells_z/2) {
					cells[i][j][k].flow=flow_props_from_rho_p(rho_L, p_L, vel_L, &air);
					;}
				else {cells[i][j][k].flow=flow_props_from_rho_p(rho_R, p_R, vel_R, &air);}
			}
		}
	}
	//write initial conditions to file
	//start updating in time
	double max_a=0;
	double t_elapsed=0;
	cell_threeD cells_no_ghost[n_cells_x-4][n_cells_y-4][n_cells_z-4];
	for(int i_t=0; i_t!=n_tsteps; ++i_t){
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				for(int k=2; k!=n_cells_z-2; k++){
					cells_no_ghost[i-2][j-2][k-2]=cells[i][j][k];
				}
			}
		}
		stringstream vtkfilename;
		vtkfilename << "post/t00"<<i_t<<".vtk";
		write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
		write_cell_data(&cells_no_ghost[0][0][0],(n_cells_x-4)*(n_cells_y-4)*(n_cells_z-4),vtkfilename.str());
		// write_to_vtk(n_cells_x, n_cells_y, n_cells_z, L.x, L.y, L.z, vtkfilename.str());
		// write_cell_data(&cells[0][0][0],(n_cells_x)*(n_cells_y)*(n_cells_z),vtkfilename.str());
		for(int i=1; i!=n_cells_x-1; i++){
			for(int j=1; j!=n_cells_y-1; j++){
				for(int k=1; k!=n_cells_z-1; k++){
					cells[i][j][k].interpolate(cells[i][j][k].interfR_x,&interpolateSecondOrderUpwind,&cells[i-1][j][k],&cells[i+1][j][k]);
					cells[i][j][k].interpolate(cells[i][j][k].interfL_x,&interpolateSecondOrderUpwind,&cells[i+1][j][k],&cells[i-1][j][k]);
					cells[i][j][k].interpolate(cells[i][j][k].interfR_y,&interpolateSecondOrderUpwind,&cells[i][j-1][k],&cells[i][j+1][k]);
					cells[i][j][k].interpolate(cells[i][j][k].interfL_y,&interpolateSecondOrderUpwind,&cells[i][j+1][k],&cells[i][j-1][k]);
					cells[i][j][k].interpolate(cells[i][j][k].interfR_z,&interpolateSecondOrderUpwind,&cells[i][j][k-1],&cells[i][j][k+1]);
					cells[i][j][k].interpolate(cells[i][j][k].interfL_z,&interpolateSecondOrderUpwind,&cells[i][j][k+1],&cells[i][j][k-1]);
				}
			}
		}
		int i_flux = 0;
		for(int i=2; i!=n_cells_x-1; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				for(int k=2; k!=n_cells_z-2; k++){
					ausmdv(cells[i-1][j][k].interfR_x, cells[i][j][k].interfL_x, fluxes[i_flux], &air);
					i_flux++;
				}
			}
		}
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-1; j++){
				for(int k=2; k!=n_cells_z-2; k++){
					cells[i][j-1][k].interfR_y.m_vel.switch_y_x();
					cells[i][j][k].interfL_y.m_vel.switch_y_x();
					ausmdv(cells[i][j-1][k].interfR_y, cells[i][j][k].interfL_y, fluxes[i_flux], &air);
					fluxes[i_flux].momentum.switch_y_x();
					//do i need to switch the velocities back?
					i_flux++;
				}
			}
		}
		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				for(int k=2; k!=n_cells_z-1; k++){
					cells[i][j][k-1].interfR_z.m_vel.switch_z_x();
					cells[i][j][k].interfL_z.m_vel.switch_z_x();
					ausmdv(cells[i][j][k-1].interfR_z, cells[i][j][k].interfL_z, fluxes[i_flux], &air);
					fluxes[i_flux].momentum.switch_z_x();
					i_flux++;
				}
			}
		}


		for(int i=2; i!=n_cells_x-2; i++){
			for(int j=2; j!=n_cells_y-2; j++){
				for(int k=2; k!=n_cells_z-2; k++){
					cells[i][j][k].update_in_time(dt);
					max_a=max(max_a, air.sound_speed(cells[i][j][k].flow));
				}
			}
		}
		
		t_elapsed += dt;
		dt=0.1*min_L/max_a;
		cout << "n: " << i_t <<  "; t: " << t_elapsed << "; dt: " << dt << "\n";
	}
	stringstream vtkfilename;
	vtkfilename << "post/t00"<<n_tsteps<<".vtk";
	write_to_vtk(n_cells_x-4, n_cells_y-4, n_cells_z-4, L.x, L.y, L.z, vtkfilename.str());
	write_cell_data(&cells_no_ghost[0][0][0],(n_cells_x-4)*(n_cells_y-4)*(n_cells_z-4),vtkfilename.str());
	// write_cells_to_file(cells,n_cells,"myfile.dat");
}