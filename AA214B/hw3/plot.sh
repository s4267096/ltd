gnuplot -e "unset key; set xlabel 'x (m)'; set ylabel 'Density (kg/m^3)'; set title 'Density'; plot 'result.dat' using 1:2 with lines ; set term png; set output 'density.png'; replot; set term x11"
gnuplot -e "unset key; set xlabel 'x (m)'; set ylabel 'Velocity (m/s)'; set title 'Velocity'; plot 'result.dat' using 1:4 with lines ; set term png; set output 'Velocity.png'; replot; set term x11"
gnuplot -e "unset key; set xlabel 'x (m)'; set ylabel 'Pressure (Pa)'; set title 'Pressure'; plot 'result.dat' using 1:3 with lines ; set term png; set output 'Pressure.png'; replot; set term x11"
