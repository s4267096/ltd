\documentclass[letterpaper,12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage[margin=0.75in]{geometry}
\usepackage{IEEEtrantools}
\usepackage{subcaption}
\usepackage{epstopdf}
\usepackage{listings}
\newcommand{\ieq}{\begin{IEEEeqnarray*}{rCl}}
	\newcommand{\ieeq}{\end{IEEEeqnarray*}}
\newcommand{\jeq}{&=&}
\newcommand{\degrees}{^\circ}
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
	basicstyle=\footnotesize\ttfamily,
	breaklines=true,
	language=C++,
	commentstyle=\color{mygreen},
	keywordstyle=\color{blue},
	stringstyle=\color{mymauve}
}
\title{AA214B - Homework 3}
\author{Jonathan Ho}
\date{February 21 2017}

\begin{document}
	\maketitle
	\subsection*{Question 1}
	\subsubsection*{Part 1}
	The flux at $x = 0$ can be calculated using the equation:
	\[A_{RL} W(0) = \frac{1}{2} A_{RL}(W_R + W_L) - \frac{1}{2}|A_{RL}|(W_R - W_L)\]
	
	$W_R, W_L, A_{RL}$ are readily computed from their definitions supplied in Problem 1. Computing $|A_{RL}|$ is less trivial. We need diagonalise the matrix and replace the eigenvalue diagonal with absolute values as follows. If the matrix can be diagonalised as: $A_{RL} = Q^{-1}\Lambda Q$ where $Q$ is composed of left eigenvector rows $l_i$ and $Q^{-1}$ is composed of right eigenvector columns $r_i$ such that:
	\[Q^{-1} = [r_1 \quad r_2 \quad r_3]\quad ; \quad Q = \left[
	\begin{array}{c}
	l_1\\
	l_2\\
	l_3
	\end{array}
	\right]\]
	We can evaluate $|A_{RL}|$ as 
	\ieq
	|A_{RL}| \jeq Q^{-1}|\Lambda| Q \\
	|A_{RL}| \jeq\left[
	\begin{array}{ccc}
		&&\\
		|\lambda_1|r_1, & |\lambda_2|r_2, & |\lambda_3|r_3  \\
		&&
	\end{array}
	\right]
	\left[
	\begin{array}{ccc}
		&l_1&\\
		&l_2 &  \\
		&l_3&
	\end{array}
	\right]\IEEEyesnumber\label{eqDecomp}
	\\
	\ieeq
	These left and right eigenvectors can be computed numerically using a program such as MATLAB, but it is also possibly to compute $Q$ and $Q^{-1}$ analytically in closed form. This symbolic manipulation was carried out using MATLAB to improve computational efficiency during run time. Note the substitution $h_{RL} = c_{RL}^2/(\gamma-1)+0.5v_{RL}^2$ and $M = v_{RL}/c_{RL}$ was made to simplify algebra. Note that all variables below refer to Roe--averaged states, $v= v_{RL},\: c = c_{RL}$. Normalising $Q^{-1}$ by the top element of each column gave:
	\[
	Q^{-1} = \left[
	\begin{array}{ccc}
	1 & 1 & 1\\
	v + c & v & v - c\\
	\frac{c^2}{\gamma - 1} + \frac{v(2c+v)}{2} & \frac{v^2}{2} & \frac{c^2}{\gamma - 1} - \frac{v(2c-v)}{2}
	\end{array}
	\right]
	\]
	
	\[
	Q = \left[
	\begin{array}{ccc}
	\frac{(\gamma - 1)M^2}{4} - \frac{M}{2} & \frac{1}{2c} - \frac{(\gamma - 1)v}{2c^2} & \frac{\gamma-1}{2c^2}\\
	1 - \frac{(\gamma - 1)M^2}{2} & \frac{(\gamma - 1)v}{c^2} & -\frac{\gamma-1}{c^2}\\
	\frac{(\gamma - 1)M^2}{4} + \frac{M}{2} & -\frac{1}{2c} - \frac{(\gamma - 1)v}{2c^2} & \frac{\gamma-1}{2c^2}
	\end{array}
	\right]
	\]
	with $\lambda_1 = v+ c,  \lambda_2= v, \lambda_3 = v- c$ chosen in descending order.
	
	The calculation of $W(x/t)$ needs to be broken down into the different domains. Note that $\Delta \xi_i = l_i(W_R - W_L)$ and this is exploited in the expressions below. It is possible to compute the solution using either $W_L$ or $W_R$ as reference, but the simpler choice was made where relevant:
	\begin{IEEEeqnarray*}{rCll}
		W \jeq W_L &\qquad x/t < \lambda_3 < \lambda_2 < \lambda_1\\
		W \jeq W_L + l_3(W_R - W_L)r_3 &\qquad  \lambda_3 < x/t < \lambda_2 < \lambda_1\\
		W \jeq W_R - l_1(W_R - W_L)r_1&\qquad  \lambda_3 < \lambda_2 < x/t < \lambda_1\\
		W \jeq W_R &\qquad  \lambda_3 < \lambda_2 < \lambda_1 < x/t\\
	\end{IEEEeqnarray*}
	
	\subsubsection*{Part 2 -- Subroutine to Implement Roe's Algorithm}
	
	C++ was used in this homework to utilise the author's existing code for 1-dimensional finite volume CFD code. To this end, the modules \texttt{thermo.h}, \texttt{matrix.h}, \texttt{flow\_oneD.h} were utilised and are included in the Appendix.
	
	\lstinputlisting{roe_slow_p1.cxx}
	
	
	\subsubsection*{Part 3 -- Main function to call Subroutine}
	\lstinputlisting{main_p1.cxx}
	
	\subsubsection*{Part 5 -- Computational Expense}
	
	\noindent The output from the main code in Part 3 is shown below:
	
	\lstinputlisting[caption={Output for Problem 1}]{output_p1}
	
	\noindent This indicates a computation time of 0.004586 ms for $A_{RL}W(0)$ and fluxes of 
	\begin{equation*} \rho v_x = 180.5 \text{ kg/s} \quad; \quad \rho v_x^2= 84237 \text{kg.m/s}^2\quad;\quad (E + p)v_x = 60.0 \text{MJ/s} \end{equation*}
	
	\subsubsection*{Part 4 -- Function for Computing $W(x/t)$}
	\lstinputlisting{roe_plot.cxx}
	The resulting file \texttt{Result.dat} was plotted using gnuplot using the script shown below.
	\lstinputlisting[language=bash]{plot.sh}
	
	This gives the solutions shown in Figure \ref{fig1}--\ref{fig3}.
	
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{density}
		\caption{Density at $t = 0.01$ s}
		\label{fig1}
	\end{figure}
	
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{Pressure}
		\caption{Pressure at $t = 0.01$ s}
		\label{fig2}
	\end{figure}
	
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{Velocity}
		\caption{Velocity at $t = 0.01$ s}
		\label{fig3}
	\end{figure}
	
	
	\clearpage
	\subsection*{Question 2}
	\subsubsection*{Part 1}
	Under the secant approximation we can write 
	\begin{IEEEeqnarray}{rCl}
		\mathcal{F}_x(W(0)) &\approx& \mathcal{F}_x(W_L) + A_{RL}(W(0) - W_L)\label{eq1}\\ 
		\mathcal{F}_x(W(0)) &\approx& \mathcal{F}_x(W_R) + A_{RL}(W(0) - W_R)\label{eq2}
	\end{IEEEeqnarray}
	If we add these two expressions and divide by 2 we obtain:
	\ieq
	\mathcal{F}_x(W(0)) \jeq \frac{1}{2}(\mathcal{F}_x(W_R) +\mathcal{F}_x(W_L)) + A_{RL}W(0) - \frac{1}{2} A_{RL}(W_R + W_L)
	\ieeq
	
	
	We then substitute the original expression we have for $A_{RL}W(0)$ and we are left with:
	
	\[\mathcal{F}_x(W(0)) = \frac{1}{2}(\mathcal{F}_x(W_R) +\mathcal{F}_x(W_L))- \frac{1}{2}|A_{RL}|(W_R - W_L)\]
	
	\subsubsection*{Part 2}
	We can alternately note that either that $A_{RL}W(0) = AW_L + A^-(W_R - W_L)$ or $A_{RL}W(0) = AW_R - A^+(W_R - W_L)$ into Equation \ref{eq1} \& \ref{eq2} and we arrive at the expressions:
	\begin{IEEEeqnarray}{rCl}
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_L) + A^-(W_R - W_L)\label{eq3}\\
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_R) - A^+(W_R - W_L)\label{eq4}
	\end{IEEEeqnarray}
	
	\subsubsection*{Part 3}
	The value of $v$ relative to $c$ determines which eigenvalues $\lambda_i$ are positive. 
	The simplest computation of the flux, i.e. the choice of whether to compute $A^-$ or $A^+$ using Equation \ref{eq3} or \ref{eq4}, depends on which eigenvalues are positive or negative.
	\begin{enumerate}
		\item If $0 < \lambda_3 < \lambda_2 < \lambda_1$, all are positive and $A^- = 0$;
		\item If $\lambda_3 < 0 < \lambda_2 < \lambda_1$, only $\lambda_3$ is negative so $A^-$ can be computed using $r_3$ and $l_3$ only;
		\item If $\lambda_3 < \lambda_2< 0  < \lambda_1$, only $\lambda_1$ is positive so $A^+$ can be computed using $r_1$ and $l_1$ only;
		\item If $\lambda_3 < \lambda_2 < \lambda_1 < 0$, all are negative and $A^+ = 0$
	\end{enumerate}
	Recalling Equation \ref{eqDecomp} and noting that only one of $|\lambda_i|\neq 0$, this allows us to simplify the computation of the flux as:
	\begin{IEEEeqnarray*}{rCll}
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_L) &\qquad 0 < \lambda_3 < \lambda_2 < \lambda_1\\
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_L) + \lambda_3l_3(W_R - W_L)r_3 &\qquad  \lambda_3 < 0 < \lambda_2 < \lambda_1\\
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_R) - \lambda_1l_1(W_R - W_L)r_1 &\qquad  \lambda_3 < \lambda_2 < 0 < \lambda_1\\
		\mathcal{F}_x(W(0)) \jeq \mathcal{F}_x(W_R) &\qquad  \lambda_3 < \lambda_2 < \lambda_1 < 0
	\end{IEEEeqnarray*}
	
	In the first and last case, the computation is trivial, being a single flux calculation. In either of the middle two cases we have the additional computation of:
	\begin{itemize}
		\item Dot product of $l_i(W_R - W_L)$;
		\item Scalar Multiplication by $\lambda_i$;
		\item Scaling of Vector by $\lambda_i l_i(W_R - W_L)$.
	\end{itemize}
	
	This is far fewer operations than the matrix multiplications required by the algorithm in Part 1.
	
	\subsubsection*{Part 4 -- Alternate Roe Subroutine}
	\lstinputlisting[language=C++]{roe_fast.cxx}
	
	\subsubsection*{Part 5 -- Upgraded Roe Subroutine from Question 1}
	\lstinputlisting[language=C++]{roe_slow.cxx}
	
	\subsubsection*{Part 6 -- Comparison of different algorithms}
	Both subroutines shown in Part 5 and Part 4 were implemented in the code below.
	
	\lstinputlisting{main.cxx}
	
	The output is shown below which shows the flux computation agrees and there is a speed up of 2.9x. The flux is similar, but not identical to Problem 1 which is unsurprising considering the secant approximation.
	\begin{equation*} \rho v_x = 180.5 \text{ kg/s} \quad; \quad \rho v_x^2= 85465.2 kg/s\text{kg.m/s}^2\quad;\quad (E + p)v_x = 60.75 \text{MJ/s} \end{equation*}
	
	\lstinputlisting[caption={Output for Problem 2}]{output_p2}
	
	A summary of the computational expense of the different algorithms investigated is presented in Table \ref{tab1}.
	
	
	\begin{table}[hbpt!]
		\centering
		\caption{Summary of Computational Performance of Different Algorithms}
		\begin{tabular}{l|ccc}
			\hline
			& \textbf{Problem 1} & \textbf{Problem 2.4} & \textbf{Problem 2.5}\\\hline
			Computational Time (ms) & 0.004586 & 0.003151 & 0.001202 \\
			Speed up over Problem 1 & 1x & 1.46x & 3.81x\\\hline
		\end{tabular}
		
		\label{tab1}
		
	\end{table}
	
	
	\subsection*{Appendix -- Auxiliary Code Listings}
	\lstinputlisting[caption={\texttt{flow\_oneD.cxx}}]{flow_oneD.cxx}
	\lstinputlisting[caption={\texttt{thermo.cxx}}]{thermo.cxx}
	\lstinputlisting[caption={\texttt{matrix.cxx}}]{matrix.cxx}
\end{document}