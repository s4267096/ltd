//Class definition for a gas model to perform some simple thermodynamic calculations
#include <iostream>
#include <cmath>
#include <fstream>
#include "thermo.h"
#include <iomanip>
using namespace std;
gas_model::gas_model(double gamma, double M)
	:m_gamma{gamma}, m_M{M}, m_R{R/M}
{m_c_v = m_R/(gamma - 1);}
void gas_model::update_from_rho_e(flow_props &f){
	f.m_T = f.m_e/m_c_v;
	f.m_p = m_R*f.m_rho*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_u * f.m_u + f.m_p/f.m_rho;
}
void gas_model::update_from_rho_T(flow_props &f){
	f.m_e = m_c_v*f.m_T;
	f.m_p = m_R*f.m_rho*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_u * f.m_u + f.m_p/f.m_rho;
}
void gas_model::update_from_rho_p(flow_props &f){
	f.m_T = f.m_p/f.m_rho/m_R;
	f.m_e = m_c_v*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_u * f.m_u + f.m_p/f.m_rho;
}
void gas_model::update_from_p_T(flow_props &f){
	f.m_rho=f.m_p/m_R/f.m_T;
	f.m_e=m_c_v*f.m_T;
	f.m_h = f.m_e + 0.5 * f.m_u * f.m_u + f.m_p/f.m_rho;
}
double gas_model::sound_speed(flow_props f){
	return sqrt(m_gamma*m_R*f.m_T);
}
