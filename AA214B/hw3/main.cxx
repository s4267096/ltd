#include <cstdio>
#include <ctime>
#include <iostream>
#include "flow_oneD.h"
#include "matrix.h"
#include "thermo.h"
int roe_fast(flow_props, flow_props, flux& face, gas_model*);
int roe_slow(flow_props, flow_props, flux& face, gas_model*);
int roe_plot(flow_props, flow_props, gas_model*);


using namespace std;
int main(){
	//Define Fluid State on Left and Right and initia flow object
	double p_L = 1e5; double rho_L = 1; double u_L = 100;
	double p_R = 10000; double rho_R = 0.125; double u_R = -50;
	gas_model air(1.4,0.02897);
	flow_props flow_L = flow_props_from_rho_p(rho_L, p_L, u_L, &air);
	flow_props flow_R = flow_props_from_rho_p(rho_R, p_R, u_R, &air);
	flux flux_at_zero;
	
	std::clock_t start;
    double standardDuration;
    start = std::clock();

    for(int i = 0; i != 1000; i++) roe_slow(flow_L, flow_R, flux_at_zero, &air);

    standardDuration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	cout << "---Standard Method----------\n";
    std::cout<<"Duration: "<< standardDuration <<"ms\n";
	cout << flux_at_zero;
	
	flux flux_at_zeroquickway;
    double quickDuration;
    start = std::clock();
	for(int i = 0; i != 1000; i++) roe_fast(flow_L, flow_R, flux_at_zeroquickway, &air);
    quickDuration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	cout << "---Quick Method----------\n";
    std::cout<<"Duration: "<< quickDuration << " ms\n";
	cout << flux_at_zeroquickway;
	cout << "Speed Up: " << standardDuration/quickDuration << "x \n";

	roe_plot(flow_L, flow_R, &air);
	return 1;
}