#ifndef THERMO
#define THERMO
#include <iostream>
using namespace std;
#include "flow_oneD.h"
constexpr double R = 8.314; //should find a better way to do this
class gas_model{
	public:
		double m_gamma, m_M, m_c_v;	double m_R;
		gas_model(double,double);
		void update_from_rho_e(flow_props&);
		void update_from_rho_T(flow_props&);
		void update_from_rho_p(flow_props&);
		void update_from_p_T(flow_props&);
		double sound_speed(flow_props);
};
#endif