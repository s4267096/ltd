#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "thermo.h"
#include "matrix.h"
#include "flow_oneD.h"
using namespace std;
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int roe_slow(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel){
    double g = gmodel->m_gamma;
    double gmin1 = g - 1;
    double sq_rho_R = sqrt(f_R.m_rho);
    double sq_rho_L = sqrt(f_L.m_rho);
    double u_RL = (sq_rho_R*f_R.m_u + sq_rho_L*f_L.m_u)/(sq_rho_R + sq_rho_L);
    double h_RL = (sq_rho_R*f_R.m_h + sq_rho_L*f_L.m_h)/(sq_rho_R + sq_rho_L);

    matrix3 A_RL;
    A_RL.A[0][0] = 0.0; 
    A_RL.A[0][1] = 1.0; 
    A_RL.A[0][2] = 0.0;

    A_RL.A[1][0] = (g-3.0)/2.0*u_RL*u_RL;
    A_RL.A[1][1] = (3.0 - g)*u_RL;
    A_RL.A[1][2] = gmin1;

    A_RL.A[2][0] = -u_RL*h_RL + 0.5*(g - 1) * u_RL * u_RL * u_RL;
    A_RL.A[2][1] = h_RL - (g - 1)*u_RL*u_RL;
    A_RL.A[2][2] = g * u_RL;


    vector3 W_L(f_L.m_rho, f_L.m_rho * f_L.m_u, f_L.get_total_energy());
    vector3 W_R(f_R.m_rho, f_R.m_rho * f_R.m_u, f_R.get_total_energy());
    vector3 deltaW = subtract(W_R, W_L);
    vector3 FluxVec;
    matrix3 fabsA_RL;
    double c = sqrt((gmin1)*(h_RL - 0.5*u_RL*u_RL));
    double M = u_RL/c;

    // standard method of computing fluxes
    double eig1 = fabs(u_RL + c);
    double eig2 = fabs(u_RL);
    double eig3 = fabs(u_RL - c);
    matrix3 Qinv_D;
    Qinv_D.A[0][0] = eig1; 
    Qinv_D.A[0][1] = eig2; 
    Qinv_D.A[0][2] = eig3;
    Qinv_D.A[1][0] = eig1*(u_RL + c);
    Qinv_D.A[1][1] = eig2*u_RL;
    Qinv_D.A[1][2] = eig3*(u_RL - c);
    Qinv_D.A[2][0] = eig1*(c*c/gmin1 + u_RL*(2*c + u_RL)/2.0);
    Qinv_D.A[2][1] = eig2*u_RL*u_RL/2.0;
    Qinv_D.A[2][2] = eig3*(c*c/gmin1 - u_RL*(2*c - u_RL)/2.0);

    matrix3 Q;
    Q.A[0][0] = (gmin1)*M*M/4 - 0.5*M; 
    Q.A[0][1] = (0.5 - 0.5*M*(gmin1))/c;
    Q.A[0][2] = gmin1/2.0/c/c;
    Q.A[1][0] = 1 - 0.5*gmin1*M*M;
    Q.A[1][1] = M/c*gmin1;
    Q.A[1][2] = -gmin1/c/c;
    Q.A[2][0] = gmin1*M*M/4 + 0.5*M;
    Q.A[2][1] = (-0.5 - 0.5*M*(gmin1))/c;
    Q.A[2][2] = gmin1/2.0/c/c;

    fabsA_RL = multiply(Qinv_D, Q);
    FluxVec = subtract( multiply(A_RL, add(W_R, W_L)) , multiply(fabsA_RL, deltaW));
    FluxVec = FluxVec.scalarMult(0.5);

    
    //Unpack fluxes into flux object

    face.mass = FluxVec.x[0];
    face.momentum = FluxVec.x[1];
    face.total_energy = FluxVec.x[2];
    return 1;
}   /* end of roe() */

/*--------------------------------------------------------------*/
