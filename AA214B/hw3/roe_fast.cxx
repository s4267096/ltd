#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "thermo.h"
#include "matrix.h"
#include "flow_oneD.h"
using namespace std;
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int roe_fast(flow_props f_L, flow_props f_R, flux& face, gas_model* gmodel){
    double g = gmodel->m_gamma;
    double gmin1 = g - 1;
    double sq_rho_R = sqrt(f_R.m_rho);
    double sq_rho_L = sqrt(f_L.m_rho);
    double u_RL = (sq_rho_R*f_R.m_u + sq_rho_L*f_L.m_u)/(sq_rho_R + sq_rho_L);
    double h_RL = (sq_rho_R*f_R.m_h + sq_rho_L*f_L.m_h)/(sq_rho_R + sq_rho_L);

    vector3 W_L(f_L.m_rho, f_L.m_rho * f_L.m_u, f_L.get_total_energy());
    vector3 W_R(f_R.m_rho, f_R.m_rho * f_R.m_u, f_R.get_total_energy());
    vector3 deltaW = subtract(W_R, W_L);
    vector3 FluxVec;
    double c = sqrt((gmin1)*(h_RL - 0.5*u_RL*u_RL));
    double M = u_RL/c;

    // quick method of computing fluxes
        if (u_RL > c){
            vector3 Flux_L(f_L.m_rho * f_L.m_u, 
               f_L.m_rho * f_L.m_u * f_L.m_u + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_u);
            FluxVec = Flux_L;
        }
        else if ( u_RL > 0.0){
            vector3 Flux_L(f_L.m_rho * f_L.m_u, 
               f_L.m_rho * f_L.m_u * f_L.m_u + f_L.m_p, 
               (f_L.get_total_energy() + f_L.m_p) * f_L.m_u);
            double eig3 = u_RL - c;
            vector3 l3((gmin1)*M*M/4 + 0.5*M,
                (-0.5 - 0.5*M*(gmin1))/c,
                gmin1/2.0/c/c);
            vector3 r3(1,
                u_RL - c,
                c*c/gmin1 - u_RL*(2*c - u_RL)/2.0);
            FluxVec = add( Flux_L , r3.scalarMult(eig3*dot(l3, deltaW)));
        }
        else if (u_RL > - c){
            vector3 Flux_R(f_R.m_rho * f_R.m_u, 
                f_R.m_rho * f_R.m_u * f_R.m_u + f_R.m_p, 
                (f_R.get_total_energy() + f_R.m_p) * f_R.m_u);
            double eig1 = u_RL + c;
            c = sqrt((gmin1)*(h_RL - 0.5*u_RL*u_RL));
            M = u_RL/c;
            vector3 l1((gmin1)*M*M/4 - 0.5*M,
                (0.5 - 0.5*M*(gmin1))/c,
                gmin1/2.0/c/c);
            vector3 r1(1,
                u_RL + c,
                c*c/gmin1 + u_RL*(2*c + u_RL)/2.0);
            FluxVec = subtract( Flux_R , r1.scalarMult(eig1*dot(l1, deltaW)));
        }
        else {
            vector3 Flux_R(f_R.m_rho * f_R.m_u, 
                f_R.m_rho * f_R.m_u * f_R.m_u + f_R.m_p, 
                (f_R.get_total_energy() + f_R.m_p) * f_R.m_u);
            FluxVec = Flux_R; 
        }

    //Unpack fluxes into flux object

    face.mass = FluxVec.x[0];
    face.momentum = FluxVec.x[1];
    face.total_energy = FluxVec.x[2];

   return 1;
}   /* end of roe() */

/*--------------------------------------------------------------*/
