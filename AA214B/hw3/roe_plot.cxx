#include <stdio.h>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "thermo.h"
#include "matrix.h"
#include "flow_oneD.h"
using namespace std;
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/

/** \brief Compute the fluxes across an interface. */
int roe_plot(flow_props f_L, flow_props f_R, gas_model* gmodel){
    double g = gmodel->m_gamma;
    double gmin1 = g - 1;
    double sq_rho_R = sqrt(f_R.m_rho);
    double sq_rho_L = sqrt(f_L.m_rho);
    double u_RL = (sq_rho_R*f_R.m_u + sq_rho_L*f_L.m_u)/(sq_rho_R + sq_rho_L);
    double h_RL = (sq_rho_R*f_R.m_h + sq_rho_L*f_L.m_h)/(sq_rho_R + sq_rho_L);

    vector3 W_L(f_L.m_rho, f_L.m_rho * f_L.m_u, f_L.get_total_energy());
    vector3 W_R(f_R.m_rho, f_R.m_rho * f_R.m_u, f_R.get_total_energy());
    vector3 deltaW = subtract(W_R, W_L);
    double c = sqrt((gmin1)*(h_RL - 0.5*u_RL*u_RL));
    double M = u_RL/c;
    
    vector3 l3((gmin1)*M*M/4 + 0.5*M,
        (-0.5 - 0.5*M*(gmin1))/c,
        gmin1/2.0/c/c);
    vector3 r3(1,
        u_RL - c,
        c*c/gmin1 - u_RL*(2*c - u_RL)/2.0);
    vector3 l1((gmin1)*M*M/4 - 0.5*M,
        (0.5 - 0.5*M*(gmin1))/c,
        gmin1/2.0/c/c);
    vector3 r1(1,
        u_RL + c,
        c*c/gmin1 + u_RL*(2*c + u_RL)/2.0);
    vector3 W_x;
    double t = 0.01;
    double x_t;
    double w_rho, w_u, w_p;
    ofstream m_file;
    m_file.open("result.dat", ios::out);
    m_file<<setw(12)<<"#x"<<setw(12)<<"rho"<<setw(12)<<"pressure"
          <<setw(12)<<"velocity"<<"\n";
    for(double x = -10.0; x < 10.0; x += 0.1){
        x_t = x / t;
        if (x_t < u_RL - c){
            W_x = W_L;
        }
        else if (x_t < u_RL){
            W_x = add(W_L, r3.scalarMult(dot(l3,deltaW)));
        }
        else if (x_t < u_RL + c){
            W_x = subtract(W_R, r1.scalarMult(dot(l1,deltaW)));
        }
        else if (x_t > u_RL + c){
            W_x = W_R;
        }
        w_rho = W_x.x[0]; 
        w_u = W_x.x[1]/W_x.x[0]; 
        w_p = (g - 1.0)*(W_x.x[2] - 0.5*w_rho*w_u*w_u);
        m_file<<setw(12)<<x<<setw(12)<<w_rho<<setw(12)<<w_p
          <<setw(12)<<w_u<<"\n";
    }
    m_file.close();

    return 1;
}   /* end of roe() */

/*--------------------------------------------------------------*/
