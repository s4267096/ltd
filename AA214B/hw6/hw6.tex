\documentclass[letterpaper,12pt]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage[margin=0.75in]{geometry}
\usepackage{IEEEtrantools}
\usepackage{subcaption}
\usepackage{epstopdf}
\usepackage{listings}
\newcommand{\ieq}{\begin{IEEEeqnarray*}{rCl}}
	\newcommand{\ieeq}{\end{IEEEeqnarray*}}
\newcommand{\jeq}{&=&}
\newcommand{\degrees}{^\circ}
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
	basicstyle=\footnotesize\ttfamily,
	breaklines=true,
	language=C++,
	commentstyle=\color{mygreen},
	keywordstyle=\color{blue},
	stringstyle=\color{mymauve}
}
\title{AA214B - Homework 6}
\author{Jonathan Ho}
\date{March 13 2017}

\begin{document}
	\maketitle
	\subsection*{Flow Solver Architecture}
	Some existing code of the author in C++ was exploited in solving this problem. It is set up for the possibility of doing more complex problems and as such, has multiple objects and modules defined. 
	
	\noindent \texttt{cell.h} contains a cell class which contains:
	\begin{itemize}
		\item flow state object at the cell;
		\item flow state object for the left and right faces of the cell;
		\item flux object for left and right interfaces at the cell;
		\item geometric information
		\item method for reconstruction interface values at left right given a certain interpolation method;
		\item integration method to update the flow state based on fluxes.
	\end{itemize}
	\texttt{flow\_oneD.h} is a flow state class which contains:
	\begin{itemize}
		\item flow state variables $\rho, u, e, p, T, h$;
		\item a gas model object;
		\item methods for computing total energy, momentum;
		\item	\text{flux} class contains mass, momentum and energy fluxes.
	\end{itemize}

	\texttt{thermo.h} is a gas model class which updates a flow state class based on two other thermodynamic variables such as $(\rho, p)$ or $p, c$. 
	\texttt{Interpolate.h} contains a variety of first order and second order reconstruction methods at the scalar level.
	\subsection*{Part 1 -- Exact Solution}
	The following algorithm was used for computing the exact solution and is extracted mainly from the AA214B lecture notes from Chapter 5. The required inputs are $p$, $\rho$ and $u$ from left and right states. which are denoted with the subscripts $4$ and $1$ respectively as shown in Figure \ref{shocktube}. 
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.5]{../../lxdhw8/report/diagram.png}
		\caption{Shock Tube Diagram}
		\label{shocktube}
	\end{figure}
	
	The following steps were used to compute the exact solution. Note all the equation numbers refer to Charbel Farhat's AA214B Lecture 5 notes on Representative Model Problems.
	\begin{enumerate}
		\item Compute $c_4, c_1$ using $c = \sqrt{\gamma P/\rho}$'
		\item Compute $p_2/p_1$ by solving the following implicit equation using the bisection method:
		\subitem \[\frac{p_4}{p_1} = \frac{p_2}{p_1}\left(
					1 + \frac{\gamma - 1}{2c_4}\left[u_4 - u_1
					 - \frac{c1}{\gamma}\frac{\frac{p_2}{p_1}-1}{
					 	\sqrt{\frac{\gamma+1}{2\gamma\left(\frac{p_2}{p_1}-1\right)+1}}}\right]\right)^{-\frac{2\gamma}{\gamma-1}}\]
		\item Compute $c_2$ using Equation 10;
		\item Compute $u_2$ using Equation 11;
		\item Compute $p_2$ using $(p_2/p_1)p_1$;
		\item Compute $\rho_2$ by re-arranging $c = \sqrt{\gamma P/\rho}$;
		\item Compute $V_{shock}$ (velocity of shock) using Equation 12;
		\item Compute $u_3 = u_2$ and $p3 = p_2$;
		\item Compute $c_3$ using Equation 17;
		\item Compute $\rho_3$ by re-arranging $c = \sqrt{\gamma P/\rho}$;
		\item For a range of $x$ discretised in the domain $[-10, 10]$ at $t = 0.01$
		\subitem Compute $x/t$
		\subitem If $x/t < V_{shock}$ assign flow state at 1;
		\subitem else if $x/t < u_2$ assign flow state at 2;
		\subitem else if $x/t < u_3 - c_3$ assign flow state at 3;
		\subitem else if $x/t < u_4 - c_4$ assign rarefaction values defined by Equation 15;
		\subitem else assign flow state at 4;
	\end{enumerate}
	This is implemented in Listing \ref{analytical} and was called in the main routine Listing \ref{test} and plotted using gnuplot as shown in Figures \ref{densityexact}--\ref{temperatureexact}.
	\lstinputlisting[caption={Analytical Solution},label={analytical}]{../../lxdhw8/analytical.cxx}

	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/density.png}
		\caption{Exact Solution -- Density}
		\label{densityexact}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/Pressure.png}
		\caption{Exact Solution -- Pressure}
		\label{pressureexact}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/Velocity.png}
		\caption{Exact Solution -- Velocity}
		\label{velocityexact}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/Temperature.png}
		\caption{Exact Solution -- Temperature}
		\label{temperatureexact}
	\end{figure}
	\clearpage
	\subsection*{Part 2 -- Simulation Methodology}
	\subsubsection*{Overall Outline}
	For all flux methods, the following overall methodology was used. The following simulation set up was implemented in the \texttt{main} function in Listing \ref{test}.
	\begin{enumerate}
		\item Initiate a flow state object for left and right flow states based on the initial conditions given of $\rho_L = 1$kg/m$^3$, $u_L = 50$m/s, $p_L = 100,000$Pa and $\rho_R = 0.125$kg/m$^3$, $u_R = 100.0$m/s, $p_R = 10,000$Pa.
		\item Initiate the gas model using $\gamma = 1.4$ and $M_w = 0.02897$;
		\item Initiate an array of $n$ cells  (of which 2 leading, and 2 trailing cells are ghost cells used for reconstruction)
		\item Initiate an array of of $n-3$ fluxes which are the faces of the $n-4$ cells that are not ghost cells and link these fluxes to the left and right faces of each cell;
		\item Fill the cells with flow state objects according to the initial conditions
	\end{enumerate}
	The simulation was executed in \texttt{run\_simulation} as follows:
	\begin{enumerate}
		\item At the left and right interfaces for each cell, reconstruct the flow state based on either a first or second order method that may take flow states from adjacent cells.
		\item At each interface, calculate the mass, momentum and energy fluxes based on interface flow states from cell to the left and right using either Roe or Steger-warming method;
		\item Update the flow state in each cell using time integration
		\item Compute the maximum sound speed in the domain as an estimate of the highest wave speed and update $dt$ based on the CFL condition using $CFL = 0.1$. While this value is much lower than the required value, it was not prohibitively expensive so it was used as it ensured stability without jeapordizing computational efficiency.
	\end{enumerate}
	\lstinputlisting[caption={Main routine},label={test}]{../../lxdhw8/test.cxx}
	
	This was carried out for a variety of discretisations.
	\subsubsection*{Flux Calculation}
	Two different flux calculators were used, Steger-Warming and Roe. The code for Roe's calculator from homework 3 which was set up to take a left and right flow state (which correspond to the right interface of the cell on the left and left interface of the cell on the right) only was used directly, and the reader is directed to that report for details. 
	
	The Steger Warming method is well summarised by Farhat in his notes ``AA214B: The Finite Volume Method''. The equations in the following method refer to those in Farhat's slides. For each cell face with a flow state from the left and right the following was calculated to compute the total flux. For the flow state on the left (the right interface of the cell to the left) only $\mathcal{F}_x^+$ was considered and for the flow state on the right only $\mathcal{F}_x^-$ was considered. For both the flow state on the left and right:
	\begin{itemize}
		\item Compute Mach number as $u/c$ 
		\item Based on the mach number $M$, compute either $\mathcal{F}_x^+$ (for the flow state on the left) or $\mathcal{F}_x^-$ (for the flow state on the right) based on the equations on slide 41-42 of the lecture notes, taking note to correct $\lambda_i$ using the formula: $\lambda_i^\pm = \frac{1}{2}\left(\lambda_i \pm \sqrt{\lambda_i^2 + \epsilon^2}\right)$ using $\epsilon = 10 ^{-6}$.
		\item Take the total flux to be the sum of $\mathcal{F}_x^+$ and $\mathcal{F}_x^-$ from the left and right states and store into a flux object.
	\end{itemize}
	This is shown in Listing \ref{sw} below. Roe's flux calculator is given in Listing \ref{roe}.
	\lstinputlisting[caption={Steger Warming Flux Splitting Method},label={sw}]{../../lxdhw8/stegerwarming.cxx}
	\lstinputlisting[caption={Roe's Method},label={roe}]{../../lxdhw8/roe.cxx}
	
	\subsubsection*{Reconstruction}
	For the ``first order method'', the left and right interface flow states were simply taken to be the flow state at the cell centre. This is shown in the \texttt{interpolateFirstOrderUpwind} function shown in Listing \ref{interpolate} below. 
	
	The ``second order method'' was slightly more complicated and incorporated the Van Leer's limiter as defined on slide 34--36 of Farhat's notes ``AA214B: The Finite Volume Method''. For the cell arrangement depicted in the comments of Listing \ref{interpolate} where we are reconstructing some flow variable on the right interface (denoted by *) of cell $i-1$:
	\begin{enumerate}
		\item Compute $r = \frac{f_{i-1} - f_{i-2}}{f_i - f_{i-1} + \epsilon}$ where $\epsilon = 10^{-12}$ was chosen to prevent dividing by zero
		\item If $r > 0$ keep the value of $r$, if it is negative set it to $r = 0$.
		\item Compute Van Leer's limiter function $\phi = \frac{2r}{1+r}$;
		\item Calculate the interface value as $f_{i-1/2} = f_{i-1} + \frac{1}{2}\phi(f_i - f_{i-1})$.
	\end{enumerate}
	This method is shown in \texttt{inerpolateSecondOrderUpwind} in LIsting \ref{interpolate}. The method is easily reversed for calculating left interface values. In this current implementation, the primitive variables $\rho, p, u$ were reconstructed (see the \texttt{interpolateL} and \texttt{interpolateR} functions in \texttt{cell.cxx} in Listing \ref{cell} in the appendix), and the remaining thermodynamic variables were updated using the gas model class.
	
	\lstinputlisting[caption={Reconstruction Methods},label={interpolate}]{../../lxdhw8/interpolate.cxx} 
	
	\subsection*{Part 3 -- Simulation Results}
	\subsubsection*{Steger-Warming Flux Vector Splitting Method}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/density.png}
		\caption{Density}
		\label{densityswfirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Pressure.png}
		\caption{Pressure}
		\label{pressureswfirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Velocity.png}
		\caption{Velocity}
		\label{velocityswfirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Temperature.png}
		\caption{Temperature}
		\label{temperatureswfirstorder}
	\end{figure}
	\clearpage
	\subsubsection*{Roe's First-Order Upwind Method}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/density.png}
		\caption{Density}
		\label{densityroefirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Pressure.png}
		\caption{Pressure}
		\label{pressureroefirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Velocity.png}
		\caption{Velocity}
		\label{velocityroefirstorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Temperature.png}
		\caption{Temperature}
		\label{temperatureroefirstorder}
	\end{figure}
	\clearpage
	\subsubsection*{Roe's Second-Order Upwind Method with Van Leer Limiter}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/density.png}
		\caption{Density}
		\label{densityroesecondorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Pressure.png}
		\caption{Pressure}
		\label{pressureroesecondorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Velocity.png}
		\caption{Velocity}
		\label{velocityroesecondorder}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Temperature.png}
		\caption{Temperature}
		\label{temperatureroesecondorder}
	\end{figure}
	\clearpage
	\subsection*{Part 4 -- Error compared to analytical solution}
	\subsubsection*{Steger-Warming Flux Vector splitting Method}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/density_error.png}
		\caption{Error in Density compared to Anaytical}
		\label{densitystegerwarmingfirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Pressure_error.png}
		\caption{Error in Pressure compared to Anaytical}
		\label{pressurestegerwarmingfirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Velocity_error.png}
		\caption{Error in Velocity compared to Anaytical}
		\label{velocitystegerwarmingfirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/stegerwarmingfirstorder/Temperature_error.png}
		\caption{Error in Temperature compared to Anaytical}
		\label{temperatureroefirstorder_error}
	\end{figure}
	\clearpage
	\subsubsection*{Roe's First-Order Upwind Method}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/density_error.png}
		\caption{Error in Density compared to Anaytical}
		\label{densityroefirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Pressure_error.png}
		\caption{Error in Pressure compared to Anaytical}
		\label{pressureroefirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Velocity_error.png}
		\caption{Error in Velocity compared to Anaytical}
		\label{velocityroefirstorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roefirstorder/Temperature_error.png}
		\caption{Error in Temperature compared to Anaytical}
		\label{temperatureroefirstorder_error}
	\end{figure}
	\clearpage
	\subsubsection*{Roe's Second-Order Upwind Method with Van Leer Limiter}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/density_error.png}
		\caption{Error in Density compared to Anaytical}
		\label{densityroesecondorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Pressure_error.png}
		\caption{Error in Pressure compared to Anaytical}
		\label{pressureroesecondorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Velocity_error.png}
		\caption{Error in Velocity compared to Anaytical}
		\label{velocityroesecondorder_error}
	\end{figure}
	\begin{figure}[hbpt!]
		\centering
		\includegraphics[scale=0.7]{../../lxdhw8/roesecondorder/Temperature_error.png}
		\caption{Error in Temperature compared to Anaytical}
		\label{temperatureroesecondorder_error}
	\end{figure}
\subsubsection*{Comparison of Different Methods}
		\begin{figure}[hbpt!]
			\centering
			\includegraphics[scale=0.7]{../../lxdhw8/density_comparemethods.png}
			\caption{Error in Density compared to analytical with N = 160}
			\label{densityroesecondorder_comparemethods}
		\end{figure}
		\begin{figure}[hbpt!]
			\centering
			\includegraphics[scale=0.7]{../../lxdhw8/Pressure_comparemethods.png}
			\caption{Error in Pressure compared to analytical with N = 160}
			\label{pressureroesecondorder_comparemethods}
		\end{figure}
		\begin{figure}[hbpt!]
			\centering
			\includegraphics[scale=0.7]{../../lxdhw8/Velocity_comparemethods.png}
			\caption{Error in Velocity compared to analytical with N = 160}
			\label{velocityroesecondorder_comparemethods}
		\end{figure}
		\begin{figure}[hbpt!]
			\centering
			\includegraphics[scale=0.7]{../../lxdhw8/Temperature_comparemethods.png}
			\caption{Error in Temperature compared to analytical with N = 160}
			\label{temperatureroesecondorder_comparemethods}
		\end{figure}
	\subsubsection*{Comments}
	\begin{itemize}
		\item At the first order level, both Steger Warming and Roe's calculator had comparable performance. Noticeably, Roe seemed to outperform Steger in capturing the correct values of temperature at $N=160$ in regions 2 and 3 of the problem. In general, Roe's calculator seemed to have lower error across all flow quantities as shown in Figures \ref{densityroesecondorder_comparemethods}--\ref{temperatureroesecondorder_comparemethods}.
		\item The error reduces as the grid size is reduced as expected for both methods.
		\item Both second and first order methods were able to compute the levels of $\rho$, $u$ etc. and location  of change in levels (i.e. the speed of the waves) relatively accurately in each region, 
		\item First order methods were less accurate in calculating how sharp the change in levels were as we can see in Roe's second order method with Van Leer limiter that we get much sharper changes at the shock and contact discontinuity in line with the analytical solution.
		\item A byproduct of the ability of second-order methods to better capture sharp discontinuities are oscillations in the properties close to these discontinuities. These oscillations can be reduced with different limiters such Van Albada's limiter or Superbee, but these may sacrifice how well the discontinuity is captured.
	\end{itemize}
	\subsection*{Appendix -- Auxiliary Code Listings}
		\lstinputlisting[label={cell},caption={\texttt{cell.cxx}}]{../../lxdhw8/cell.cxx}
		\lstinputlisting[caption={\texttt{flow\_oneD.cxx}}]{../../lxdhw8/flow_oneD.cxx}
		\lstinputlisting[caption={\texttt{thermo.cxx}}]{../../lxdhw8/thermo.cxx}
		\lstinputlisting[caption={\texttt{matrix.cxx}}]{../../lxdhw8/matrix.cxx}
		\lstinputlisting[caption={\texttt{post.cxx}}]{../../lxdhw8/post.cxx}
\end{document}\grid
